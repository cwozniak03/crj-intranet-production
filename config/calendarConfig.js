var fs = require('fs');
var readline = require('readline');
var google = require('googleapis');
var googleAauth = require('google-auth-library');
var scopes = ['https://www.googleapis.com/auth/calendar',
        'https://www.googleapis.com/auth/admin.directory.user'];
var key = require('./crj-intranet-google-service-account.json');
var jwtClient = new google.google.auth.JWT(key.client_email, null, key.private_key, scopes, "pto@cristoreyjesuit.org");

exports.jwtClient = jwtClient;
