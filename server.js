// server.js

// set up ======================================================================
// get all the tools we need
var config = require("./config/configFile.js").get(process.env.NODE_ENV.trim());
var express  = require('express');
var session  = require('express-session');
var RedisStore = require('connect-redis')(session);
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var app      = express();
var port     = config.port;

var flash    = require('connect-flash');
var cron = require('node-cron');

var AdminPortal = require('./models/adminportal');
var CronJobs = require('./models/cronjobs');
var Powerschool = require('./models/powerschool');
var EmailSender = require('./models/emailSender');
var Permissions = require('./models/permissions');
var DataExtract = require('./models/dataExtractionUpload');

var PH = require('./models/ph');
var Promise = require('promise');
var moment = require('moment');
// configuration ===============================================================
// connect to our database

//require('./config/passport')(passport); // pass passport for configuration



// set up our express application
//app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({
	store: new RedisStore({
		host: 'localhost',
		port: 6379
	}),
	secret: 'correcthorsebatterystaple',
	resave: true,
	saveUninitialized: true
 } )); // session secret
//app.use(passport.initialize());
//app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
app.use(express.static(__dirname + '/public'));

// routes ======================================================================
require('./app/routes.js')(app); // load our routes and pass in our app and fully configured passport
require('./app/ptoRoutes.js')(app); //Load the pto routes
require('./app/substituteRoutes.js')(app); //Load the substitute routes
require('./app/calendarRoutes.js')(app); //Load the google calendar api routes
require('./app/primeSecRoutes.js')(app); //Load the primary/secondary subs routes
require('./app/adminPortalRoutes.js')(app); //Load the admin portal routes
require('./app/ptoDatesRoutes.js')(app); //Load the date Routes
require('./app/phRoutes.js')(app); //Load the PH Routes
require('./app/contractsRoutes.js')(app); //Load the Contract Routes
require('./app/utilRoutes.js')(app); //Load the Util Routes
require('./app/collegeFinancialsRoutes.js')(app); //Load the college financial routes
require('./app/statusRoutes.js')(app);
require('./app/assetRoutes.js')(app);
require('./app/checkoutRoutes.js')(app);
// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);
if(process.env.NODE_ENV.trim() == "production") {
	let ptoResetPromise = AdminPortal.getResetDate();
	ptoResetPromise.then(function(result) {
		let fields = result.split("/");
		let month = fields[0];
		let day = fields[1];
		CronJobs.PTOReset(month,day);
		CronJobs.getTardies();
		CronJobs.getStudentDataJob();
		CronJobs.getStudentGradesJob();
		CronJobs.getAssignmentsJob();
		CronJobs.getAbsensesJob();
		CronJobs.getCoursesJob();
		CronJobs.getTeachersJob();
		CronJobs.getSectionsJob();
		CronJobs.getStandardizedTestsJob();
	});
}