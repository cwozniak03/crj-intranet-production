// load up the user model
/*var mysql = require('mysql');*/
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');
var Promise = require('promise');
let dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;

function utils() {
	this.GenerateCode = function() {
		let text = "";
		let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for (var i = 0; i < 10; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		let insertQuery = "INSERT INTO override_codes (code) VALUES (?)";
		dbconfig.getConnection(function(err, connection) {
			connection.query(insertQuery,[text],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'INSERT Failed - Generate Code: ' + err});
				}
			});
		});
		 return text;
	},
	this.ValidateCode = function(code) {
		return new Promise(function(fulfill,reject) {
			let getQuery = 'SELECT code from override_codes where code = ? AND code_used <> 1';
			dbconfig.getConnection(function(err, connection) {
				connection.query(getQuery,[code],function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'SELECT Failed - Validate Code: ' + err}));
					} else {
						if(result.length > 0) {
							fulfill(true);
						} else {
							fulfill(false);
						}
					}
				});
			});
		});
	},
	this.markCodeUsed = function(code) {
		let updateQuery = 'UPDATE override_codes SET code_used = 1 WHERE code = ?';
		dbconfig.getConnection(function(err, connection) {
			connection.query(updateQuery,[code],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'INSERT Failed - Add User Info: ' + err});
				}
			});
		});
	}

}

module.exports = new utils();