const xl = require('excel4node');
const moment = require('moment');
const fs = require('fs');
const stream = require('stream');
function ExcelCreation() {
	this.CreateExcelFileOutstandingPH = function(excelHeaders, dataset, res) {
		const wb = new xl.Workbook();
		// Add Worksheets to the workbook
		const ws = wb.addWorksheet('Sheet 1');
		// Create a reusable style
		const headerStyle = wb.createStyle({
		  font: {
		    color: '#000000',
		    size: 16,
		    bold:true
		  },
		  numberFormat: '$#,##0.00; ($#,##0.00); -',
		});
		for(let i = 1; i <= excelHeaders.length; i++) {
			ws.cell(1,i).string(excelHeaders[i-1]);
		}
		let x = 2;
		dataset.forEach(function(data) {
			Object.keys(data).forEach(function(value,i) {	
				if(value != 'ph_submissions_id') {
					if(value == 'ph_submissions_date_assigned' || value == 'ph_date_to_attend') {
						ws.cell(x,i+1).date(moment(data[value]).format("MM/DD/YYYY"));
					} else if(value == 'ph_submissions_grade_level') {					
						ws.cell(x,i+1).number(data[value]);
					} else if(value == 'ph_submissions_attended') {
						if(data[value] == 0) {
							ws.cell(x,i+1).string("Not Attended");
						} else {
							ws.cell(x,i+1).string("Attended");
						}
					} else {
						ws.cell(x,i+1).string(data[value]);
					}		
				}
			});
			x++;
		});		 
		wb.write('./public/OutstandingPH.xlsx', function(err,success) {
			if(err) {
				console.log("Excel write Error: "+err);
			} else {
				res.sendStatus(200);
			}
		});
	}
}
module.exports = new ExcelCreation();