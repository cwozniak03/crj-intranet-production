// load up the user model
var bcrypt = require('bcrypt-nodejs');
var dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
var moment = require('moment');
var ad = require('../config/ad.js').ad;
var EmailSender = require('../models/emailSender');
var Promise = require('promise');
var Permissions = require('../models/permissions');
var Powerschool = require('../models/powerschool');
let config = require("../config/configFile.js").get(process.env.NODE_ENV.trim());

function PH() {
	this.addPH = function(req,res,isFriday,isHidden) {
		phs = this;

		const fri = 5;

		var ph_attended = 0;
		if(isHidden) {
			ph_attended = 3;
		}
		var student_username = req.body.students;
		var ph_reason = req.body.ph_reason;
		var ph_notes = req.body.ph_notes;
		var teacher_username = req.session.username;
		var ph_type = req.body.ph_type;
		if (isFriday) {
			ph_type = "Friday PH";
		}
		var assignedDate;
		var date = new Date();
		date = moment(date).format("YYYY-MM-DD");
		if(ph_type == "PH") {
			assignedDate = moment().add(7,'days').format("YYYY-MM-DD");
		} else if(ph_type == "Friday PH") {
			assignedDate = moment().add(1,'week').isoWeekday(fri).format('YYYY-MM-DD');
		} else if(ph_type == "Saturday PH") {
			assignedDate = date;
		}
		var grade = getStudentGrade(student_username);
		var StudentInfoPromise = Permissions.getStudentName(student_username);
		StudentInfoPromise.then(function(studentInfo) {
			var insertQuery = "INSERT INTO ph_submissions (ph_submissions_assigned_to, ph_submissions_reason, ph_submissions_description, ph_submissions_assigned_by, ph_submissions_date_assigned, ph_submissions_grade_level, ph_type, ph_submissions_attended, ph_date_to_attend, first_name, last_name) ";
			insertQuery += "values (?,?,?,?,?,?,?,?,?,?,?)";
			dbconfig.getConnection(function(err, connection){
				connection.query(insertQuery,[student_username, ph_reason, ph_notes, teacher_username, date, grade, ph_type, ph_attended, assignedDate, studentInfo.givenName, studentInfo.sn],function(err, rows) {
					connection.release();
					if(err){
						console.log({status: 1, message: 'Post Failed - PH add: ' + err});
						res.render('ph/ph.ejs', {
							login: true,
							success: false
						});
					} else {
						EmailSender.emailPHAssigned(res,student_username, ph_reason,ph_type,ph_notes);
						res.render('ph/ph.ejs', {
							login: true,
							success: true
						});
					}
				});
			});
		});
	},
	this.addTardyPH = function(info,isThirds) {
		phs = this;
		const fri = 5;
		var student_username = info.student_web_id;
		var ph_reason = 'Tardy';
		var ph_notes;
		if(isThirds) {
			ph_notes = 'Third first period tardy entry from Powerschool.';
		} else {
			ph_notes = 'Automated tardy entry from Powerschool.';
		}
		var teacher_username = config.usernames.deanofstudents;
		var date = new Date();
		var name = info.lastfirst.split(",");
		date = moment(date).format("YYYY-MM-DD");
		var grade = getStudentGrade(student_username);
		var checkAlreadyHasPHForTodayPromise = phs.checkAlreadyHasPHForToday(student_username, info);
		var assignedDate;
		checkAlreadyHasPHForTodayPromise.then(function(result){
			if(result[0]) {
				//Assign Regular PH with "hidden" attended marker (We want to keep track, but students wont have to attend past 1 a day)
				phs.createTardyPH(student_username, ph_reason, ph_notes, teacher_username, date, grade, 'PH', 3, moment().add(7,'days').format("YYYY-MM-DD"), name[0], name[1].trim());
				//Check if a student already has a Friday PH
				var checkAlreadyHasFridayPromise = phs.checkAlreadyHasFridayThisWeek(result[1].student_web_id);
				checkAlreadyHasFridayPromise.then(function(hasFriday){
					if(!hasFriday) {
						assignedDate = moment().add(1,'week').isoWeekday(fri).format('YYYY-MM-DD');
						//Assign a Friday PH
						phs.createTardyPH(student_username, ph_reason, ph_notes, teacher_username, date, grade, 'Friday PH', 0, assignedDate, name[0], name[1].trim());
						EmailSender.emailTardiesFriday(info);	
					}
				});
			} else {
				assignedDate = moment().add(7,'days').format("YYYY-MM-DD");
				//EmailSender.emailTardies(info); COMMENTED OUT DUE TO TESTING BCC SINGLE EMAIL DUE TO GOOGLE API RESTRICTIONS
				phs.createTardyPH(student_username, ph_reason, ph_notes, teacher_username, date, grade, 'PH', 0, assignedDate, name[0], name[1].trim());
			}
		});
	},
	this.addFridayManual = function(username) {
		return new Promise(function(fulfill, reject) {
			let assignedDate;
			let date = new Date();
			date = moment(date).format("YYYY-MM-DD");
			assignedDate = moment().add(1,'week').isoWeekday(5).format('YYYY-MM-DD');
			let student_username = username;
			var ph_reason = "Missed PH";
			var ph_notes = "Friday PH Assigned due to missing a PH"
			var teacher_username = config.usernames.deanofstudents;
			var ph_type = "Friday PH";
			var grade = getStudentGrade(username);
			var StudentInfoPromise = Permissions.getStudentName(username);
			StudentInfoPromise.then(function(studentInfo) {
				var insertQuery = "INSERT INTO ph_submissions (ph_submissions_assigned_to, ph_submissions_reason, ph_submissions_description, ph_submissions_assigned_by, ph_submissions_date_assigned, ph_submissions_grade_level, ph_type, ph_submissions_attended, ph_date_to_attend, first_name, last_name) ";
				insertQuery += "values (?,?,?,?,?,?,?,?,?,?,?)";
				dbconfig.getConnection(function(err, connection){
					connection.query(insertQuery,[username, ph_reason, ph_notes, teacher_username, date, grade, ph_type, 0, assignedDate, studentInfo.givenName, studentInfo.sn],function(err, rows) {
						connection.release();
						if(err){
							reject(console.log({status: 1, message: 'Post Failed - PH add: ' + err}));
						} else {
							EmailSender.emailPHMissed(username, date, assignedDate);
							fulfill(true);
						}
					});
				});
			});
		});
	}
	this.createTardyPH = function(student_username, ph_reason, ph_notes, teacher_username, date, grade, ph_type, ph_attended, assignedDate, last_name, first_name){
		var insertQuery = "INSERT INTO ph_submissions (ph_submissions_assigned_to, ph_submissions_reason, ph_submissions_description, ph_submissions_assigned_by, ph_submissions_date_assigned, ph_submissions_grade_level, ph_type, ph_submissions_attended, ph_date_to_attend, last_name, first_name) ";
		insertQuery += "values (?,?,?,?,?,?,?,?,?,?,?)";
		dbconfig.getConnection(function(err, connection){
			connection.query(insertQuery,[student_username, ph_reason, ph_notes, teacher_username, date, grade, ph_type, ph_attended, assignedDate, last_name, first_name],function(err, rows) {
				connection.release();
				if(err){
					console.log({status: 1, message: 'Post Failed - PH add: ' + err});
				}
			});
		});
	},
	this.addFirstPeriodTardy = function(info, tagged_marker) {
		phs = this;
		var date = moment(date).format("YYYY-MM-DD");
		var grade = getStudentGrade(info.student_web_id);
		var insertQuery = "INSERT INTO first_period_tardies (assigned_to, assigned_by, date_assigned, tagged_marker, grade_level) VALUES (?,?,?,?,?)";
		dbconfig.getConnection(function(err,connection) {
			connection.query(insertQuery,[info.student_web_id, config.usernames.deanofstudents, date, tagged_marker, grade],function(err, rows){
				connection.release();
				if(err){
					console.log({status: 1, message: 'Post Failed - First Period Tardy PH add: ' + err});
				}
			});
		});
	},
	//Update the tagged marker for all of the exiting first period tardies so they dont get pulled again
	this.UpdateFirstTardiesTagged = function(assigned_to) {
		return new Promise(function(fulfill, reject) {
			var updateQuery = "UPDATE first_period_tardies SET tagged_marker = 1 WHERE assigned_to = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(updateQuery, [assigned_to], function(err, rows){
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Update Failed - First Period Tardy Update tagged marker: ' + err}));
					} else {
						fulfill(true);
					}
				});
			});
		});
	},
	//Separate the First Period Tardies from the rest, this makes us avoid Deadlock errors on Selects and Inserts for the table
	this.splitTardiesToGroups = function(tardies,firstperiod_ID) {
		return new Promise(function(fulfill, reject) {
			var ReturnTardies = {};
			var PHTardies = [];
			var FirstTardies = [];
			for(i=0;i<tardies.length;i++) {
				if(parseInt(firstperiod_ID) == parseInt(tardies[i].periodid)) {	
					FirstTardies.push(tardies[i]);
				} else {
					PHTardies.push(tardies[i]);
				}
			}
			ReturnTardies = {"FirstTardies":FirstTardies, "PHTardies":PHTardies}
			fulfill(ReturnTardies);
		});
	},
	//The business rules state that if a Student is late to First Period 3 times they will get a regular PH. Otherwise it just gets logged in the separate table.
	//This basically means that a First Period tardy counts as 1/3 of a PH.
	this.ThirdFirstTardy = function(tardies) {
		var phs = this;
		return new Promise(function(fulfill,reject){
			var promises = [];
			var ReturnTardies = {};
			var Thirds = [];
			var NonThirds = [];
			for(i=0;i<tardies.length;i++) {
				var totalFirstTardiesPromise = phs.checkTotalFirstTardies(tardies[i]);
				promises.push(totalFirstTardiesPromise);
			}
			//We have to do promise all so that we don't fulfill before the promises have finished.
			Promise.all(promises).then(values => {
				for(x=0;x<values.length;x++){
					if(values[x][0]==2){
						Thirds.push(values[x][1]);
					} else {
						NonThirds.push(values[x][1]);
					}
				}
				ReturnTardies = {"Thirds":Thirds, "NonThirds": NonThirds};
				fulfill(ReturnTardies);
			});
		});
	},
	this.checkTotalFirstTardies = function(tardies) {;
		return new Promise(function(fulfill,reject) {
			var selectQuery = "SELECT COUNT(*) as Counter FROM first_period_tardies WHERE tagged_marker = 0 AND assigned_to = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,[tardies.student_web_id],function(err, result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'SELECT Failed - Check Total First Tardies: ' + err});
					} else {
						fulfill([result[0].Counter,tardies]);
					}
				});
			});
		});
	},
	this.checkAlreadyHasPHForToday = function(student_username, tardyInfo) {
		return new Promise(function(fulfill, reject) {
			var username = student_username;
			var date = new Date();
			date = moment(date).format("YYYY-MM-DD");
			var selectQuery = "SELECT * from ph_submissions WHERE ph_submissions_assigned_to = ? AND ph_submissions_date_assigned = ? AND ph_type = 'PH' ";
			dbconfig.getConnection(function(err, connection){
				connection.query(selectQuery,[username,date],function(err,results) {
					connection.release();
					if(err) {
						reject(console.log({stats: 1, message: 'SELECT Failed - Check already has ph for Today: ' + err}));
					} else {
						if(results.length > 0) { fulfill([true,tardyInfo]);} else { fulfill([false,tardyInfo]); }
					}
				});
			});
		});
	},
	this.checkAlreadyHasFridayThisWeek = function(student_username) {
		return new Promise(function(fulfill, reject) {
			var username = student_username;
			var date = new Date();
			date = moment().add(1,'week').isoWeekday(5).format('YYYY-MM-DD');
			var selectQuery = "SELECT * FROM ph_submissions WHERE ph_submissions_assigned_to = ? AND ph_submissions_date_assigned = ? AND ph_type = 'Friday PH' ";
			dbconfig.getConnection(function(err, connection){
				connection.query(selectQuery,[username,date],function(err,results) {
					connection.release();
					if(err) {
						reject(console.log({stats: 1, message: 'SELECT Failed - Check already has ph for Today: ' + err}));
					} else {
						if(results.length > 0) { fulfill(true);} else { fulfill(false); }
					}					
				});
			});
		});
	},
	this.emailPHForToday = function() {
		let today = moment().format("YYYY-MM-DD");
		let selectQuery = "SELECT ph_submissions_assigned_to FROM ph_submissions WHERE ph_submissions_attended = 0 AND ph_date_to_attend = ?";
		let studentEmailList = [];
		dbconfig.getConnection(function(err, connection){
			connection.query(selectQuery,[today], function(err, results) {
				connection.release();
				if(err) {
					console.log({stats: 1, message: 'SELECT Failed - Check already has ph for Today: ' + err});
				} else {
					for(var x=0;x<results.length;x++) {
						studentEmailList.push(results[x].ph_submissions_assigned_to);
					}
					EmailSender.emailPHReminder(studentEmailList);
				}
			});
		});
	},
	this.getTardiesByGrade = function(req, res) {
		var grade = req.body.ddlGrade,
			start_date = moment(req.body.start_date).format('YYYY-MM-DD'),
			end_date = moment(req.body.end_date).format('YYYY-MM-DD');	
		var selectQuery = "SELECT * FROM ph_submissions WHERE ph_submissions_grade_level = ? and ph_submissions_reason = 'Tardy' and ph_submissions_date_assigned BETWEEN ? AND ?";
		dbconfig.getConnection(function(err, connection) {
			connection.query(selectQuery,[grade,start_date,end_date],function(err,result) {
				connection.release();
				if(err){
					console.log({status:1, message: 'Select Failed - Get Tardies:' + err});
				} else {
					res.render('ph/gradeTardies', {
						phs : result,
						login : true,
						moment : moment,
						grade : grade,
						start_date: start_date,
						end_date : end_date
					});
				}
			});
		});
	},
	this.getOustandingPHs = function() {
		return new Promise(function(fulfill, reject) {
			var selectQuery = "SELECT first_name, last_name, ph_submissions_assigned_by, ph_submissions_date_assigned, ph_date_to_attend, ";
			selectQuery += "ph_submissions_reason, ph_submissions_description, ph_submissions_attended, ph_submissions_grade_level, ";
			selectQuery += "ph_submissions_id FROM ph_submissions WHERE ph_submissions_attended = 0 AND ph_type = 'PH'";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,function(err,result) {
					connection.release();
					if(err){
						reject(console.log({status:1, message: 'Select Failed - Get Oustanding PHs:' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},
	this.renderOutstandingPHs = function(req,res) {
		let ph = this;
		let canRemovePH = Permissions.hasRole(req.session.username, "PHRemove");
		let oustandingPHPromise = ph.getOustandingPHs();
		Promise.all([canRemovePH, oustandingPHPromise]).then(function(values) {
			res.render('ph/oustandingPH', {
				phs : values[1],
				login : true,
				moment : moment,
				canRemove : values[0]
			});
		});	 
	},
	this.getOustandingFridays = function(req,res) {
		let selectQuery	= "SELECT a.*, ";
		selectQuery += "b.ph_submissions_reason as missed_reason,";
		selectQuery += "b.ph_submissions_description as missed_description,";
		selectQuery += "b.ph_submissions_assigned_by as missed_assigned_by,";
		selectQuery += "b.ph_submissions_date_assigned as missed_date_assigned,";
		selectQuery += "b.ph_type as missed_ph_type,";
		selectQuery += "b.ph_date_to_attend as missed_date_to_attend ";
		selectQuery += "FROM ph_submissions a ";
		selectQuery += "LEFT JOIN missed_ph_links on a.ph_submissions_id = missed_ph_links.friday_ph_id ";
		selectQuery += "LEFT JOIN ph_submissions b on b.ph_submissions_id = missed_ph_links.missed_ph_id ";
		selectQuery += "WHERE a.ph_submissions_attended = 0 AND a.ph_type = 'Friday PH'";
		dbconfig.getConnection(function(err, connection) {
			connection.query(selectQuery,function(err,result) {
				connection.release();
				if(err){
					console.log({status:1, message: 'Select Failed - Get Oustanding PHs:' + err});
				} else {
					res.render('ph/oustandingFridayPH', {
						phs : result,
						login : true,
						moment : moment
					});
				}
			});
		});
	},
	this.getOutstandingSaturdays = function(req,res) {
		var selectQuery = "SELECT * FROM ph_submissions WHERE ph_submissions_attended = 0 AND ph_type = 'Saturday PH'";
		dbconfig.getConnection(function(err, connection) {
			connection.query(selectQuery,function(err,result) {
				connection.release();
				if(err){
					console.log({status:1, message: 'Select Failed - Get Oustanding PHs:' + err});
				} else {
					res.render('ph/oustandingSaturdayPH', {
						phs : result,
						login : true,
						moment : moment
					});
				}
			});
		});
	},
	this.getPHListBySID = function(req,res) {
		var selectQuery = "SELECT * FROM ph_submissions WHERE ph_submissions_attended = 0 AND ph_type = 'PH'";
		dbconfig.getConnection(function(err, connection) {
			connection.query(selectQuery,function(err,result) {
				connection.release();
				if(err){
					console.log({status:1, message: 'Select Failed - Get Oustanding PHs:' + err});
				} else {
					res.render('ph/phBySID', {
						phs : result,
						login : true,
						moment : moment
					});
				}
			});
		});
	},	
	this.getOutstandingPHsByStudent = function(student, ph_type) {
		return new Promise(function(fulfill, reject) {
			var selectQuery = "SELECT * FROM ph_submissions WHERE ph_submissions_attended = 0 AND ph_type= ? AND ";
			selectQuery += "(first_name LIKE "+dbconfig.escape('%'+student+'%')+" OR last_name LIKE "+dbconfig.escape('%'+student+'%')+")";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,[ph_type],function(err,result) {
					connection.release();
					if(err){
						reject(console.log({status:1, message: 'Select Failed - Get Oustanding PHs:' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},
	this.getAttendedPHsByStudent = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let selectQuery = "SELECT * FROM ph_submissions ";
			selectQuery += "INNER JOIN ph_attended ON ph_submissions_id = ph_attended.ph_submissions_attended_id ";
			selectQuery += "WHERE ph_submissions_attended = 1 AND ph_type='PH' AND ph_submissions_assigned_to LIKE "+dbconfig.escape('%'+req.body.student+'%');
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,function(err,result) {
					connection.release();
					if(err){
						reject(console.log({status:1, message: 'Select Failed - Get Attended PHs By Student:' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},
	this.getAttendedPHs = function(req,res) {
		let selectQuery = "SELECT * FROM ph_submissions ";
		selectQuery += "INNER JOIN ph_attended ON ph_submissions.ph_submissions_id = ph_attended.ph_submissions_attended_id ";
		selectQuery += "WHERE ph_submissions_attended = 1";
		dbconfig.getConnection(function(err, connection) {
			connection.query(selectQuery,function(err,result) {
				connection.release();
				if(err){
					console.log({status:1, message: 'Select Failed - Get Oustanding PHs:' + err});
				} else {
					res.render('ph/attendedPHReport', {
						phs : result,
						login : true,
						moment : moment
					});
				}
			});
		});
	},
	this.getMoreThan10 = function(req,res) {
		var grade = req.body.ddlGrade,
			start_date = moment(req.body.start_date).format('YYYY-MM-DD'),
			end_date = moment(req.body.end_date).format('YYYY-MM-DD');	
		var selectQuery = "SELECT ph_submissions_assigned_to, COUNT(ph_submissions_assigned_to) as count FROM ph_submissions WHERE ph_submissions_date_assigned BETWEEN ? AND ?";
			selectQuery += " Group By ph_submissions_assigned_to";
			selectQuery += "HAVING count > 10";
		dbconfig.getConnection(function(err, connection) {
			connection.query(selectQuery,function(err,result) {
				connection.release();
				if(err) {
					console.log({status:1, message: 'Select Failed - Get more than 10 PH in a span: ' + err});
				} else {
					res.render('ph/10PHReport', {
						phs : result,
						login :true,
						moment : moment,
						start_date: start_date,
						end_date : end_date
					});
				}
			});
		});
	},	
	this.getRecentFirsts = function(req,res) {
		//Ok so here is the deal, if I change from country_rank and current_country it stops working. So the current country
		//should be current student and country rank should be student rank. But then it wont work so deal with it
		var selectQuery = "SELECT * FROM (SELECT *, ";
        	selectQuery +="@country_rank := IF(@current_country = assigned_to, @country_rank + 1, 1) AS country_rank,";
            selectQuery +=" @current_country := assigned_to";
       		selectQuery +=" FROM first_period_tardies";
      		selectQuery +=" where tagged_marker = 1";
        	selectQuery +=" ORDER BY assigned_to, date_assigned DESC";
     		selectQuery +=" ) ranked";
   			selectQuery +=" WHERE country_rank <= 3";
   		dbconfig.getConnection(function(err,connection) {
   			connection.query(selectQuery, function(err,result) {
   				connection.release();
   				if(err) {
   					console.log({status:1, message: 'Select Failed - Recent Firsts: ' + err});
   				} else {
   					res.render('ph/FirstsReports', {
						phs : result,
						login :true,
						moment : moment
					});
   				}
   			})
   		});
	}
	this.markAttended = function(req,res) {
		let headerArray = req.header('Referer').split("/");
		let url = headerArray[headerArray.length-1];
		var updateQuery = 'UPDATE ph_submissions SET ph_submissions_attended = 1 WHERE ph_submissions_id = ?';
		dbconfig.getConnection(function(err, connection) {
			connection.query(updateQuery,[req.body.id],function(err,result) {
				connection.release();
				if(err){
					console.log({status:1, message: 'Update Failed - Mark Attended:' + err});
				} else {
					res.send(true);
					//Javascript handles the redirect
				}
			});
		});
	},
	this.markMissed = function(req,res) {
		let phs = this;
		let usernamePromise = phs.getUsernameMissedPH(req.body.id);
		usernamePromise.then(function(username) {
			var updateQuery = 'UPDATE ph_submissions SET ph_submissions_attended = 4 WHERE ph_submissions_id = ?';
			dbconfig.getConnection(function(err, connection) {
				connection.query(updateQuery,[req.body.id],function(err,result) {
					connection.release();
					if(err){
						console.log({status:1, message: 'Update Failed - Mark Attended:' + err});
					} else {
						phs.addFridayManual(username);
						phs.addMissedPHLinks(req.body.id, result.insertId);
						res.send(true);
					}
				});
			});
		});
	},
	this.getUsernameMissedPH = function(ph_submission_id) {
		return new Promise(function(fulfill, reject) {
			let getQuery = 'SELECT ph_submissions_assigned_to FROM ph_submissions WHERE ph_submissions_id = ?';
			dbconfig.getConnection(function(err, connection) {
				connection.query(getQuery,[ph_submission_id],function(err,result) {
					connection.release();
					if(err){
						reject(console.log({status:1, message: 'Update Failed - Mark Attended:' + err}));
					} else {
						fulfill(result[0].ph_submissions_assigned_to);
					}
				});
			});
		});
	},
	this.addMissedPHLinks = function(missed_ph_id, friday_ph) {
		var insertQuery = "INSERT INTO missed_ph_links (missed_ph_id, friday_ph_id) VALUES (?,?)";
		dbconfig.getConnection(function(err,connection) {
			connection.query(insertQuery,[missed_ph_id, friday_ph],function(err, rows){
				connection.release();
				if(err){
					console.log({status: 1, message: 'Insert Failed - MissedPHLink: ' + err});
				}
			});
		});		
	},
	this.markRemoved = function(req,res) {
		var updateQuery = 'UPDATE ph_submissions SET ph_submissions_attended = 3 WHERE ph_submissions_id = ?';
		dbconfig.getConnection(function(err, connection) {
			connection.query(updateQuery,[req.body.id],function(err,result) {
				connection.release();
				if(err){
					console.log({status:1, message: 'Update Failed - Mark Removed:' + err});
				} else {
					res.send(true);
					//Javascript handles the redirect
				}
			});
		});
	},	
	this.getAbsenses = function() {
		var m = moment(new Date());
		var seconds = (m.hour()*60*60 + m.minute()*60);
		var curPeriodPromise = Powerschool.getGetCurrentPeriodID(seconds);
		curPeriodPromise.then(function(currentPeriod_ID) {
			var isFirstPromise = Powerschool.getGetFirstPeriodID();
			var originalFirstPromise = Powerschool.getGetOriginalFirstPeriodID();
			var powerschoolToken = Powerschool.getToken();
			var absentStudentPromise = Powerschool.getAllAbsentStudents(currentPeriod_ID);
			Promise.all([originalFirstPromise, isFirstPromise, absentStudentPromise, powerschoolToken]).then(values => {
				var firstPeriod_ID = parseInt(values[1]),
					originalFirstPeriod_ID = parseInt(values[0]),
					students = values[2],
					token = values[3];
				for(var x=0; x < students.length; x++) {
					if(currentPeriod_ID != firstPeriod_ID) {
						var lastPeriod = originalFirstPeriod_ID + 7;
						if(currentPeriod_ID != lastPeriod) {
							var absentNextPeriodPromise = Powerschool.checkAbsentSpecificPeriod(parseInt(currentPeriod_ID)+1, students[x].student_id, token);
							absentNextPeriodPromise.then(function(result) {
								var absentPreviousPromise = Powerschool.checkAbsentSpecificPeriod(parseInt(currentPeriod_ID)-1, result.student_id, token);
								if(result.absent == "0") {
									absentPreviousPromise.then(function(absentID) {
										var studentPromise = Powerschool.getStudentInformation(absentID.student_id, token);
										if(absentID.absent == "0") {
											studentPromise.then(function(student) {
												EmailSender.emailAbsenses(student);
											});
										}
									});
								}
							});
						} else if(currentPeriod_ID == lastPeriod) {
							var absentNextPeriodPromise = Powerschool.checkAbsentSpecificPeriod(originalFirstPeriod_ID, students[x].student_id, token);
							absentNextPeriodPromise.then(function(result) {
								var absentPreviousPromise = Powerschool.checkAbsentSpecificPeriod(parseInt(currentPeriod_ID)-1, result.student_id, token);
								if(result.absent == "0") {
									absentPreviousPromise.then(function(absentID) {
										var studentPromise = Powerschool.getStudentInformation(absentID.student_id, token);
										if(absentID.absent == "0") {
											studentPromise.then(function(student) {
												EmailSender.emailAbsenses(student);
											});
										}
									});
								}
							});
						} else if(currentPeriod_ID == originalFirstPeriod_ID) {
							var absentNextPeriodPromise = Powerschool.checkAbsentSpecificPeriod(parseInt(currentPeriod_ID)+1, students[x].student_id, token);
							absentNextPeriodPromise.then(function(result) {
								var absentPreviousPromise = Powerschool.checkAbsentSpecificPeriod(lastPeriod, result.student_id, token);
								if(result.absent == "0") {
									absentPreviousPromise.then(function(absentID) {
										var studentPromise = Powerschool.getStudentInformation(absentID.student_id, token);
										if(absentID.absent == "0") {
											studentPromise.then(function(student) {
												EmailSender.emailAbsenses(student);
											});
										}
									});
								}
							});
						}
					}
				}
			});
		});
	}
}

function getStudentGrade(username) {
	var GradeLevel,
		CurrentDate,
	    CurrentMonth;
	CurrentDate = new Date();
	CurrentMonth = CurrentDate.getMonth();
	var graduationYear = username.slice(-4);
	var currentDate = new Date();
	var curYear = currentDate.getFullYear();
	if(CurrentMonth >=7 && CurrentMonth<=12) {
		if(graduationYear == curYear+1) {
			return 12;
		} else if(graduationYear == curYear+2) {
			return 11;
		} else if(graduationYear == curYear+3) {
			return 10;
		} else if(graduationYear == curYear+4) {
			return 9;
		}
	} else {
		if(graduationYear == curYear) {
			return 12;
		} else if(graduationYear == curYear+1) {
			return 11;
		} else if(graduationYear == curYear+2) {
			return 10;
		} else if(graduationYear == curYear+3) {
			return 9;
		}
	}
}
module.exports = new PH();