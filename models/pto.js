// load up the user model
/*var mysql = require('mysql');*/
var bcrypt = require('bcrypt-nodejs');
var dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
var moment = require('moment');
var ad = require('../config/ad.js').ad;
var email = require('../config/gmail.'+ (process.env.NODE_ENV.trim() || "development") +'.js').email;
var Promise = require('promise');
let Permissions = require('../models/permissions');
let Powerschool = require('../models/powerschool');
let adRequests = require('../models/adRequests');
let EmailSender = require('../models/emailSender');
let Calendar = require('../models/calendar');
let PTODates = require('../models/ptodates');
let Utils = require('../models/utils');
let fs = require('fs');
let config = require("../config/configFile.js").get(process.env.NODE_ENV.trim());
// expose this function to our app using module.exports

function Pto() {
	this.addPTO = function(req,res) { //Add a PTO request to the system
		ptos = this;
		let pto_value = req.body.pto_amount;
		let pto_request_type_id = req.body.request_type;
		let submitduties = false;
		let submitrequest = false;
		let start_date = moment(req.body.pto_date);
		let end_date = req.body.pto_date_end;
		let datediff;
		let alreadySubmittedPromise = PTODates.checkPTOExists();
		alreadySubmittedPromise.then(function(PTOExists) {
			if(PTOExists) {
				req.flash("pto","success");
				res.redirect("/pto");
			} else {

			}
		});
		if(end_date != "") {
			end_date = moment(req.body.pto_date_end);
			datediff = end_date.diff(start_date, 'days');
		}
		//Get the number of days being taken

		let dateArray = [];
		let promiseAllDates = [];
		let promiseDatesExist = [];
		//Add them all to an array and factor out Weekends
		//The following snippet was made possible by Mark Bazin and viewers like you
		if(end_date != "") {
			for(x=0;x<=datediff;x++) {
				var dateAdd = moment(start_date).add(x,'days');
				if(dateAdd.day() != 6 && dateAdd.day() != 0) {
					dateArray.push(moment(dateAdd).format("YYYY-MM-DD"));
					promiseDatesExist.push(PTODates.checkPTOExists(moment(dateAdd).format("YYYY-MM-DD"),req.session.username));
				}
			}
		} else { //just in case someone only puts in one date
			dateArray.push(moment(start_date).format("YYYY-MM-DD"));
			promiseDatesExist.push(PTODates.checkPTOExists(moment(dateAdd).format("YYYY-MM-DD"),req.session.username));
		}
		let duties = [];
		let classes = [];
		let sectionArray = {};
		if(req.body.duty != undefined) { //Get all the duties that the teacher has for that day
			req.body.duty.forEach(function(item) {
				duties.push(item);
			});
			submitduties = true;
		}
		if(req.body.classChks != undefined) { //get all the classes for the teacher
			req.body.classChks.forEach(function(item) {
				classes.push(item);
			});
			submitrequest = true;
		}
		if(req.body.txtOverrideCode != "") {
			Utils.markCodeUsed(req.body.txtOverrideCode);
		}
		console.log(req.session.username);
		console.log(JSON.stringify(classes,null,2));
		Promise.all(promiseDatesExist).then(function(dateValues) { //Load up which dates have already been sumbitted to avoid double entry.
			dateValues.forEach(function(dateResults) {
				for(let key in dateResults) {
					if(dateResults[key].exists) { //If the entry already exists, then get rid of it
						let index = dateArray.indexOf(key);
						if(index !== -1) dateArray.splice(index, 1);
					}
				}
			});
			if(classes.length > 0) {
				let sectionExpression = Powerschool.getSectionExpression(classes);
				sectionExpression.then(function(sectionE) {
					const keys = Object.keys(sectionE);
					for(const key in sectionE) {
						sectionArray[sectionE[key].id] = sectionE[key].day_of_week;
					}
					dateArray.forEach(function(item) { //Molly Byrne made this easier by making me realize, the classes would go to the correct time based on the date
						promiseAllDates.push(ptos.insertPTODate(req.session.username, item, req.body.pto_notes,pto_value,pto_request_type_id, ptos, submitrequest, submitduties,classes, duties, res, sectionArray));
					});
				});
			} else {
				dateArray.forEach(function(item) { //Molly Byrne made this easier by making me realize, the classes would go to the correct time based on the date
					promiseAllDates.push(ptos.insertPTODate(req.session.username, item, req.body.pto_notes,pto_value,pto_request_type_id, ptos, submitrequest, submitduties,classes, duties, res, sectionArray));
				});
			}
			Promise.all(promiseAllDates).then(function(values) {
				if(req.body.hidIsOverPTO == 1) {
					EmailSender.sendEmailtoHR(req.session.username,start_date,end_date,req.body.pto_notes,pto_value);
				}
				ptos.sendEmailToManager(req.session.username,start_date,end_date,req.body.pto_notes,pto_value); //Send an email to the users manager and cc user
				req.flash("pto","success");
				res.redirect("/pto");
			});
		});
	},
	this.insertPTODate = function(username,date,notes,value,request_type,ptos, submitrequest, submitduties,classes, duties, res, sectionArray) {
		let currentDate = moment(new Date()).format("YYYY-MM-DD");
		return new Promise(function(fulfill, reject) {
			let insertQuery = "INSERT INTO pto_request (username, pto_date, pto_notes, pto_value, pto_request_type_id, pto_request_submitted_date) values (?,?,?,?,?,?)";
			dbconfig.getConnection(function(err, connection){ //enter the pto request into the system
				connection.query(insertQuery,[username, date, notes,value,request_type, currentDate],function(err, rows) {
					connection.release();
					if(err){
						reject(console.log({status: 1, message: 'Post Failed - PTO add: ' + err}));
						req.flash("pto","failure");
						res.redirect("/pto");
					} else {
						if(submitrequest) {ptos.addSubRequests(classes, date, rows.insertId, res, sectionArray);} //add the classes that are being missed			
						if(submitduties) {ptos.addSubDuties(rows.insertId, duties, res);} //add the duties that will be missed
						fulfill(true);
					}
				});
			});
		});
	}
	this.assignPTO = function(req,res) { //does the same as above, but is for allowing whomever is managing substitutes to add a pto request for someone else.
		ptos = this;
		var pto_value;
		var pto_request_type_id = req.body.request_type;
		var submitduties = false;
		var submitrequest = false;
		if(typeof req.body.half_day != 'undefined') {
			pto_value = .5;
		} else {
			pto_value = 1;
		}
		var duties = [];
		var classes = [];
		if(req.body.duty != undefined) {
			req.body.duty.forEach(function(item) {
				duties.push(item);
			});
			submitduties = true;
		}
		if(req.body.classChks != undefined) {
			req.body.classChks.forEach(function(item) {
				classes.push(item);
			});
			submitrequest = true;
		}
		var insertQuery = "INSERT INTO pto_request (username, pto_date, pto_notes, pto_value, pto_request_type_id, pto_approved) values (?,?,?,?,?,1)";
		dbconfig.getConnection(function(err, connection){
			connection.query(insertQuery,[req.body.pto_usernames, moment(req.body.pto_date).format("YYYY-MM-DD"), req.body.pto_notes,pto_value,pto_request_type_id],function(err, rows) {
				connection.release();
				if(err){
					res.send({status: 1, message: 'Post Failed - PTO add: ' + err});
				} else {
					if(submitrequest) {ptos.addSubRequests(classes, moment(req.body.pto_date).format("YYYY-MM-DD"), rows.insertId, res);}			
					if(submitduties) {ptos.addSubDuties(rows.insertId, duties, res);}
					res.redirect("/assignpto");
				}
			});
		});
	},
	this.addSubRequests = function(classes, date, pto_request_id, res, sectionArray) { //adds the classes being missed to the database. This shows up on the assigning subs page
		let insertQuery = "INSERT INTO pto_substitute_classes (pto_classes_class_id, pto_request_id) values (?,?)";
		let path = '/ws/schema/query/com.company.product.school.calendar_expression';
		let payload = {
			"schoolid":config.powerschool.school_id,
			"bell_day": date
		};
		let tableString = "calendar_day";
		const functionError = "Day Expression ";
		let powerschoolPromise = Powerschool.getToken();
		powerschoolPromise.then(function(token) {
			console.log(payload);
			let DayExpression = Powerschool.getData(path, payload, tableString, functionError, token, null);
			DayExpression.then(function(Expression) {
				classes.forEach(function(item) {
					fs.readFile('./data/ExpressionsTable.json', function(err,data) {
						console.log("Item for SectionVal Error: " + item);
						if(err) { console.log(err);}
						let Expressions = JSON.parse(data);
						console.log(Expressions);
						let sectionVal = sectionArray[item].replace(/\d+|[()]/g, '');
						if(ClassExists(Expression, sectionVal, Expressions)) {
							dbconfig.getConnection(function(err,connection){
								connection.query(insertQuery,[item,pto_request_id],function(err, rows) {
									connection.release();
									if(err){
										console.log({status: 1, message: 'Post Failed - PTO Substitue Add: ' + err});
									}
								});
							});
						}
					});
				});
			});
		});
	},
	this.addSubDuties = function(pto_request_id, duties, res) { //adds the duties being missed to the database
		var insertQuery = "INSERT INTO pto_substitute_duties (pto_request_id, pto_substitute_duty) values (?,?)";
		duties.forEach(function(item) {
			dbconfig.getConnection(function(err,connection){
				connection.query(insertQuery,[pto_request_id,item],function(err, rows){
					connection.release();
					if(err){
						console.log({status: 1, message: 'Post Failed - PTO Duties Add: ' + err});
					}
				});
			});
		});
	},
	this.sendEmailToManager = function(username, start_date, end_date, note, value) { //gets the users manager and then sends them an email that a request has been made
		var managerPromise = adRequests.getManager(username);
		managerPromise.then(function(info) {
			EmailSender.emailManager(info.managerUsername,username,start_date, end_date,note,value, info.givenName);
		});
	},
	this.deletePTO = function(req, res) { //delete a pto request
		var pto_id = req.body.pto_id;
		var deleteQuery = "DELETE FROM pto_request WHERE pto_request_id = ?";
		dbconfig.getConnection(function(err,connection){
			connection.query(deleteQuery,[pto_id], function(err, rows) {
				connection.release();
				if(err){
					res.send({status: 1, message: 'Delete Failed - PTO Delete: ' + err});
				} else {
					res.redirect("/pto");
				}
			});
		});
	},
	this.approvePTO = function(req,res) { //manager has approved a pto request
		var pto_id = req.body.pto_id;
		var pto = this;
		var manager = req.session.username;
		var updateQuery = "UPDATE pto_request SET pto_approved = 1 WHERE pto_request_id = ?";
		dbconfig.getConnection(function(err,connection){
			connection.query(updateQuery,[pto_id], function(err, rows) {
				connection.release();
				if(err){
					console.log({status: 1, message: 'Update Failed - PTO Approval: ' + err});
					req.flash("pto","failure");
					res.redirect("/ptoapproval");
				} else {
					pto.emailApprovalDenialOutcome("Approved",req.body.pto_username,req.body.pto_date);
					if(manager != "gmenard" || manager != "equinones") {
						Calendar.ptoCalendarInvite(manager, req.body.pto_username, req.body.pto_date);
					}
					req.flash("pto","success");
					res.redirect("/ptoapproval");
				}
			});
		});
	},
	this.denyPTO = function(req,res) { //manager has denied a pto request
		let pto = this;
		let pto_id = req.body.pto_id;
		let updateQuery = "UPDATE pto_request SET pto_approved = 3 WHERE pto_request_id = ?";
		dbconfig.getConnection(function(err,connection){
			connection.query(updateQuery,[pto_id], function(err, rows) {
				connection.release();
				if(err){
					console.log({status: 1, message: 'Update Failed - PTO Denial: ' + err});
					req.flash("pto","failure");
					res.redirect("/ptoapproval");
				} else {
					pto.emailApprovalDenialOutcome("Denied",req.body.pto_username,req.body.pto_date);
					req.flash("pto", "success");
					res.redirect("/ptoapproval");
				}
			});
		});		
	},
	this.getPTORequestTypes = function(req,res) { //Gets all of the request types from the database.
		return new Promise(function(fulfill,reject) {
			var getQuery = "SELECT * FROM pto_request_types";
			dbconfig.getConnection(function(err, connection) {
				connection.query(getQuery,function(err,result) {
					connection.release();
					if(err) {
						reject(res.send({status: 1, message: 'Get Failed - PTO Request Types: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},
	this.ptoSeedAmount = function(req,res) {
		var ptos = this;
		return new Promise(function(fulfill,reject) {
			var seedAmount;
			var getQuery = "SELECT pto_initial_seed_amount FROM pto_initial_seed where username=?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(getQuery,[req.session.username],function(err,result) {
					connection.release();
					if(err) {
						reject(res.send({status: 1, message: 'Get Failed - PTO Inital Seed: ' + err}));
					} else {
						if(result.length == 0) {
							var basePTOPromise = ptos.staffBasePTO();
							basePTOPromise.then(function(staffBasePTO){
								seedAmount = staffBasePTO;
								fulfill(seedAmount)
							});
						} else {
							seedAmount = result[0].pto_initial_seed_amount;
							fulfill(seedAmount);
						}
					}
				});
			});
		});
	},
	this.staffBasePTO = function() {
		return new Promise(function(fulfill,reject) {
			var staffBase;
			var selectQuery = "Select admin_options_value FROM admin_options WHERE admin_options_name = 'Staff PTO'";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Select Failed - Check Admin Exists: ' + err}));
					} else {
						if(!result.length) {
							fulfill(result[0].admin_options_value);
						} else {
							fulfill(20); //I set this as a default in case someone accesses the PTO page before stuff is set up
						}
					}
				});
			});
		});
	},
	this.facultyBasePTO = function() {
		return new Promise(function(fulfill,reject) {
			var staffBase;
			var selectQuery = "Select admin_options_value FROM admin_options WHERE admin_options_name = 'Faculty PTO'";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Select Failed - Check Admin Exists: ' + err}));
					} else {
						if(!result.length) {
							fulfill(result[0].admin_options_value);
						} else {
							fulfill(10); //I set this as a default in case someone accesses the PTO page before stuff is set up
						}
					}
				});
			});
		});
	},
	this.getAdtlPTO = function(username) {
		return new Promise(function(fulfill,reject) {
			let FiscalBegin = getCurrentFiscalYear();
			let BeginDate = FiscalBegin + "-" + "07-01";
			let EndDate = FiscalBegin+ 1 + "-" + "06-30";
			let selectQuery = "Select SUM(num_adtl_days) as num_adtl_days FROM pto_adtl_days WHERE username = ? AND date BETWEEN ? AND ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,[username,BeginDate,EndDate],function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Select Failed - Check Admin Exists: ' + err}));
					} else {
						if(result.length > 0) {
							fulfill(result[0].num_adtl_days);
						} else {
							fulfill(0); //I set this as a default in case someone accesses the PTO page before stuff is set up
						}
					}
				});
			});
		});	
	}
	this.calculateTotalPTO = function(req,res,ptoSeed,pendingPto,deniedPto,allUsers,canDelete,request_types) { //None of this applies to Henry Sustaita, he is a fringe case that would be too complicated to code for.
		let ptos = this;
		let FiscalBegin = getCurrentFiscalYear();
		let BeginDate = FiscalBegin + "-" + "07-01";
		let EndDate = FiscalBegin+ 1 + "-" + "06-30";
		let login;
		let daysUsed = 0;
		let username;
		if(req.session.isAuthenticated) { //Checks if user is authenticated and passes the login value to ejs
			login = true;
		} else {
			login = false;
		}
		if(canDelete) {
			if(req.body.pto_usernames != "" && req.body.pto_usernames !== undefined) {
				username = req.body.pto_usernames;
			} else {
				username = req.session.username;
			}
		} else {
			username = req.session.username;
		}
		let usernames = {"session_user": req.session.username, "selected_username": username};
		let isFacultyPromise = Permissions.isFaculty(username);
		let adtlPTOPromise = this.getAdtlPTO(username);
		//let facultyBasePTOPromise = ptos.facultyBasePTO(req,res);
		Promise.all([isFacultyPromise, adtlPTOPromise]).then(values => {
			let isFaculty = values[0];
			let adtlPTO = values[1];
			//let facultyBasePTO = values[2];
			let getQuery = "SELECT pto_request_id, pto_date, pto_notes, pto_value FROM pto_request where username=? AND pto_date BETWEEN ? AND ? AND pto_approved = 1 AND pto_request_type_id IN (1,2)"; //Grab all of the users PTO requests and send them to the template
			dbconfig.getConnection(function(err, connection){
			    connection.query(getQuery,[username,BeginDate,EndDate],function(err, result) {
			    	connection.release();
			    	if(err){
			    		res.send({status: 1, message: 'Get Failed - PTO calcuate total PTO: ' + err});
			    	} else {
			    		for(i=0;i<result.length;i++) {
			    			daysUsed += result[i].pto_value;
			    		}
			    		allUsers= allUsers.sort(); //Sort all the users so it is alphabetical
		    			//if(isFaculty) {ptoSeed = facultyBasePTO;}
		    			ptoSeed += adtlPTO;
			    		res.render('pto.ejs', {
			    			pto : result,
			    			pendingPto : pendingPto,
			    			deniedPto : deniedPto,
			    			login : login,
			    			moment : moment,
			    			days : daysUsed,
			    			totalPTO : ptoSeed,
			    			canDelete :canDelete,
			    			pto_username : allUsers,
			    			usernames: usernames,
			    			request_types : request_types,
			    			flash: req.flash('pto'),
			    			isFaculty : isFaculty
			    		});
			    	}
			    });
			});
		});
	},
	this.getPendingPTO = function(req,res,canDelete) {
		return new Promise(function(fulfill,reject) {
			var FiscalBegin = getCurrentFiscalYear();
			var BeginDate = FiscalBegin + "-" + "07-01";
			var EndDate = FiscalBegin+ 1 + "-" + "06-30";
			var username;
			if(canDelete) {
				if(req.body.pto_usernames != "" && req.body.pto_usernames !== undefined) {
					username = req.body.pto_usernames;
				} else {
					username = req.session.username;
				}
			} else {
				username = req.session.username;
			}
			var getQuery = "SELECT pto_request_id, pto_date, pto_notes, pto_value FROM pto_request where username=? AND pto_date BETWEEN ? AND ? AND pto_approved = 0"; //Grab all of the users PTO requests and send them to the template
			dbconfig.getConnection(function(err, connection){
			    connection.query(getQuery,[username,BeginDate,EndDate],function(err, result) {
			    	connection.release();
			    	if(err){
			    		reject(res.send({status: 1, message: 'Get Failed - PTO calcuate total PTO: ' + err}));
			    	} else {
			    		fulfill(result);
			    	}
			    });
			});
		});
	},
	this.getDeniedPTO = function(req,res,canDelete) {
		return new Promise(function(fulfill,reject) {
			var FiscalBegin = getCurrentFiscalYear();
			var BeginDate = FiscalBegin + "-" + "07-01";
			var EndDate = FiscalBegin+ 1 + "-" + "06-30";
			var username;
			if(canDelete) {
				if(req.body.pto_usernames != "" && req.body.pto_usernames !== undefined) {
					username = req.body.pto_usernames;
				} else {
					username = req.session.username;
				}
			} else {
				username = req.session.username;
			}			
			var getQuery = "SELECT pto_request_id, pto_date, pto_notes, pto_value FROM pto_request where username=? AND pto_date BETWEEN ? AND ? AND pto_approved = 3"; //Grab all of the users PTO requests and send them to the template
			dbconfig.getConnection(function(err, connection){
			    connection.query(getQuery,[username,BeginDate,EndDate],function(err, result) {
			    	connection.release();
			    	if(err){
			    		reject(res.send({status: 1, message: 'Get Failed - PTO calcuate total PTO: ' + err}));
			    	} else {
			    		fulfill(result);
			    	}
			    });
			});
		});
	},
	this.SeedInitialPTOLogin = function(username) { //This will set up the inital seed of 20 PTO days when someone first logs in if their seed is empty
		let ptos = this;
		return new Promise(function(fulfill,reject) {
			let isFacultyPromise = Permissions.isFaculty(username);
			isFacultyPromise.then(function(isFaculty) {
				let basePTOSeed;
				if(isFaculty) {
					basePTOSeed = ptos.facultyBasePTO();
				} else {
					basePTOSeed = ptos.staffBasePTO();
				}
				basePTOSeed.then(function(basePTO) {
					let insertQuery = "CALL pto_seed_login(?,?)";
					dbconfig.getConnection(function(err,connection) {
						connection.query(insertQuery,[username, basePTO],function(err,result) {
							connection.release();
							if(err) {
								reject(console.log({status: 1, message: 'Post Failed - PTO Inital Seed: ' + err}));
							} else {
								fulfill(true);
							}
						})
					});
				});
			});
		});
	},
	this.getPTOApprovers = function() {
		return new Promise(function(fulfill,reject) {
			var groups = [];
			var getQuery = "SELECT admin_options_value FROM admin_options WHERE admin_options_name = 'PTO Approver'"; //Grab all of the users PTO requests and send them to the template
			dbconfig.getConnection(function(err, connection){
			    connection.query(getQuery,function(err, result) {
			    	connection.release();
			    	if(err){
			    		reject(console.log({status: 1, message: 'Get Failed - Get PTO Approvers: ' + err}));
			    	} else {
			    		for(var x=0;x<=result.length-1;x++){
			    			groups.push(result[x].admin_options_value);
			    		}
			    		fulfill(groups);
			    	}
			    });
			});
		});			
	}
	this.getPTOApprovalsNeeded = function(req, res) {
		var ptos = this;
		var login;
		var groupApprovals = [];
		var usersInGroupApprovals = [];
		var promiseAggregate = [];
		let userPromiseAggregate = [];
		var usersPromise;
		if(req.session.isAuthenticated) { //Checks if user is authenticated and passes the login value to ejs
			login = true;
		} else {
			login = false;
		}
		var ptoApproversPromise = ptos.getPTOApprovers();
		var getDirectReports = adRequests.getDirectReports(req.session.username);
		getDirectReports.then(function(directReports){
			directReports.forEach(function(results) {
				userPromiseAggregate.push(adRequests.getUserUsernameFromCN(results));
			});
			Promise.all(userPromiseAggregate).then(function(usernames) {
				var getQuery = "SELECT pto_request_id, pto_date, pto_notes, pto_request_types.pto_request_type_name, pto_value, username FROM pto_request ";
					getQuery += "INNER JOIN pto_request_types ON pto_request_types.pto_request_type_id = pto_request.pto_request_type_id ";
					getQuery += "where username IN('"+usernames.join("','")+"') AND pto_approved = 0 ORDER BY username, pto_date"; //Grab all of the users PTO requests and send them to the template
				dbconfig.getConnection(function(err, connection){
					connection.query(getQuery,function(err, result) {
				    	connection.release();
				    	if(err){
				    		console.log({status: 1, message: 'Get Failed - PTO get approvals Needed: ' + err});
				    	} else {
							res.render('ptoapprovals.ejs', {
				    			login : login,
				    			moment : moment,
				    			flash: req.flash('pto'),
				    			pto: result
				    		});
				    	}
				    });
				});
			});
		});
	},
	/*Move this to Email Sender at some point*/
	this.emailApprovalDenialOutcome = function(ApprovalStatus,username,pto_date) {
		let msg;
		let emailaddress = username + config.emailDomain;
		let DateDay = new Date(pto_date).getDay();
		pto_date = moment(new Date(pto_date)).format("YYYY-MM-DD");
		if(ApprovalStatus == "Denied") {
			msg = "Your pto request for " + pto_date + " has been denied. Please contact your manager for any questions you may have.\r\n"
		} else if(ApprovalStatus == "Approved") {
			msg = "Your pto request for " + pto_date + " has been Approved. Please contact your manager for any questions you may have.\r\n"
		}
		let mailOptions = {
			text: msg,
			from: "PTO Request Status",
			to: emailaddress,
			subject: "Update on your PTO request status"
		}
		email.sendMail(mailOptions, (err) => {
			if (err) {
				return console.log("Email Approval Denial Outcome Err: "+ err);
			}
		});
	},
	this.CreateBlackoutOverride = function() {
		let overrideCode = Utils.GenerateCode();
		var insertQuery = "INSERT INTO pto_override_code (override_code) VALUES (?) ";
		dbconfig.getConnection(function(err, connection){ //enter the pto request into the system
			connection.query(insertQuery,[overrideCode],function(err, rows) {
				connection.release();
				if(err){
					console.log({status: 1, message: 'Post Failed - Create Override: ' + err});
				} else {
					res.render('overrideCode.ejs', {
		    			login : login,
		    			code: overrideCode
		    		});
				}
			});
		});
	}
}

function getCurrentFiscalYear() {
	var FiscalBegin,
	    CurrentDate,
	    CurrentMonth;
	CurrentDate = new Date();
	CurrentMonth = CurrentDate.getMonth();
	if(CurrentMonth >=6 && CurrentMonth<=12) {
		FiscalBegin = CurrentDate.getFullYear();
	} else {
		FiscalBegin = CurrentDate.getFullYear() - 1;
	}
	return FiscalBegin;
}

function ClassExists(dayExpression, sectionExpression, ExpressionsTable) {
	let ExpressionsTruth;
	const DAY_TO_VAL = {A: 0, B: 1, C: 2, D: 3, E: 4};
	let dayVal = DAY_TO_VAL[dayExpression[0].cycle_day_letter];
	const keys = Object.keys(ExpressionsTable.Expressions)
	for(const key in ExpressionsTable.Expressions) {
		const val = Object.keys(ExpressionsTable.Expressions[key]);
		for(const val in ExpressionsTable.Expressions[key]) {
			if(val == sectionExpression) {
				ExpressionsTruth = ExpressionsTable.Expressions[key][val];
			}
		}
	}
	if(ExpressionsTruth[dayVal]) {
		return true;
	}
}
module.exports = new Pto();