var bcrypt = require('bcrypt-nodejs');
var dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
var moment = require('moment');
var ad = require('../config/ad.js').ad;
var Promise = require('promise');
var Permissions = require('../models/permissions');

function PTODates() {
	this.addBlackoutDate = function(req,res) {
		return new Promise(function(fulfill, reject) {
			var insertQuery = "INSERT INTO pto_blackout_dates (pto_blackout_date, pto_blackout_date_description) VALUES (?,?)";
			dbconfig.getConnection(function(err, connection) {
				connection.query(insertQuery,[moment(req.body.txtBlackoutDate).format("YYYY-MM-DD"),req.body.txtBlackoutDescription],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Add Failed - Insert Blackout Date: ' + err});
					} else {
						res.redirect('/blackoutDates');	
					}
				});
			});
		});
	},
	this.updateBlackoutDate = function(req,res) {
		return new Promise(function(fulfill, reject) {
			var updateQuery = "UPDATE pto_blackout_dates set pto_blackout_date = ?, pto_blackout_date_description = ? WHERE pto_blackout_date_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(updateQuery,[moment(req.body.date).format("YYYY-MM-DD"), req.body.description, req.body.id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Update Failed - Update Blackout Date: ' + err});
					} else {
						res.redirect('/blackoutDates');
					}
				})
			});
		});
	},
	this.deleteBlackoutDate = function(req,res) {
		return new Promise(function(fulfill, reject) {
			var deleteQuery = "DELETE FROM pto_blackout_dates WHERE pto_blackout_date_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(deleteQuery,[req.body.id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Delete Failed - Delete Blackout Date: ' + err});
					} else {
						res.redirect('/blackoutDates');
					}
				})
			});
		});
	},
	this.getBlackoutDates = function(req,res) {
		var selectQuery = "Select * FROM pto_blackout_dates ORDER BY pto_blackout_date ASC";
		blackoutDates = {};
		dbconfig.getConnection(function(err, connection) {
			connection.query(selectQuery,function(err, result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Select Failed - Get Blackout Dates: ' + err});
				} else {
					for(var x=0;x<=result.length-1;x++) {
						blackoutDates[result[x].pto_blackout_date_id] = {'date': moment(result[x].pto_blackout_date).format("MM/DD/YYYY"), 'description': result[x].pto_blackout_date_description};
					}
					res.render('blackoutDates.ejs', {
						login : true,
						results : blackoutDates,
						moment: moment
					});
				}
			});
		});
	},
	this.checkIfPTOIsBlackout = function(pto_date, pto_date_end) {
		return new Promise(function(fulfill, reject) {
			if(pto_date_end == "") { pto_date_end = pto_date;}
			var selectQuery = "SELECT * FROM pto_blackout_dates WHERE pto_blackout_date BETWEEN ? AND ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery, [pto_date, pto_date_end], function(err, result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Select Failed - Check date: ' + err}));
					} else {
						if(result.length > 0) {
							fulfill(true);
						} else {
							fulfill(false);
						}	
					}
				});
			})
		});
	},
	this.searchPTODate = function(req,res) {
		return new Promise(function(fulfill, reject) {
			var info ={};
			var pto_date = req.body.date || null;
			if(pto_date == null ){
				var selectQuery = "Select pto_request_id, username, pto_date FROM pto_request WHERE pto_date IS NULL";
			} else {
				var selectQuery = "Select pto_request_id, username, pto_date FROM pto_request WHERE pto_date = ?";
			}
			var canDeletePromise = Permissions.canDeletePTO(req,res);
			canDeletePromise.then(function(canDelete) {
				dbconfig.getConnection(function(err, connection) {
					connection.query(selectQuery,[pto_date],function(err, result) {
						connection.release();
						if(err) {
							reject(console.log({status: 1, message: 'Select Failed - Get PTO Reset Date: ' + err}));
						} else {
							var results = {'ptos':result};
							fulfill(results);
						}
					});
				});
			});
		});
	},
	this.checkPTOExists = function(pto_date, username) {
		return new Promise(function(fulfill, reject) {
			let dateReturn = {};
			let selectQuery = "Select pto_request_id FROM pto_request WHERE pto_date = ? AND username = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,[pto_date, username],function(err, result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Select Failed - Get PTO Exists: ' + err}));
					} else {
						if(result.length > 0) {
							dateReturn[pto_date] = {'exists':true};
							fulfill(dateReturn);
						} else {
							dateReturn[pto_date] = {'exists':false};
							fulfill(dateReturn);
						}	
					}
				});
			});
		});		
	}
}

module.exports = new PTODates();