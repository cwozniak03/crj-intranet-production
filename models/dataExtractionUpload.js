let dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
let Promise = require('promise');
let PowerSchool = require('../models/powerschool');
let config = require("../config/configFile.js").get(process.env.NODE_ENV.trim());

function DataExtract() {
	this.getStudentInfo = function(LastID, token) {
		return new Promise(function(fulfill, reject) {
			let dataExtract = this;
			let values = [];
			let LastInsertedID = 0;
			let path = '/ws/schema/query/com.company.product.school.students';
			let payload = {
				"last_id": LastID
			};
			let tableString = "students";
			const functionError = "Student Data ";
			const functionRerun = "FillStudents";
			let studentPromise = PowerSchool.getData(path, payload, tableString, functionError, token, functionRerun); //Get the Info and then Insert it
			studentPromise.then(function(students) {
				students.forEach(function(student){
					values.push([student.id, student.student_number, student.gender, student.ethnicity, student.lastfirst, student.last_name, student.first_name, student.enroll_status]);
					if(parseInt(student.id) > parseInt(LastInsertedID)) {
						LastInsertedID = student.id;
					}
				});
				let insertQuery = "INSERT INTO student_data (student_powerschool_id, student_number, gender, ethnicity, lastfirst, last_name, first_name, enroll_status) values ?";
				dbconfig.getConnection(function(err, connection){
					connection.query(insertQuery,[values],function(err, rows) {
						connection.release();
						if(err){
							reject(console.log("Err Insert Student Info Data Extract: " + err));
						} else {
							fulfill(LastInsertedID);
						}
					});
				});
			});
		});
	},
	this.getStudentGrades = function(LastID, token) {
		return new Promise(function(fulfill, reject) {
			let dataExtract = this;
			let values = [];
			let LastInsertedID = 0;
			const path = '/ws/schema/query/com.company.product.school.student_grades';
			const payload = {
				"last_id": LastID
			};
			let tableString = "storedgrades";
			const functionError = "Student Grades ";
			let gradePromise = PowerSchool.getData(path, payload, tableString, functionError, token, null);
			gradePromise.then(function(grades) {
				if(grades == "retry") {
					reject("retry");
				} else {
					grades.forEach(function(grade) {
						values.push([grade.percent, grade.teacher_name, grade.gpa_points, grade.grade, grade.course_number, grade.credit_type, grade.earnedcrhrs,
								grade.grade_level, grade.storecode, grade.potentialcrhrs, grade.studentid, grade.dcid, grade.course_name, grade.excludefromgpa, 
								grade.datestored, grade.termid, grade.gpa_addedvalue]);
						if(parseInt(grade.dcid) > parseInt(LastInsertedID)) {
							LastInsertedID = grade.dcid;
						}
					});
					let insertQuery = "INSERT INTO student_grades (percent, teacher_name, gpa_points, grade, course_number, credit_type, earnedcrhrs, grade_level, ";
					insertQuery += "storecode, potentialcrhrs, student_id, powerschool_id, course_name, excludefromgpa, datestored, term_id, gpa_addedvalue) values ?";
					dbconfig.getConnection(function(err, connection){
						connection.query(insertQuery,[values],function(err, rows) {
							connection.release();
							if(err){
								reject(console.log("Err Insert Student Grades Data Extract: " + err));
							} else {
								fulfill(LastInsertedID);
							}
						});
					});
				}
			});
		});
	},
	this.runGPASqlFunction = function() {
		return new Promise(function(fulfill, reject) {
			let getQuery = 'CALL gpa_inserts()'; 
			dbconfig.getConnection(function(err, connection){
				connection.query(getQuery,function(err, rows) {
					connection.release();
					if(err){
						reject(console.log("Err run GPA Function: " + err));
					} else {
						fulfill(true);
					}
				});
			});
		});
	},
	this.runClassGPACalculations = function() {
		let dataExtracts = this;
		let ClassesPromise = this.getAllClasses();
		let functionCall = 'CALL get_class_GPA(?)';
		ClassesPromise.then(function(Classes) {
			Object.keys(Classes).forEach(function(key) {
				dbconfig.getConnection(functionCall,[Classes[key].Class], function(err,rows) {
					if(err){
						console.log("Err run GPA Class Calculations: " + err);
					}
				});
			});
		});
	},
	this.runClassGradeFunction = function() {
		return new Promise(function(fulfill, reject) {
			let getQuery = 'CALL calculate_current_grades()'; 
			console.log("Calculating Grades");
			dbconfig.getConnection(function(err, connection){
				connection.query(getQuery,function(err, rows) {
					connection.release();
					if(err){
						reject(console.log("Err run Calculate Current Grade Function: " + err));
					} else {
						fulfill(true);
					}
				});
			});
		});
	},	
	this.getAllClasses = function() {
		return new Promise(function(fulfill, reject) {
			let getQuery = 'SELECT Class FROM class_gpa_values'; 
			dbconfig.getConnection(function(err, connection){
				connection.query(getQuery,function(err, rows) {
					connection.release();
					if(err){
						reject(console.log("Err get Classes for GPA: " + err));
					} else {
						fulfill(rows);
					}
				});
			});
		});	
	},
	this.addAllAssignmentsCurYear = function(newID, LastID, token) {
		return new Promise(function(fulfill, reject) {
			let dataExtract = this;
			let LastInsertedID = 0;
			let values = [];
			let assigmnentPromise = PowerSchool.getCurrentAssignments(newID, LastID, token);
			assigmnentPromise.then(function(assignments) {
				assignments.forEach(function(assignment) {
					values.push([assignment.assignmentscoreid, assignment.studentsdcid, assignment.ismissing, assignment.isincomplete, assignment.totalpointvalue, assignment.scorepercent, 
								assignment.scorepoints, assignment.storelettergrade, assignment.scoreentrydate, assignment.isabsent, 
								assignment.iscollected, assignment.isexempt, assignment.islate, assignment.scoretype, 
								assignment.sectionsdcid, assignment.assignmentid, assignment.duedate,
								assignment.district_category_name, assignment.storecode]);
					if(parseInt(assignment.assignmentscoreid) > parseInt(LastInsertedID)) {
						LastInsertedID = assignment.assignmentscoreid;
					}
				});
				let insertQuery = "INSERT INTO student_assignment_grades (assignment_score_id, Student_ID, IsMissing, IsIncomplete, TotalPoints, ";
				insertQuery += "ScorePercent, ScorePoints, ScoreLetterGrade, ScoreEntryDate, IsAbsent, IsCollected, IsExempt, IsLate, ";
				insertQuery += "Score_Type, Section_ID, District_Category_Name, StoreCode, Assignment_ID, Due_Date) values ?";
				dbconfig.getConnection(function(err, connection){
					connection.query(insertQuery,[values],function(err, rows) {
						connection.release();
						if(err){
							reject(console.log("Err Insert Student Asignment Grades Data Extract: " + err));
						} else {
							fulfill(LastInsertedID);
						}
					});
				});
			}).catch(function(rejectPowerschool) {
				reject(rejectPowerschool);
			});
		});		
	},

	this.getStudentAbsenses = function(LastID, token) {
		return new Promise(function(fulfill, reject) {
			let dataExtract = this;
			let values = [];
			let LastInsertedID = 0;
			const path = '/ws/schema/query/com.company.product.school.all_absenses';
			const payload = {
				"school_id": config.powerschool.school_id,
				"last_id": LastID
			};
			let tableString = "attendance";
			const functionError = "Student Absenses ";
			const functionRerun = "FillAbsenses";
			let attendancePromise = PowerSchool.getData(path, payload, tableString, functionError, token, functionRerun);
			attendancePromise.then(function(attendances) {
				attendances.forEach(function(attendance) {
					values.push([attendance.id, attendance.att_date, attendance.description, attendance.teacher_lastfirst, attendance.student_lastfirst,
							attendance.student_number, attendance.course_name, attendance.credittype, attendance.start_time, attendance.yearid]);
					if(parseInt(attendance.id) > parseInt(LastInsertedID)) {
						LastInsertedID = attendance.id;
					}
				});
				let insertQuery = "INSERT INTO student_attendance (powerschool_attendance_id, att_date, description, teacher_last_first, ";
				insertQuery += "student_last_first, student_number, course_name, credittype, start_time, year_id) values ?";
				dbconfig.getConnection(function(err, connection){
					connection.query(insertQuery,[values],function(err, rows) {
						connection.release();
						if(err){
							reject(console.log("Err Insert Student Absense Data Extract: " + err));
						} else {
							fulfill(LastInsertedID);
						}
					});
				});
			});
		});
	},
	this.getCourses = function(LastID) {
		return new Promise(function(fulfill, reject) {
			let dataExtract = this;
			let values = [];
			let LastInsertedID = 0;
			let path = '/ws/schema/query/com.company.product.school.courses';
			let payload = {
				"school_id": config.powerschool.school_id,
				"last_id": LastID
			};
			let tableString = "courses";
			const functionError = "Get Courses ";
			const functionRerun = "FillCourses";
			let powerschoolPromise = powerschool.getToken();
			powerschoolPromise.then(function(token) {			
				let coursePromise = PowerSchool.getData(path, payload, tableString, functionError, token, functionRerun);
				coursePromise.then(function(courses) {
					courses.forEach(function(course) {
						values.push([course.id, course.course_name, course.course_number, course.credit_type]);
						if(parseInt(course.id) > parseInt(LastInsertedID)) {
							LastInsertedID = course.id;
						}
					});
					let insertQuery = "INSERT INTO courses (ps_course_id, Course_Name, Course_Number, Credit_Type) values ?";
					dbconfig.getConnection(function(err, connection){
						connection.query(insertQuery,[values],function(err, rows) {
							connection.release();
							if(err){
								reject(console.log("Err Insert Course Data Extract: " + err));
							} else {
								fulfill(LastInsertedID);
							}
						});
					});
				});
			});
		});
	},
	this.getTeachers = function(LastID) {
		return new Promise(function(fulfill, reject) {
			let dataExtract = this;
			let values = [];
			let LastInsertedID = 0;
			let path = '/ws/schema/query/com.company.product.school.teachers';
			let payload = {
				"school_id": config.powerschool.school_id,
				"last_id": LastID
			};
			let tableString = "teachers";
			const functionError = "Get Teachers ";
			const functionRerun = "FillTeachers";
			let powerschoolPromise = powerschool.getToken();
			powerschoolPromise.then(function(token) {
				let teacherPromise = PowerSchool.getData(path, payload, tableString, functionError, token, functionRerun);
				teacherPromise.then(function(teachers) {
					teachers.forEach(function(teacher) {
						values.push([teacher.id, teacher.lastfirst]);
						if(parseInt(teacher.id) > parseInt(LastInsertedID)) {
							LastInsertedID = teacher.id;
						}
					});
					let insertQuery = "INSERT INTO teachers (teacher_number, teacher_last_first) values ?";
					dbconfig.getConnection(function(err, connection){
						connection.query(insertQuery,[values],function(err, rows) {
							connection.release();
							if(err){
								reject(console.log("Err Insert Teacher Data Extract: " + err));
							} else {
								fulfill(LastInsertedID);
							}
						});
					});
				});
			});
		});
	},
	this.getStandardizedTests = function(LastID) {
		return new Promise(function(fulfill, reject) {
			let dataExtract = this;
			let values = [];
			let LastInsertedID = 0;
			let path = '/ws/schema/query/com.company.product.school.standardized_test_scores';
			let payload = {
				"school_id": config.powerschool.school_id,
				"last_id": LastID
			};
			let tableString = "test";
			const functionError = "Get Standardized Test Scores ";
			const functionRerun = "FillStandardizedTestScores";
			let powerschoolPromise = powerschool.getToken();
			powerschoolPromise.then(function(token) {
				let StandardizedTestPromise = PowerSchool.getData(path, payload, tableString, functionError, token, functionRerun);
				StandardizedTestPromise.then(function(standardized_tests) {
					standardized_tests.forEach(function(test) {
						values.push([test.student_test_id, test.test_name, test.testscore_name, test.studentid, test.numscore, test.percentscore, test.student_number]);
						if(parseInt(test.student_test_id) > parseInt(LastInsertedID)) {
							LastInsertedID = test.student_test_id;
						}
					});
					let insertQuery = "INSERT INTO standardized_tests (ps_student_test_score_id, test_name, test_score_name, studentid, numscore, percentscore, student_number) values ?";
					dbconfig.getConnection(function(err, connection){
						connection.query(insertQuery,[values],function(err, rows) {
							connection.release();
							if(err){
								reject(console.log("Err Insert Standardized Test Data Extract: " + err));
							} else {
								fulfill(LastInsertedID);
							}
						});
					});
				});
			});
		});
	},
	this.getSections = function(LastID, token) {
		return new Promise(function(fulfill, reject) {
			let dataExtract = this;
			let values = [];
			let LastInsertedID = 0;
			let path = '/ws/schema/query/com.company.product.school.sections';
			let payload = {
				"school_id": config.powerschool.school_id,
				"last_id": LastID
			};
			let tableString = "sections";
			const functionError = "Get Sections ";
			const functionRerun = "FillSections";
			let SectionPromise = PowerSchool.getData(path, payload, tableString, functionError, token, functionRerun);
			SectionPromise.then(function(sections) {
				sections.forEach(function(section) {
					values.push([section.id, section.course_number, section.teacher, section.expression]);
					if(parseInt(section.id) > parseInt(LastInsertedID)) {
						LastInsertedID = section.id;
					}
				});
				let insertQuery = "INSERT INTO sections (ps_section_id, Course_Number, teacher_id, Expression) values ?";
				dbconfig.getConnection(function(err, connection){
					connection.query(insertQuery,[values],function(err, rows) {
						connection.release();
						if(err){
							reject(console.log("Err Insert Section Data Extract: " + err));
						} else {
							fulfill(LastInsertedID);
						}
					});
				});
			});
		});
	},
	this.getMiddleSchool = function(LastID, token) {
		return new Promise(function(fulfill, reject) {
			let dataExtract = this;
			let values = [];
			let LastInsertedID = 0;
			let path = '/ws/schema/query/com.company.product.school.middle_schools';
			let payload = {
				"school_id": config.powerschool.school_id,
				"last_id": LastID
			};
			let tableString = "u_def_ext_students";
			const functionError = "Get Middle Schools ";
			const functionRerun = "FillMiddleSchools";
			let SectionPromise = PowerSchool.getData(path, payload, tableString, functionError, token, functionRerun);
			MiddleSchoolPromise.then(function(middleschools) {
				middleschools.forEach(function(middleschool) {
					values.push([middleschool.studentdcid, middleschool.course_number]);
					if(parseInt(middleschool.studentdcid) > parseInt(LastInsertedID)) {
						LastInsertedID = middleschool.studentdcid;
					}
				});
				let insertQuery = "INSERT INTO student_middle_school (student_id, middle_school_name) values ?";
				dbconfig.getConnection(function(err, connection){
					connection.query(insertQuery,[values],function(err, rows) {
						connection.release();
						if(err){
							reject(console.log("Err Insert Middle School Data Extract: " + err));
						} else {
							fulfill(LastInsertedID);
						}
					});
				});
			});
		});
	},
	this.getEnrollment = function(LastID, token) {
		return new Promise(function(fulfill, reject) {
			let dataExtract = this;
			let values = [];
			let LastInsertedID = 0;
			let path = '/ws/schema/query/com.company.product.school.reenrollment';
			let payload = {
				"school_id": config.powerschool.school_id,
				"last_id": LastID
			};
			let tableString = "reenrollments";
			const functionError = "Get Enrollment ";
			const functionRerun = "FillEnrollment";
			let EnrollmentPromise = PowerSchool.getData(path, payload, tableString, functionError, token, functionRerun);
			EnrollmentPromise.then(function(reenrollments) {
				reenrollments.forEach(function(reenrollment) {
					values.push([reenrollment.dcid, reenrollment.studentid, reenrollment.entrydate, reenrollment.entrycode, reenrollment.exitdate, reenrollment.exitcode, reenrollment.grade_level]);
					if(parseInt(reenrollment.dcid) > parseInt(LastInsertedID)) {
						LastInsertedID = reenrollment.dcid;
					}
				});
				let insertQuery = "INSERT INTO reenrollment (ps_reenrollment_id, studentid, entrydate, entrycode, exitdate, exitcode, grade_level) values ?";
				dbconfig.getConnection(function(err, connection){
					connection.query(insertQuery,[values],function(err, rows) {
						connection.release();
						if(err){
							reject(console.log("Err Insert ReEnrollment Extract: " + err));
						} else {
							fulfill(LastInsertedID);
						}
					});
				});
			});
		});
	}	
}
module.exports = new DataExtract();