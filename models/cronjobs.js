// load up the user model
let dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
let config = require("../config/configFile.js").get(process.env.NODE_ENV.trim());
let moment = require('moment');
let Promise = require('promise');
let cron = require('cron');
let DataExtractTruncateTables = require('../models/dataExtractTruncateTables');
let DataExtractGetIDs = require('../models/dataExtractGetIDs');
let PH = require('../models/ph');
let PowerschoolSimple = require('../models/powerschoolSimplePulls');
let EmailSender = require('../models/emailSender');
let DataFills = require('../models/dataFills')
// expose this function to our app using module.exports

function CronJobs() {	
	this.PTOReset = function(month,day) {
		var PTOResetCron = cron.job('15 5 '+day+' */'+month+' *', function() {
			console.log("PTO Reset schedule started");
			dbconfig.getConnection(function(err, connection) {
				connection.query('CALL fill_pto_totals_curr_fiscal_year',function(err, result) {
					if(err) {
						console.log({status: 1, message: 'Fiscal Year Reset Error: ' + err});
					} else {
						dbconfig.getConnection(function(err,connection) {
							connection.query('CALL update_seed',function(err, result) {
								if(err) {
									console.log({status: 1, message: 'PTO Seed Reset Error: ' + err});
								} else {
									console.log("Everything is reset!");
								}
							});
						});
					}
				});
			});
		}, {
			scheduled:true,
			timezone: "America/Chicago"
		});
		PTOResetCron.start();
	},
	this.getTardies = function() {
		let GetTardiesJob = cron.job('30 16 * * 1-5', function() {
			console.log("Running the tardies.");
			let today = new Date();
			let date = moment(today).format("YYYY-MM-DD");
			let studentEmailArray = [];
			let path = '/ws/schema/query/com.company.product.school.get_current_first_period';
			let payload = {
				"bell_day":date
			};
			let tableString = "calendar_day";
			const functionError = "Get First Period ID ";
			let powerschoolPromise = PowerschoolSimple.getToken();
			powerschoolPromise.then(function(token) {
				let FirstPeriodIDPromise = PowerschoolSimple.getData(path, payload, tableString, functionError, token, null);
				let today = new Date();
				let date = moment(today).format("YYYY-MM-DD");
				let currentYear = getCurrentSchoolYearBegins();
				let yearID = parseInt(currentYear.toString().slice(-2))+10;
				let pathTardies = '/ws/schema/query/com.company.product.school.get_all_tardies';
				var payloadTardies = {
					"att_day":date,
					"year_id":yearID
				};
				let tableStringTardies = "attendance";
				const functionErrorTardies = "Get Tardies Daily ";
				let tardiesPromise = PowerschoolSimple.getData(pathTardies, payloadTardies, tableStringTardies, functionErrorTardies, token, null);
				Promise.all([FirstPeriodIDPromise, tardiesPromise]).then(values => {
					let tardies = values[1];
					let firstPeriodID = values[0];
					if(firstPeriodID != "NoClasses") {
						let splitTardiesPromise = PH.splitTardiesToGroups(tardies,firstPeriodID);
						splitTardiesPromise.then(function(splitTardies) {
							for(let i=0;i<splitTardies.PHTardies.length;i++) {
								PH.addTardyPH(splitTardies.PHTardies[i],false);
								studentEmailArray.push(splitTardies.PHTardies[i].student_schoolemail);
							}
							//Send the tardies email as one BCC email instead of mass emails.
							EmailSender.emailTardies(studentEmailArray);
							let ThirdsPromise = PH.ThirdFirstTardy(splitTardies.FirstTardies);
							ThirdsPromise.then(function(ThirdsTardies) {
								let promises = [];
								//Update the first Tardies, first, so that we avoid deadlock issues
								for(x=0;x<ThirdsTardies.Thirds.length;x++) {
									let updateFirstTardies = PH.UpdateFirstTardiesTagged(ThirdsTardies.Thirds[x].student_web_id);
									promises.push(updateFirstTardies);
								}
								//We have to do promise all so that we don't fulfill before the promises have finished.
								Promise.all(promises).then(values => {
									//Loop through the Thirds again, and Add a PH and a First PH
									let addPromises = [];
									for(y=0;y<ThirdsTardies.Thirds.length;y++) {
										PH.addTardyPH(ThirdsTardies.Thirds[y],true);
										let addFirstTardiesPromise = PH.addFirstPeriodTardy(ThirdsTardies.Thirds[y],1);
										addPromises.push(addFirstTardiesPromise);
									}
									for(z=0;z<ThirdsTardies.NonThirds.length;z++) {
										let addFirstTardiesNonThirdsPromise = PH.addFirstPeriodTardy(ThirdsTardies.NonThirds[z],0);
										addPromises.push(addFirstTardiesNonThirdsPromise);
									}
									Promise.all(addPromises).then(values => {
										console.log("First Tardies Recorded");
									});
								});
							});
						});	
					}	
				});
			});
		}, {
			scheduled:true,
			timezone: "America/Chicago"
		});
		GetTardiesJob.start();
	},
	this.getStudentDataJob = function() {
		let runStudentData = cron.job('10 7 1 */1 *', function() {
			console.log("Student Data Extract Running");
			const path = '/ws/schema/query/com.company.product.school.students_newest';
			const payload = {
			};
			let tableString = "students";
			const functionError = "Student Newest ";		
			let truncateStudentDataPromise = DataExtractTruncateTables.truncateStudentDataTable();
			truncateStudentDataPromise.then(function() { //Extract and Insert the student Information
				console.log("Student Data Extract Occuring");
				let powerschoolPromise = PowerschoolSimple.getToken();
				powerschoolPromise.then(function(token) {
					let newestStudentPromise = PowerschoolSimple.getData(path, payload, tableString, functionError, token, null);
					let lastStudentInsertedPromise = DataExtractGetIDs.getLastInserted("student_powerschool_id","student_data");
					Promise.all([newestStudentPromise, lastStudentInsertedPromise]).then(function(values) {
						let newestStudentID = values[0][0].id;
						let lastStudentID = values[1];
						if(parseInt(lastStudentID) < parseInt(newestStudentID)) {
							DataFills.FillStudents(newestStudentID, lastStudentID, "getStudentInfo", token);
						}
					});
				});
			});
		}, {
			scheduled:true,
			timezone: "America/Chicago"
		});
		runStudentData.start();		
	},
	this.getStudentGradesJob = function() {
		let runStudentGrades = cron.job('10 7 1 */1 *', function() {
			console.log("Student Grade Extract Running");
			const path = '/ws/schema/query/com.company.product.school.student_grades_newest';
			const payload = {};
			let tableString = "storedgrades";
			const functionError = "Student Grades Newest ";
			let powerschoolPromise = PowerschoolSimple.getToken();
			powerschoolPromise.then(function(token) {
				let newestGradePromise = PowerschoolSimple.getData(path, payload, tableString, functionError, token, null);
				let lastGradeInsertedPromise = DataExtractGetIDs.getLastInserted("powerschool_id","student_grades");
				Promise.all([newestGradePromise, lastGradeInsertedPromise]).then(function(values) { //Extract and Insert the student Grades
					console.log("Grade Data Extract Occuring");
					let newestGradeID = values[0][0].dcid;
					let lastGradeID = values[1];
					if(parseInt(lastGradeID) < parseInt(newestGradeID)) {
						DataFills.FillGrades(newestGradeID, lastGradeID, "getStudentGrades", token);
					}
				});
			});
		}, {
			scheduled:true,
			timezone: "America/Chicago"
		});
		runStudentGrades.start();
	},
	this.getAssignmentsJob = function() {
		let runStudentAssignment = cron.job('30 02 * * 1-5', function() {
			let currentYearBegin = getCurrentSchoolYearBegins();
			let currentYearEnd = currentYearBegin+1;
			let begin_date = currentYearBegin + '-07-01';
			let end_date = currentYearBegin + '-06-30';
			console.log(begin_date);
			const path = '/ws/schema/query/com.company.product.school.assignment_newest';
			const payload = {
				"begin_date": begin_date,
				"end_date": end_date
			};
			let tableString = "assignmentscore";
			const functionError = "Assignment Newest ";
			console.log("Assignment Extract Running");
			let truncateAssignmentDataPromise = DataExtractTruncateTables.truncateStudentAssignmentsTable()
			truncateAssignmentDataPromise.then(function() { //Extract and Insert the student current Year Assignments
				console.log("Assignment Extract Occuring");
				let powerschoolPromise = PowerschoolSimple.getToken();
				powerschoolPromise.then(function(token) {
					let newestAssignmentPromise = PowerschoolSimple.getData(path, payload, tableString, functionError, token, null);
					let lastAssignmentInsertedPromise = DataExtractGetIDs.getLastInserted("assignment_score_id","student_assignment_grades");
					Promise.all([newestAssignmentPromise, lastAssignmentInsertedPromise]).then(function(values) {
						let newestAssignmentID = values[0][0].assignmentscoreid;
						let lastAssignmentID = values[1];
						if(parseInt(lastAssignmentID) < parseInt(newestAssignmentID)) {
							DataFills.FillAssignments(newestAssignmentID, lastAssignmentID, "addAllAssignmentsCurYear", token);
						}
					});
				});
			});
		}, {
			scheduled:true,
			timezone: "America/Chicago"
		});
		runStudentAssignment.start();
	},
	this.getAbsensesJob = function() {
		let runStudentAbsense = cron.job('00 04 * * 1-5', function() {
			const path = '/ws/schema/query/com.company.product.school.attendance_newest';
			const payload = {};
			let tableString = "attendance";
			const functionError = "Attendance Newest ";
			console.log("Absense Extract Running");
			let powerschoolPromise = PowerschoolSimple.getToken();
			powerschoolPromise.then(function(token) {
				let newestAbsensePromise = PowerschoolSimple.getData(path, payload, tableString, functionError, token, null);
				let lastAbsenseInsertedPromise = DataExtractGetIDs.getLastInserted("powerschool_attendance_id","student_attendance");
				Promise.all([newestAbsensePromise, lastAbsenseInsertedPromise]).then(function(values) {
					let newestAbsenseID = values[0][0].id;
					let lastAbsenseID = values[1];
					if(parseInt(lastAbsenseID) < parseInt(newestAbsenseID)) {
						DataFills.FillAbsenses(newestAbsenseID, lastAbsenseID, "getStudentAbsenses", token);
					}
				});
			});
		}, {
			scheduled:true,
			timezone: "America/Chicago"
		});
		runStudentAbsense.start();
	},
	this.getCoursesJob = function() {
		let runCourses = cron.job('00 01 * * 1-5', function() {
			let path = '/ws/schema/query/com.company.product.school.courses_newest';
			let payload = {};
			let tableString = "courses";
			const functionError = "Get Courses Newest";
			console.log("Courses Extract Running");
			let powerschoolPromise = PowerschoolSimple.getToken();
			powerschoolPromise.then(function(token) {
				let newestCoursePromise = PowerschoolSimple.getData(path, payload, tableString, functionError, token, null);
				let lastCourseInsertedPromise = DataExtractGetIDs.getLastInserted("ps_course_id","courses");
				Promise.all([newestCoursePromise, lastCourseInsertedPromise]).then(function(values) {
					let newestCourseID = values[0][0].id;
					let lastCourseID = values[1];
					if(parseInt(lastCourseID) < parseInt(newestCourseID)) {
						DataFills.FillCourses(newestCourseID, lastCourseID, "getCourses", token);
					}
				});
			});
		}, {
			scheduled:true,
			timezone: "America/Chicago"
		});
		runCourses.start();
	},
	this.getTeachersJob = function() {
		let runTeachers = cron.job('02 01 * * 1-5', function() {
			let path = '/ws/schema/query/com.company.product.school.teachers_newest';
			let payload = {};
			let tableString = "teachers";
			const functionError = "Get Teachers Newest ";
			console.log("Teacher Extract Running");
			let powerschoolPromise = PowerschoolSimple.getToken();
			powerschoolPromise.then(function(token) {
				let newestTeacherPromise = PowerschoolSimple.getData(path, payload, tableString, functionError, token, null);
				let lastTeacherInsertedPromise = DataExtractGetIDs.getLastInserted("teacher_number","teachers");
				Promise.all([newestTeacherPromise, lastTeacherInsertedPromise]).then(function(values) {
					let newestTeacherID = values[0][0].id;
					let lastTeacherID = values[1];
					if(parseInt(lastTeacherID) < parseInt(newestTeacherID)) {
						DataFills.FillTeachers(newestTeacherID, lastTeacherID, "getTeachers", token);
					}
				});
			});
		}, {
			scheduled:true,
			timezone: "America/Chicago"
		});
		runTeachers.start();
	},
	this.getSectionsJob = function() {
		let runSections = cron.job('07 01 * * 1-5', function() {
			console.log("Section Extract Running");
			let path = '/ws/schema/query/com.company.product.school.sections_newest';
			let payload = {
				"school_id": config.powerschool.school_id
			};
			let tableString = "sections";
			const functionError = "Get Sections Newest ";
			let powerschoolPromise = PowerschoolSimple.getToken();
			powerschoolPromise.then(function(token) {
				let newestSectionPromise = PowerschoolSimple.getData(path, payload, tableString, functionError, token, null);
				let lastSectionInsertedPromise = DataExtractGetIDs.getLastInserted("ps_section_id","sections");
				Promise.all([newestSectionPromise, lastSectionInsertedPromise]).then(function(values) {
					let newestSectionID = values[0][0].id;
					let lastSectionID = values[1];
					if(parseInt(lastSectionID) < parseInt(newestSectionID)) {
						DataFills.FillSections(newestSectionID, lastSectionID, "getSections", token);
					}
				});
			});
		}, {
			scheduled:true,
			timezone: "America/Chicago"
		});
		runSections.start();
	},
	this.getStandardizedTestsJob = function() {
		let runSections = cron.job('45 01 * * 1-5', function() {
			console.log("Standardized Test Extract Running");
			let path = '/ws/schema/query/com.company.product.school.standardized_test_newest';
			let payload = {
				"school_id": config.powerschool.school_id
			};
			let tableString = "studenttestscore";
			const functionError = "Get Standardized Test Newest ";
			let powerschoolPromise = PowerschoolSimple.getToken();
			powerschoolPromise.then(function(token) {
				let newestStandardizedPromise = PowerschoolSimple.getData(path, payload, tableString, functionError, token, null);
				let lastStandardizedInsertedPromise = DataExtractGetIDs.getLastInserted("ps_student_test_score_id","standardized_tests");
				Promise.all([newestStandardizedPromise, lastStandardizedInsertedPromise]).then(function(values) {
					let newestStandardizedID = values[0][0].id;
					let lastStandardizedID = values[1];
					if(parseInt(lastStandardizedID) < parseInt(newestStandardizedID)) {
						DataFills.FillStandardizedTestScores(newestStandardizedID, lastStandardizedID, "getStandardizedTests", token);
					}
				});
			});
		}, {
			scheduled:true,
			timezone: "America/Chicago"
		});
		runSections.start();
	},
	this.getReenrollmentJob = function() {
		let runSections = cron.job('37 23 * * *', function() {
			console.log("ReEnrollment Extract Running");
			let path = '/ws/schema/query/com.company.product.school.reenrollments_newest';
			let payload = {"school_id": config.powerschool.school_id};
			let tableString = "reenrollments";
			const functionError = "Get ReEnrollment Newest ";
			let powerschoolPromise = PowerschoolSimple.getToken();
			powerschoolPromise.then(function(token) {
				let newestReenrollmentPromise = PowerschoolSimple.getData(path, payload, tableString, functionError, token, null);
				let lastReenrollmentInsertedPromise = DataExtractGetIDs.getLastInserted("ps_reenrollment_id","reenrollment");
				Promise.all([newestReenrollmentPromise, lastReenrollmentInsertedPromise]).then(function(values) {
					let newestReenrollmentID = values[0][0].dcid;
					let lastReenrollmentID = values[1];
					if(parseInt(lastReenrollmentID) < parseInt(newestReenrollmentID)) {
						DataFills.FillData(newestReenrollmentID, lastReenrollmentID, "getEnrollment",token);
					}
				});
			});
		}, {
			scheduled:true,
			timezone: "America/Chicago"
		});
		runSections.start();
	},
	this.getMiddleSchoolsJob = function() {
		let runMiddleSchools = cron.job('45 01 * * 1-5', function() {
			console.log("Middle Extract Running");
			let path = '/ws/schema/query/com.company.product.school.student_middle_school';
			let payload = {};
			let tableString = "u_def_ext_students";
			const functionError = "Get Middle School Newest ";
			let powerschoolPromise = PowerschoolSimple.getToken();
			powerschoolPromise.then(function(token) {
				let newestMSPromise = PowerschoolSimple.getData(path, payload, tableString, functionError, token, null);
				let lastMSPromise = DataExtractGetIDs.getLastInserted("student_id","student_middle_school");
				Promise.all([newestMSPromise, lastMSPromise]).then(function(values) {
					let newestMSID = values[0][0].id;
					let lastMSID = values[1];
					if(parseInt(lastMSID) < parseInt(newestMSID)) {
						DataFills.FillData(newestMSID, lastMSID, "getMiddleSchool", token);
					}
				});
			});
		}, {
			scheduled:true,
			timezone: "America/Chicago"
		});
		runMiddleSchools.start();		
	}	
}
function getCurrentSchoolYearBegins() {
	var SchoolBegin,
	    CurrentDate,
	    CurrentMonth;
	CurrentDate = new Date();
	CurrentMonth = CurrentDate.getMonth();
	if(CurrentMonth >=7 && CurrentMonth<=12) {
		SchoolBegin = CurrentDate.getFullYear();
	} else {
		SchoolBegin = CurrentDate.getFullYear() - 1;
	}
	return SchoolBegin;
}
module.exports = new CronJobs();