// load up the user model
var bcrypt = require('bcrypt-nodejs');
var dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
var moment = require('moment');
var googleAuthorization = require('../config/calendarConfig.js').jwtClient;
const {google} = require('googleapis');
const calendar = google.calendar('v3');
var Promise = require('promise');
var Substitutes = require('../models/substitutes');
let config = require("../config/configFile.js").get(process.env.NODE_ENV.trim());
// expose this function to our app using module.exports


//
function Calendar() {
	this.sendCalendarInvite = function(req,res) {
		return new Promise(function(fulfill, reject) {
			var startTime = moment.utc(req.body.start_time*1000).format('HH:mm:ss');
			var endTime = moment.utc(req.body.end_time*1000).format('HH:mm:ss');
			var date = moment(req.body.date).format("YYYY-MM-DD");
			startTime = date+'T'+startTime
			endTime = date+'T'+endTime
			let period_number = req.body.period_number;
			var course_name = req.body.course_name;
			var room = req.body.room;
			var email = req.body.email;
			let teacher = req.body.teacher;
			var user = email.slice(0,-20);
			var lastName = user.slice(1);
			let firstLetter = lastName.charAt(0).toUpperCase();
			lastName = firstLetter+ lastName.slice(1);
			console.log(teacher + " " + course_name);
			googleAuthorization.authorize(function(err, token) {
				if(err) {
					reject(console.log({status: 1, message: 'Google Authorization Failed: ' + err}));
				} else {
					var event = {
					  'summary': lastName +' - Substitute Assignment',
					  'location': 'Room: '+ room,
					  'description': 'You have been assigned to substitute for '+ teacher + "'s class "+ course_name + " in room " + room + ". If for some reason you cannot attend this class Substitution please email mchew@cristoreyjesuit.org.",
					  'start': {
					    'dateTime': startTime,
					    'timeZone': config.timeZone,
					  },
					  'end': {
					    'dateTime': endTime,
					    'timeZone': config.timeZone,
					  },
					  'attendees': [
					    {'email': config.gmail.calendarInviter},
					    {'email' : email},
					    //{'responseStatus':'needsAction'},
					  ],
					  'reminders': {
					    'useDefault': false,
					    'overrides': [
					      {'method': 'popup', 'minutes': 10},
					    ],
					   },
					};
					var optionalParams = {
						'sendNotifications' : true
					}
					calendar.events.insert({
					  auth: googleAuthorization,
					  calendarId: 'primary',
					  resource: event,
					  sendNotifications: true
					}, function(err, event) {
					 	if (err) {
					    	console.log('There was an error contacting the Calendar service: ' + err);
					    	return;
						}
						let promiseStack = [];
						promiseStack.push(Substitutes.markSubAssigned(req.body.pto_class_id));
						promiseStack.push(Substitutes.logSubClassAssigned(course_name,user,date,req.body.pto_class_id, period_number));
						promiseStack.push(Substitutes.addCalendarID(event.data.id,req.body.pto_class_id));
						Promise.all(promiseStack).then(function(values) {
							res.send(values[1]);
						});
					});
				}
			});
		});
	},
	this.ptoCalendarInvite = function(manager,user,date) {
		return new Promise(function(fulfill, reject) {
			date = new Date(date);
			date = moment(date).format('YYYY-MM-DD');
			googleAuthorization.authorize(function(err, token) {
				if(err) {
					reject(console.log({status: 1, message: 'Google Authorization Failed: ' + err}));
				} else {
					var event = {
					  'summary': user +' - PTO',
					  'start': {
					    'date': date,
					  },
					  'end': {
					    'date': date,
					  },
					  'attendees': [
					    {'email': manager+config.emailDomain},
					    {'email' : user+config.emailDomain},
					    //{'responseStatus':'needsAction'},
					  ],
					};
					var optionalParams = {
						'sendNotifications' : true
					}
					calendar.events.insert({
					  auth: googleAuthorization,
					  calendarId: 'primary',
					  resource: event,
					  sendNotifications: true
					}, function(err, event) {
					 	if (err) {
					    	console.log('There was an error contacting the Calendar service: ' + err);
					    	return;
						}
					});
				}
			});
		});
	},
	this.deleteCalendarEvent = function(event_id) {
		return new Promise(function(fulfill, reject) {
			googleAuthorization.authorize(function(err, token) {
				if(err) {
					reject(console.log({status: 1, message: 'Google Authorization Failed: ' + err}));
				} else {
					calendar.events.delete({
						auth: googleAuthorization,
						calendarId: 'pto@cristoreyjesuit.org',
						eventId: event_id[0].event_id
					}, function(err, event) {
						if(err) {
							reject(console.log({"There was an error deleting: ": err}));
							return
						}
						fulfill(true);
					});
				}
			});
		});
	}
}
module.exports = new Calendar();