let bcrypt = require('bcrypt-nodejs');
let dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
let moment = require('moment');
let ad = require('../config/ad.js').ad;
let Promise = require('promise');
let Permissions = require('../models/permissions');
let stream = require('stream');
const fs = require('fs');
const Pagination = require('../models/pagination');

function CollegeFinancials() {
	this.getAllStudents = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let getQuery = "SELECT lastfirst, student_number FROM student_data order by lastfirst";
			var allUsers = {};
			dbconfig.getConnection(function(err, connection) {
				connection.query(getQuery,function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Add Failed - Get Students: ' + err}));
					} else {
						fulfill(result);	
					}
				});
			});
		});
	}
	this.addCollege = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let insertQuery = "INSERT INTO colleges (college_name, tuition_cost, room_board_cost, other_cost) VALUES (?,?,?,?)";
			let tuition_cost = req.body.txtTuitionCost;
			let room_board_cost = req.body.txtRoomBoardCost;
			let other_cost = req.body.txtOtherCost;
			if(tuition_cost == "") {tuition_cost = 0}
			if(room_board_cost == "") {room_board_cost = 0}
			if(other_cost == "") {other_cost = 0}
			dbconfig.getConnection(function(err, connection) {
				connection.query(insertQuery,[req.body.txtCollegeName, tuition_cost, room_board_cost, other_cost],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Add Failed - Insert College: ' + err});
					} else {
						res.redirect('/colleges');	
					}
				});
			});
		});
	},
	this.updateCollege = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let tuition_cost = req.body.tuition_cost;
			let room_board_cost = req.body.room_board_cost;
			let other_cost = req.body.other_cost;
			if(tuition_cost == "") {tuition_cost = 0}
			if(room_board_cost == "") {room_board_cost = 0}
			if(other_cost == "") {other_cost = 0}			
			let updateQuery = "UPDATE colleges set college_name = ?, tuition_cost = ?, room_board_cost = ?, other_cost = ? WHERE college_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(updateQuery,[req.body.college_name, tuition_cost, room_board_cost, other_cost, req.body.college_id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Update Failed - Update College Name: ' + err});
					} else {
						res.redirect('/colleges');
					}
				})
			});
		});
	},
	this.deleteCollege = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let deleteQuery = "DELETE FROM colleges WHERE college_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(deleteQuery,[req.body.college_id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Delete Failed - Delete College: ' + err});
					} else {
						res.redirect('/colleges');
					}
				})
			});
		});
	},
	this.getColleges = function() {
		return new Promise(function(fulfill, reject) {
			let selectQuery = "Select * FROM colleges ORDER BY college_name ASC";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,function(err, result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Select Failed - Get Colleges: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},
	this.renderCollegesView = function(req,res) {
		let collegesPromise = this.getColleges();
		collegesPromise.then(function(colleges) {
			res.render('collegeFinancials/colleges.ejs', {
				login : true,
				colleges : colleges,
				moment: moment
			});
		});
	},
	this.addGrant = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let insertQuery = "INSERT INTO grants (grant_name, funding_type_id) VALUES (?,?)";
			dbconfig.getConnection(function(err, connection) {
				connection.query(insertQuery,[req.body.txtGrantName, req.body.ddlfunding_types],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Add Failed - Insert Grant: ' + err});
					} else {
						res.redirect('/grants');	
					}
				});
			});
		});
	},
	this.updateGrant = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let updateQuery = "UPDATE grants set grant_name = ?, funding_type_id = ? WHERE grant_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(updateQuery,[req.body.grant_name, req.body.funding_type_id, req.body.grant_id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Update Failed - Update Grant: ' + err});
					} else {
						res.redirect('/grants');
					}
				})
			});
		});
	},
	this.deleteGrant = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let deleteQuery = "DELETE FROM grants WHERE grant_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(deleteQuery,[req.body.grant_id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Delete Failed - Delete Grant: ' + err});
					} else {
						res.redirect('/grants');
					}
				})
			});
		});
	},
	this.getGrants = function() {
		return new Promise(function(fulfill, reject) {
			let selectQuery = "Select * FROM grants "
			selectQuery += "INNER JOIN funding_types ON grants.funding_type_id = funding_types.funding_type_id ";
			selectQuery += "ORDER BY grant_name ASC";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,function(err, result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Select Failed - Get Grants: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},
	this.renderGrantsView = function(req,res) {
		let grantsPromise = this.getGrants();
		let fundingTypesPromise = this.getFundingTypes();
		Promise.all([grantsPromise, fundingTypesPromise]).then(function(values) {
			res.render('collegeFinancials/grants.ejs', {
				login : true,
				grants : values[0],
				funding_types : values[1],
				moment: moment
			});
		});
	},	
	this.addFundingType = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let insertQuery = "INSERT INTO funding_types (funding_type_name) VALUES (?)";
			dbconfig.getConnection(function(err, connection) {
				connection.query(insertQuery,[req.body.txtFundingTypeName],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Add Failed - Insert Funding Type: ' + err});
					} else {
						res.redirect('/funding_types');	
					}
				});
			});
		});
	},
	this.updateFundingType = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let updateQuery = "UPDATE funding_types set funding_type_name = ? WHERE funding_type_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(updateQuery,[req.body.funding_type_name, req.body.funding_type_id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Update Failed - Update Funding Type: ' + err});
					} else {
						res.redirect('/funding_types');
					}
				})
			});
		});
	},
	this.deleteFundingType = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let deleteQuery = "DELETE FROM funding_types WHERE funding_type_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(deleteQuery,[req.body.funding_type_id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Delete Failed - Delete Funding Type: ' + err});
					} else {
						res.redirect('/funding_types');
					}
				})
			});
		});
	},
	this.getFundingTypes = function() {
		return new Promise(function(fulfill, reject) {
			let selectQuery = "Select * FROM funding_types ORDER BY funding_type_name ASC";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,function(err, result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Select Failed - Get Funding Types: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},
	//https://stackoverflow.com/questions/40967927/multiple-get-parameter-with-express?noredirect=1&lq=1
	this.addNote = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let currentDate = moment(new Date()).format("YYYY-MM-DD");
			let insertQuery = "INSERT INTO student_notes (student_number, note, date) VALUES (?,?,?)";
			dbconfig.getConnection(function(err, connection) {
				connection.query(insertQuery,[req.body.hidStuNumNote, req.body.txtNote,currentDate],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Add Failed - Insert Note: ' + err});
					} else {
						res.redirect('/student_profile/'+req.body.hidStuNumNote);	
					}
				});
			});
		});
	},
	this.updateNote = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let updateQuery = "UPDATE student_notes set note = ? WHERE student_note_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(updateQuery,[req.body.student_note, req.body.student_note_id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Update Failed - Update Note: ' + err});
					} else {
						res.redirect('/student_profile/'+req.body.student_number);
					}
				})
			});
		});
	},
	this.deleteNote = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let deleteQuery = "DELETE FROM student_notes WHERE student_note_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(deleteQuery,[req.body.student_note_id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Delete Failed - Delete Student Note: ' + err})
						reject(false);
					} else {
						fulfill(true);
					}
				})
			});
		});
	},
	this.getNotes = function(student_number, perPage, start) {
		return new Promise(function(fulfill, reject) {
			let selectQuery = "Select * FROM student_notes where student_number = ?  ORDER BY date DESC ";
			selectQuery += "LIMIT "+perPage+" OFFSET "+start;
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,[student_number],function(err, result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Select Failed - Get Notes: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},
	this.getNumberOfNotes = function(student_number) {
		return new Promise(function(fulfill, reject) {
			let selectQuery = "Select COUNT(student_note_id) totalCount FROM student_notes where student_number = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,[student_number],function(err, result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Select Failed - Get Notes: ' + err}));
					} else {
						fulfill(result[0].totalCount);
					}
				});
			});
		});	
	},
	this.renderFundingTypeView = function(req,res) {
		let fundingTypesPromise = this.getFundingTypes();
		fundingTypesPromise.then(function(funding_types) {
			res.render('collegeFinancials/funding_types.ejs', {
				login : true,
				funding_types : funding_types,
				moment: moment
			});
		});
	},
	this.getStudentEFC = function(student_number) {
		return new Promise(function(fulfill, reject) {
			let selectQuery = "SELECT * FROM student_efc WHERE student_number = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,[student_number],function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Select Failed - Get Student EFC Types: ' + err}));
					} else {
						if(result.length > 0) {
							fulfill(result[0].efc_amount);
						} else {
							fulfill(false);
						}		
					}
				})
			});
		});
	}
	this.updateInsertStudentEFC = function(req, res) {
		return new Promise(function(fulfill, reject) {
			let Query;
			let studentEFCPromise = this.getStudentEFC(req.body.student_numberEFC);
			studentEFCPromise.then(function(efc) {
				if(!efc) {
					Query = "UPDATE student_efc SET efc_amount = ? WHERE student_number = ? ";
				} else {
					Query = "INSERT INTO student_efc(efc_amount, student_number) VALUES(?,?)";
				}
				dbconfig.getConnection(function(err, connection) {
					connection.query(Query,[req.body.txtEFC, req.body.student_numberEFC],function(err,result) {
						connection.release();
						if(err) {
							console.log({status: 1, message: 'Update Failed - Update EFC: ' + err});
						} else {
							res.redirect('/student_profile/'+req.body.student_numberEFC);
						}
					});
				});
			});
		});
	}
	this.getStudentName = function(student_number) {
		return new Promise(function(fulfill, reject) {
			let getQuery = "Select * from student_data WHERE student_number = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(getQuery,[student_number],function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Get Failed - Get Student Info: ' + err}));
					} else {
						fulfill(result);	
					}
				});
			});
		});
	},
	this.getStudentColleges = function(student_number) {
		return new Promise(function(fulfill, reject) {
			let getQuery = "Select * from student_colleges ";
			getQuery += "INNER JOIN colleges on student_colleges.college_id = colleges.college_id ";
			getQuery += "WHERE student_number = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(getQuery,[student_number],function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Get Failed - Get Student Colleges: ' + err}));
					} else {
						fulfill(result);	
					}
				});
			});
		});
	},
	this.addCollegeToStudent = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let insertQuery = "INSERT INTO student_colleges (student_number, college_id, accept_apply_attend) VALUES (?,?, 3)"; //Defaults to "3" or applied when initially adding a student.
			dbconfig.getConnection(function(err, connection) {
				connection.query(insertQuery,[req.body.student_number, req.body.college_id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Add Failed - Insert College To Student: ' + err});
					} else {
						res.redirect('/student_profile/'+req.body.student_number);	
					}
				});
			});
		});
	},
	this.updateStudentCollege = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let updateQuery = "UPDATE student_colleges set accept_apply_attend = ? WHERE student_college_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(updateQuery,[req.body.accept_apply_attend, req.body.student_college_id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Add Failed - Update Student College: ' + err});
					} else {
						res.redirect('/student_profile/'+req.body.student_number);	
					}
				});
			});
		});
	},	
	this.deleteCollegeFromStudent = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let deleteQuery = "DELETE FROM student_colleges WHERE student_college_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(deleteQuery,[req.body.student_college_id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Delete Failed - Delete College From Student: ' + err})
						reject(false);
					} else {
						fulfill(true);
					}
				})
			});
		});
	},
	this.getStudentGrants = function(student_number) {
		return new Promise(function(fulfill, reject) {
			let getQuery = "Select sf.student_college_fund_id, sf.student_college_id, sf.grant_amount, sf.grant_years, g.grant_name, ft.funding_type_name, sfd.file_name from student_funds sf ";
				getQuery += "INNER JOIN grants g on sf.grant_id = g.grant_id ";
				getQuery += "INNER JOIN funding_types ft on ft.funding_type_id = g.funding_type_id ";
				getQuery += "LEFT JOIN student_fund_documents sfd on sfd.student_college_fund_id = sf.student_college_fund_id ";
				getQuery += "WHERE student_number = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(getQuery,[student_number],function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Get Failed - Get Student Grants: ' + err}));
					} else {
						fulfill(result);	
					}
				});
			});
		});		
	},
	this.updateStudentCollegeFund = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let updateQuery = "UPDATE student_funds set grant_amount = ?, grant_years = ? WHERE student_college_fund_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(updateQuery,[req.body.grant_amount, req.body.grant_years, req.body.student_college_fund_id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Add Failed - Update Student College Fund: ' + err});
					} else {
						res.redirect('/student_profile/'+req.body.student_number);	
					}
				});
			});
		});
	},		
	this.getStudentGeneralFunds = function(student_number) {
		return new Promise(function(fulfill, reject) {
			let getQuery = "Select sgf.student_general_fund_id, sgf.grant_amount, sgf.grant_years, g.grant_name, ft.funding_type_name, sfd.file_name from student_general_fund sgf ";
				getQuery += "INNER JOIN grants g on sgf.grant_id = g.grant_id ";
				getQuery += "INNER JOIN funding_types ft on ft.funding_type_id = g.funding_type_id ";
				getQuery += "LEFT JOIN student_general_fund_documents sfd on sfd.student_general_fund_id = sgf.student_general_fund_id ";
				getQuery += "WHERE student_number = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(getQuery,[student_number],function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Get Failed - Get Student Grants: ' + err}));
					} else {
						fulfill(result);	
					}
				});
			});
		});		
	},
	this.addGeneralGrantToStudent = function(student_number, grant_id, grant_amount, grant_years, file_name, file_type, file, req, res) {
		colFin = this;
		return new Promise(function(fulfill, reject) {
			let insertQuery = "INSERT INTO student_general_fund (student_number, grant_id, grant_amount, grant_years) VALUES (?,?,?,?)";
			dbconfig.getConnection(function(err, connection) {
				connection.query(insertQuery,[student_number, grant_id, grant_amount, grant_years],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Add Failed - Insert Student General Fund: ' + err});
					} else {
						if(file_name != "") {
							colFin.addDocumentToStudentGeneralFund(result.insertId, file_name, file_type, file);
						}
						res.redirect('/student_profile/'+student_number);	
					}
				});
			});
		});
	},
	this.updateStudentGeneralFund = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let updateQuery = "UPDATE student_general_fund set grant_amount = ?, grant_years = ? WHERE student_general_fund_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(updateQuery,[req.body.grant_amount, req.body.grant_years, req.body.student_general_fund_id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Add Failed - Update Student General Fund: ' + err});
					} else {
						res.redirect('/student_profile/'+req.body.student_number);	
					}
				});
			});
		});
	},	
	this.addGrantToStudentCollege = function(student_number, student_college_id, grant_id, grant_amount, grant_years, file_name, file_type, file, req, res) {
		colFin = this;
		return new Promise(function(fulfill, reject) {
			let insertQuery = "INSERT INTO student_funds (student_number, student_college_id, grant_id, grant_amount, grant_years) VALUES (?,?,?,?,?)";
			dbconfig.getConnection(function(err, connection) {
				connection.query(insertQuery,[student_number, student_college_id, grant_id, grant_amount, grant_years],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Add Failed - Insert College Student Fund: ' + err});
					} else {
						if(file_name != "") {
							colFin.addDocumentToStudentFund(result.insertId, file_name, file_type, file);
						}
						res.redirect('/student_profile/'+student_number);	
					}
				});
			});
		});
	},
	this.addDocumentToStudentFund = function(student_college_fund_id, file_name, file_type, file) {
		let insertQuery = "INSERT INTO student_fund_documents (student_college_fund_id, file_name, file_type, file) VALUES (?,?,?,?)";
		dbconfig.getConnection(function(err, connection) {
			connection.query(insertQuery,[student_college_fund_id, file_name, file_type, file],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Add Failed - Insert College Student Fund Document: ' + err});
				}
			});
		});
	},
	this.addDocumentToStudentGeneralFund = function(student_general_fund_id, file_name, file_type, file) {
		let insertQuery = "INSERT INTO student_general_fund_documents (student_general_fund_id, file_name, file_type, file) VALUES (?,?,?,?)";
		dbconfig.getConnection(function(err, connection) {
			connection.query(insertQuery,[student_general_fund_id, file_name, file_type, file],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Add Failed - Insert College Student Fund Document: ' + err});
				}
			});
		});
	},	
	this.getDocument = function(student_college_fund_id, req, res) {
		let getQuery = "SELECT * FROM student_fund_documents WHERE student_college_fund_id = ?";
		dbconfig.getConnection(function(err, connection) {
			connection.query(getQuery,[student_college_fund_id],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Add Failed - Insert College Student Fund Document: ' + err});
				} else {
					let fileContents = Buffer.from(result[0].file, "base64");
					res.setHeader('Content-disposition', 'attachment; filename=' + result[0].file_name);
					res.setHeader('Content-Type', result[0].file_type);
					return res.status(200).send(fileContents);

				}
			});
		});
	},
	this.getDocumentGeneralFund = function(student_general_fund_id, req, res) {
		let getQuery = "SELECT * FROM student_general_fund_documents WHERE student_general_fund_id = ?";
		dbconfig.getConnection(function(err, connection) {
			connection.query(getQuery,[student_general_fund_id],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Add Failed - Insert General Student Fund Document: ' + err});
				} else {
					let fileContents = Buffer.from(result[0].file, "base64");
					res.setHeader('Content-disposition', 'attachment; filename=' + result[0].file_name);
					res.setHeader('Content-Type', result[0].file_type);
					return res.status(200).send(fileContents);

				}
			});
		});
	},	
	this.deleteGrantFromStudent = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let deleteQuery = "DELETE FROM student_funds WHERE student_college_fund_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(deleteQuery,[req.body.student_college_fund_id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Delete Failed - Delete College Student Fund: ' + err})
						reject(false);
					} else {
						fulfill(true);
					}
				})
			});
		});
	},
	this.deleteGeneralGrantFromStudent = function(req,res) {
		return new Promise(function(fulfill, reject) {
			let deleteQuery = "DELETE FROM student_general_fund WHERE student_general_fund_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(deleteQuery,[req.body.student_general_fund_id],function(err,result) {
					connection.release();
					if(err) {
						console.log({status: 1, message: 'Delete Failed - Delete General Student Fund: ' + err})
						reject(false);
					} else {
						fulfill(true);
					}
				})
			});
		});
	},	
	this.renderStudentProfiles = function(student_number, page, req, res) {
		const page_id = page;
		const currentPage = page_id > 0 ? page_id : currentPage;
		const pageUri = '/student_profile/'+student_number+"/";
		const perPage = 10;
		const ColFin = this;
		let studentNamePromise = this.getStudentName(student_number);
		let studentCollegePromise = this.getStudentColleges(student_number);
		let studentGrantPromise = this.getStudentGrants(student_number);
		let studentEFCPromise = this.getStudentEFC(student_number);
		let studentGeneralGrantPromise = this.getStudentGeneralFunds(student_number);
		let studentNotesNumberPromise = this.getNumberOfNotes(student_number);
		Promise.all([studentNamePromise, studentCollegePromise, studentGrantPromise, studentEFCPromise, studentGeneralGrantPromise, studentNotesNumberPromise]).then(function(values) {
			let studentEfc;
			if(!values[3] === false) { studentEfc = values[3] } else { studentEfc = 0}
			const Paginate = new Pagination(values[5],currentPage,pageUri,perPage);
			let studentNotesPromise = ColFin.getNotes(student_number, Paginate.perPage, Paginate.offset);
			studentNotesPromise.then(function(notes) {
				res.render('collegeFinancials/student_profile.ejs', {
					login : true,
					student : values[0],
					student_colleges : values[1],
					student_grants : values[2],
					student_efc : studentEfc,
					student_general_funds: values[4],
					student_notes: notes,
					pages : Paginate.links(),
					moment: moment
				});
			});

		});
	},
	this.insertPellGrantZeroEFC = function(grant_amount) {
		FiscalBegin = getCurrentFiscalYear();
		GradYear = FiscalBegin + 1
		let insertQuery = "INSERT INTO student_general_fund (student_number, grant_id, grant_amount, grant_years) ";
			insertQuery += "Select DISTINCT(student_data.student_number), 18, ?, 1 ";
        	insertQuery += "from student_data ";
        	insertQuery += "LEFT JOIN student_general_fund on student_data.student_number = student_general_fund.student_number ";
        	insertQuery += "WHERE student_data.student_number NOT IN (Select student_number from student_efc) ";
			insertQuery += "AND LEFT(student_data.student_number,4) = ? ";
			insertQuery += "AND enroll_status = 0 ";
        	insertQuery += "AND student_data.student_number NOT IN (Select student_number FROM student_general_fund WHERE grant_id = 18);";
		dbconfig.getConnection(function(err, connection) {
			connection.query(insertQuery,[grant_amount, GradYear],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Add Failed - Insert Pell Grants Zero EFC: ' + err});
				}
			});
		});
	},
	this.insertFederalUnsubsidized = function(grant_amount) {
		FiscalBegin = getCurrentFiscalYear();
		GradYear = FiscalBegin + 1
		let insertQuery = "INSERT INTO student_general_fund (student_number, grant_id, grant_amount, grant_years) ";
			insertQuery += "Select DISTINCT(student_data.student_number), 25, ?, 1 ";
        	insertQuery += "from student_data ";
        	insertQuery += "LEFT JOIN student_general_fund on student_data.student_number = student_general_fund.student_number ";
        	insertQuery += "WHERE student_data.student_number NOT IN (Select student_number from student_efc) ";
			insertQuery += "AND LEFT(student_data.student_number,4) = ? ";
			insertQuery += "AND enroll_status = 0 ";
        	insertQuery += "AND student_data.student_number NOT IN (Select student_number FROM student_general_fund WHERE grant_id = 25);";
		dbconfig.getConnection(function(err, connection) {
			connection.query(insertQuery,[grant_amount, GradYear],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Add Failed - Insert Federal Unsubsidized Zero EFC: ' + err});
				}
			});
		});
	},
	this.insertFederalSubsidized = function(grant_amount) {
		FiscalBegin = getCurrentFiscalYear();
		GradYear = FiscalBegin + 1
		let insertQuery = "INSERT INTO student_general_fund (student_number, grant_id, grant_amount, grant_years) ";
			insertQuery += "Select DISTINCT(student_data.student_number), 24, ?, 1 ";
        	insertQuery += "from student_data ";
        	insertQuery += "LEFT JOIN student_general_fund on student_data.student_number = student_general_fund.student_number ";
        	insertQuery += "WHERE student_data.student_number NOT IN (Select student_number from student_efc) ";
			insertQuery += "AND LEFT(student_data.student_number,4) = ? ";
			insertQuery += "AND enroll_status = 0 ";
        	insertQuery += "AND student_data.student_number NOT IN (Select student_number FROM student_general_fund WHERE grant_id = 24);";
		dbconfig.getConnection(function(err, connection) {
			connection.query(insertQuery,[grant_amount, GradYear],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Add Failed - Insert Federal Subsidized Zero EFC: ' + err});
				}
			});
		});
	}
}
function getCurrentFiscalYear() {
	var FiscalBegin,
	    CurrentDate,
	    CurrentMonth;
	CurrentDate = new Date();
	CurrentMonth = CurrentDate.getMonth();
	if(CurrentMonth >=6 && CurrentMonth<=12) {
		FiscalBegin = CurrentDate.getFullYear();
	} else {
		FiscalBegin = CurrentDate.getFullYear() - 1;
	}
	return FiscalBegin;
}
module.exports = new CollegeFinancials();