let dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
const Promise = require('promise');

function DataExtractGetIDs() {
	this.getLastInserted = function(column, table) {
		return new Promise(function(fullfill, reject) {
			let getQuery = 'Select ' + column + ' from ' + table + ' ORDER BY '+ column + ' desc LIMIT 1';
			dbconfig.getConnection(function(err, connection) {
				connection.query(getQuery, function(err, rows) {
					connection.release();
					if(err){
						reject(console.log("Err Get Last ID Data Extract: " + err));
					} else {
						if(rows.length > 0) {
							fulfill(rows[0].column);
						} else {
							fulfill(0);
						}
					}
				});
			});
		});
	},
	this.getLastStudentInserted = function() {
		return new Promise(function(fulfill, reject) {
			let getQuery = 'SELECT student_powerschool_id from student_data ORDER BY student_powerschool_id desc LIMIT 1'; 
			dbconfig.getConnection(function(err, connection){
				connection.query(getQuery,function(err, rows) {
					connection.release();
					if(err){
						reject(console.log("Err Get Last ID Data Extract: " + err));
					} else {
						if(rows.length > 0) {
							fulfill(rows[0].student_powerschool_id);
						} else {
							fulfill(0);
						}			
					}
				});
			});
		});
	},
	this.getLastGradeInserted = function() {
		return new Promise(function(fulfill, reject) {
			let getQuery = 'SELECT powerschool_id from student_grades ORDER BY powerschool_id desc LIMIT 1'; 
			dbconfig.getConnection(function(err, connection){
				connection.query(getQuery,function(err, rows) {
					connection.release();
					if(err){
						reject(console.log("Err Get Last ID Data Extract: " + err));
					} else {
						if(rows.length > 0) {
							fulfill(rows[0].powerschool_id);
						} else {
							fulfill(0);
						}
					}
				});
			});
		});
	},
	this.getLastAssignmentInserted = function() {
		return new Promise(function(fulfill, reject) {
			let getQuery = 'SELECT assignment_score_id from student_assignment_grades ORDER BY assignment_score_id desc LIMIT 1'; 
			dbconfig.getConnection(function(err, connection){
				connection.query(getQuery,function(err, rows) {
					connection.release();
					if(err){
						reject(console.log("Err Get Last ID Data Extract: " + err));
					} else {
						if(rows.length > 0) {
							fulfill(rows[0].assignment_score_id);
						} else {
							fulfill(0);
						}
					}
				});
			});
		});
	},
	this.getLastAbsenseInserted = function() {
		return new Promise(function(fulfill, reject) {
			let getQuery = 'SELECT powerschool_attendance_id from student_attendance ORDER BY powerschool_attendance_id desc LIMIT 1'; 
			dbconfig.getConnection(function(err, connection){
				connection.query(getQuery,function(err, rows) {
					connection.release();
					if(err){
						reject(console.log("Err Get Last ID Absense Data Extract: " + err));
					} else {
						if(rows.length > 0) {
							fulfill(rows[0].powerschool_attendance_id);
						} else {
							fulfill(0);
						}
					}
				});
			});
		});
	},
	this.getLastCourseInserted = function() {
		return new Promise(function(fulfill, reject) {
			let getQuery = 'SELECT ps_course_id from courses ORDER BY ps_course_id desc LIMIT 1'; 
			dbconfig.getConnection(function(err, connection){
				connection.query(getQuery,function(err, rows) {
					connection.release();
					if(err){
						reject(console.log("Err Get Last ID Courses Data Extract: " + err));
					} else {
						if(rows.length > 0) {
							fulfill(rows[0].ps_course_id);
						} else {
							fulfill(0);
						}
					}
				});
			});
		});
	},
	this.getLastTeacherInserted = function() {
		return new Promise(function(fulfill, reject) {
			let getQuery = 'SELECT teacher_number from teachers ORDER BY teacher_number desc LIMIT 1'; 
			dbconfig.getConnection(function(err, connection){
				connection.query(getQuery,function(err, rows) {
					connection.release();
					if(err){
						reject(console.log("Err Get Last ID Teachers Data Extract: " + err));
					} else {
						if(rows.length > 0) {
							fulfill(rows[0].ps_course_id);
						} else {
							fulfill(0);
						}
					}
				});
			});
		});
	},
	this.getLastSectionInserted = function() {
		return new Promise(function(fulfill, reject) {
			let getQuery = 'SELECT ps_section_id from sections ORDER BY ps_section_id desc LIMIT 1'; 
			dbconfig.getConnection(function(err, connection){
				connection.query(getQuery,function(err, rows) {
					connection.release();
					if(err){
						reject(console.log("Err Get Last ID Section Data Extract: " + err));
					} else {
						if(rows.length > 0) {
							fulfill(rows[0].ps_section_id);
						} else {
							fulfill(0);
						}
					}
				});
			});
		});
	},
	this.getLastStandardizedTestInserted = function() {
		return new Promise(function(fulfill, reject) {
			let getQuery = 'SELECT ps_student_test_score_id from standardized_tests ORDER BY ps_student_test_score_id desc LIMIT 1'; 
			dbconfig.getConnection(function(err, connection){
				connection.query(getQuery,function(err, rows) {
					connection.release();
					if(err){
						reject(console.log("Err Get Last ID Student Test Scores Data Extract: " + err));
					} else {
						if(rows.length > 0) {
							fulfill(rows[0].ps_student_test_score_id);
						} else {
							fulfill(0);
						}
					}
				});
			});
		});
	},
	this.getLastMiddleSchoolInserted = function() {
		return new Promise(function(fulfill, reject) {
			let getQuery = 'SELECT student_id from student_middle_schools ORDER BY student_id desc LIMIT 1'; 
			dbconfig.getConnection(function(err, connection){
				connection.query(getQuery,function(err, rows) {
					connection.release();
					if(err){
						reject(console.log("Err Get Last ID Student Middle School Data Extract: " + err));
					} else {
						if(rows.length > 0) {
							fulfill(rows[0].student_id);
						} else {
							fulfill(0);
						}
					}
				});
			});
		});
	}
}
module.exports = new DataExtractGetIDs();