var dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
let Assets = require('../models/Assets');
let Permissions = require('../models/permissions');
let moment = require('moment');
function Checkouts() {
	this.addCheckOut = function(req,res){
		let Username = req.body.username;
		let Asset_ID = req.body.ddlAsset;
		let Due_Date = req.body.due_date;
		let note = req.body.notes;
		let Date_Checked_Out = new Date();
		let updateQuery = "UPDATE Assets SET Status_ID = 5 WHERE Asset_ID = ?";
		dbconfig.getConnection(function(err,connection) {
			let insertQuery="insert into asset_checkouts (Username,Asset_ID,Due_Date,Date_Checked_Out,Notes) values (?,?,?,?,?)";
			connection.query(insertQuery,[Username,Asset_ID,Due_Date,Date_Checked_Out,note],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'add Failed - add checkout: ' + err});
				} else{
					dbconfig.getConnection(function(err,connection) {
						connection.query(updateQuery,[Asset_ID],function(err, result) {
							connection.release();
							if(err) {
								console.log({status: 1, message: 'add Failed - add checkout: ' + err});
							} else {
								res.redirect('/checkoutAsset')
							}
						});
					});;
				}
			});
		});
	},
	this.updateCheckInAssets = function(req,res) {
		let checkout = this;
		let CheckoutID = req.body.CheckoutID;
		let DateReturned =  new Date();		
		let Asset_ID = req.body.Asset_ID;
		let updateQuery ="UPDATE asset_checkouts SET Date_Returned = ? WHERE Checkout_ID = ?";
		dbconfig.getConnection(function(err,connection) {
			connection.query(updateQuery, [CheckoutID, DateReturned, Asset_ID], function(err, result){
				connection.release();
				if (err) {
					console.log({status: 1, message: 'Update Failed - UPDATE CheckOut: ' + err});
				} else {
					let updateAssetsPromise  = checkout.updateAssetsTbl(Asset_ID);
					updateAssetsPromise.then(function() {
						res.redirect('/checkInAsset');
					});
				}
			});
		});
	},
	this.updateAssetsTbl = function(asset_id) {
		return new Promise(function(fulfill, reject) {
			let updateQuery ='UPDATE assets SET Status_ID = 4 WHERE Asset_ID = ?';
			dbconfig.getConnection(function(err, connection) {
				connection.query(updateQuery, [asset_id], function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Add Failed - Get Students: ' + err}));
					} else {
						fulfill(true);	
					}
				});
			});
		});
	}
	this.addCheckIn =function(request,respond){
		let assetId = request.body.assetId;
		const CheckOut = 2;
	},
	this.renderCheckInAssets = function(req,res){
		let CheckoutPromise = Assets.getCheckOutAssets();
		CheckoutPromise.then(function(CheckOutAssets){
			res.render('checkin.ejs', {
				login : true,
				Assets : CheckOutAssets,
				moment : moment
			});
		});
	},
	this.renderCheckOutView= function(req, res){ 
		let checkOutAssetsPromise= Assets.getCheckedInAssets();
		checkOutAssetsPromise.then(function(assets) {
			let usernamePromise = Permissions.getAllStudents();
			usernamePromise.then(function(students) {;
				res.render('checkout.ejs', {
					login : true,
					assets : assets,
					students : students
				});
			});
		});
	}
}
module.exports = new Checkouts();