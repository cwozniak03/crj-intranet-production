var dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
let Status = require('../models/statuses');
function Assets() {
	this.addAsset = function(req,res) {
		let Asset_Name = req.body.txtAssetName;
		let Status = req.body.ddlStatus;
		let Asset_Serial = req.body.txtAssetSerial;
		dbconfig.getConnection(function(err,connection) {
			let insertQuery = "Insert into assets (Asset_Name, Status_ID, Serial_Number) Values (?,?,?)";
			connection.query(insertQuery,[Asset_Name,Status,Asset_Serial],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'add Failed - add Asset: ' + err});
				} else{
					res.redirect('/getAsset');
				}
			});
		});
	},
	this.getAssets = function(request,response) {
		return new Promise(function(fulfill, reject){
			let selectQuery = "SELECT * FROM assets";
			dbconfig.getConnection(function(err,connection){
				connection.query(selectQuery,function(err,result){
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'get Failed - get Asset: ' + err}));
					} else{
						fulfill(result);
					}
				});
			});
		});
	},
	this.getCheckedInAssets = function(){
		return new Promise(function(fulfill, reject){
			let selectQuery = "select * FROM assets WHERE Status_ID = 4 ";
			dbconfig.getConnection(function(err,connection) {
				connection.query(selectQuery,function(err,result) {
					connection.release();
					if(err) { 
						reject(console.log({status: 1, message: 'Get Failed - get Checked In Assets: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},
	this.getCheckOutAssets = function(){
		return new Promise(function(fulfill, reject){
			let selectQuery = "select * FROM assets as a ";
			selectQuery += "INNER JOIN asset_checkouts as ac ON ac.Asset_ID = a.Asset_ID ";
			selectQuery += "INNER JOIN statuses as s ON s.Status_ID = a.Status_ID ";
			selectQuery +="where a.Status_ID = 5";
			dbconfig.getConnection(function(err,connection) {
				connection.query(selectQuery,function(err,result) {
					connection.release();
					if(err) { 
						reject(console.log({status: 1, message: 'Get Failed - get Status_ID: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},	
	this.renderAssetView = function(req,res) {
		let assetPromise = this.getAssets();
		assetPromise.then(function(assets) {
			let statusPromise = Status.getStatus(req,res);
			statusPromise.then(function(status) {
				res.render('assets.ejs', {
					login : true,
					assets : assets,
					statuses : status
				});
			});
		});
	},	
	this.updateAsset = function(req,res) {
		let Asset_Name = request.body.txtAsset_Name;
		let Status = request.body.txtAsset_Status;
		let Asset_ID = request.body.txtAsset_ID;
		dbconfig.getConnection(function(err,connection) {
			let updateQuery = "UPDATE assets SET Asset_Name = ?, Status_ID = ? WHERE Asset_ID = ?";
			connection.query(updateQuery,[Asset_Name,Status,Asset_ID],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'UPDATE Failed - UPDATE Asset: ' + err});
				} else{
					res.redirect('/getAsset');
				}
			});
		});
	},
	this.deleteAsset = function(req, res) {
		let Asset_ID = request.body.txtAsset_ID;
		let deleteQuery = "DELETE FROM assets WHERE Asset_ID = ?";
		dbconfig.getConnection(function(err,connection) {
			connection.query(deleteQuery,[Asset_ID],function(err,result) {				
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Delete Failed - Delete Asset: ' + err});
				} else {
					res.redirect('/getAsset');
				}
			});					
		});
	}
}
module.exports = new Assets();