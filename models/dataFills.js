let DataExtract = require('../models/dataExtractionUpload');

function DataFills() {
	this.FillStudents = function(newStudentID, lastInsertedID, token) {
		let dataFill = this;
		let getStudentsPromise = DataExtract.getStudentInfo(lastInsertedID, token);
		getStudentsPromise.then(function(lastStudentInserted) {
			if(parseInt(lastStudentInserted) < parseInt(newStudentID)) {
				dataFill.FillStudents(newStudentID, lastStudentInserted, token);
			} else {
				let runGPAFillPromise = DataExtract.runGPASqlFunction();
				runGPAFillPromise.then(function(gpa_filled) {
					console.log("GPA's Filled!");
					let runClassGPAPromise = DataExtract.runClassGPACalculations();
					runClassGPAPromise.then(function() {
						console.log("ClassGPAFilled");
					});
				});
			}
		});
	}
	this.FillGrades = function(newGradeID, lastInsertedID, token) {
		let dataFill = this;
		let getGradesPromise = DataExtract.getStudentGrades(lastInsertedID, token);
		getGradesPromise.then(function(lastGradeInserted) {
			if(parseInt(lastGradeInserted) < parseInt(newGradeID)) {
				dataFill.FillGrades(newGradeID, lastGradeInserted, token);
			}
		});
	}
	this.FillAssignments = function(newAssignmentID, lastInsertedID, token) {
		let dataFill = this;
		let getAssignmentsPromise = DataExtract.addAllAssignmentsCurYear(newAssignmentID, lastInsertedID, token);
		getAssignmentsPromise.then(function(lastAssignmentInserted) {
			if(parseInt(lastAssignmentInserted) < parseInt(newAssignmentID)) {
				dataFill.FillAssignments(newAssignmentID, lastAssignmentInserted, token);
			} else {
				console.log("Calculating");
				let gradeCalculationPromise = DataExtract.runClassGradeFunction();
				gradeCalculationPromise.then(function() {
					console.log("Grades Calculated");
				});
			}
		}).catch(function(rejectExtraction) {
			console.log(rejectExtraction);
			if(parseInt(rejectExtraction.lastID) < parseInt(rejectExtraction.newID)) {
				dataFill.FillAssignments(rejectExtraction.newID, rejectExtraction.lastID, rejectExtraction.token);
			} else {
				console.log("Calculating");
				let gradeCalculationPromise = DataExtract.runClassGradeFunction();
				gradeCalculationPromise.then(function() {
					console.log("Grades Calculated");
				});
			}
		});
	}
	this.FillAbsenses = function(newAbsenseID, lastInsertedID,token) {
		let dataFill = this;
		console.log("Filling Absense " + newAbsenseID + " " + lastInsertedID);
		let getAbsensesPromise = DataExtract.getStudentAbsenses(lastInsertedID, token);
		getAbsensesPromise.then(function(lastAbsenseInserted) {
			if(parseInt(lastAbsenseInserted) < parseInt(newAbsenseID)) {
				dataFill.FillAbsenses(newAbsenseID, lastAbsenseInserted, token);
			}
		})
	}
	this.FillCourses = function(newCourseID, lastInsertedID) {
		let dataFill = this;
		console.log("Filling Courses " + newCourseID + " " + lastInsertedID);
		let getCoursesPromise = DataExtract.getCourses(lastInsertedID);
		getCoursesPromise.then(function(lastCourseInserted) {
			if(parseInt(lastCourseInserted) < parseInt(newCourseID)) {
				dataFill.FillCourses(newCourseID, lastCourseInserted);
			}
		});
	}
	this.FillTeachers = function(newTeachersID, lastInsertedID) {
		let dataFill = this;
		console.log("Filling Teachers")
		let getTeachersPromise = DataExtract.getTeachers(lastInsertedID);
		getTeachersPromise.then(function(lastTeacherInserted) {
			if(parseInt(lastTeacherInserted) < parseInt(newTeachersID)) {
				dataFill.FillTeachers(newTeachersID, lastTeacherInserted);
			}
		});
	}
	this.FillSections = function(newSectionsID, lastInsertedID, token) {
		let dataFill = this;
		console.log("Filling Sections");
		let getSectionsPromise = DataExtract.getSections(lastInsertedID, token);
		getSectionsPromise.then(function(lastSectionInserted) {
			if(parseInt(lastSectionInserted) < parseInt(newSectionsID)) {
				dataFill.FillSections(newSectionsID, lastSectionInserted);
			}
		});
	}
	this.FillStandardizedTestScores = function(newTestScoreID, lastInsertedID) {
		console.log("Filling Test Scores");
		let getStandardizedTestsPromise = DataExtract.getStandardizedTests(lastInsertedID);
		getStandardizedTestsPromise.then(function(lastTestInserted) {
			if(parseInt(lastTestInserted) < parseInt(newTestScoreID)) {
				FillStandardizedTestScores(newTestScoreID, lastTestInserted);
			}
		});
	},
	this.FillData = function(newID, lastID, getFunction, token) {
		console.log("Filling "+getFunction);
		let FunctionPromise = DataExtract[getFunction](lastID, token);
		FunctionPromise.then(function(lastID) {
			if(parseInt(lastID) < parseInt(newID)) {
				FillData(newID, lastID, getFunction);
			}
		})
	}
}
module.exports = new DataFills();