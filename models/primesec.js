// load up the user model
var bcrypt = require('bcrypt-nodejs');
var dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
var moment = require('moment');
var Promise = require('promise');
// expose this function to our app using module.exports

function PrimeSec() {	
	this.addPrimeSecEntry = function(req, res) {
		return new Promise(function(fulfill, reject){
			var PrimeSubInfo = {
                value: req.body.value,
               	day: req.body.day,
                period: req.body.period,
                username: req.body.username
            };
            var CurrentSchoolYear = getCurrentSchoolYearBegins();
			var insertQuery = "INSERT INTO subs_ps_assign (subs_ps_assign_value, subs_ps_assign_day, subs_ps_assign_period, subs_ps_assign_user, sub_ps_assign_school_year_begins) VALUES (?,?,?,?,?)";
			dbconfig.getConnection(function(err,connection) {
				connection.query(insertQuery,[PrimeSubInfo.value, PrimeSubInfo.day, PrimeSubInfo.period, PrimeSubInfo.username, CurrentSchoolYear], function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'INSERT Failed - Add PrimeSecEntry: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},
	this.updatePrimeSecEntry = function(req,res) {
		return new Promise(function(fulfill, reject){
			var PrimeSubInfo = {
                value: req.body.value,
               	day: req.body.day,
                period: req.body.period,
                primesec_id: req.body.id
            };
			var insertQuery = "UPDATE subs_ps_assign SET subs_ps_assign_value = ?, subs_ps_assign_day =?, subs_ps_assign_period=? WHERE subs_ps_assign_id = ?";
			dbconfig.getConnection(function(err,connection) {
				connection.query(insertQuery,[PrimeSubInfo.value, PrimeSubInfo.day, PrimeSubInfo.period, PrimeSubInfo.primesec_id], function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'UPDATE Failed - Update PrimeSecEntry: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},
	this.deletePrimeSecEntry = function(req,res) {
		return new Promise(function(fulfill, reject){
			var primesec_id = req.body.id;
			var insertQuery = "DELETE FROM subs_ps_assign WHERE subs_ps_assign_id = ?";
			dbconfig.getConnection(function(err,connection) {
				connection.query(insertQuery,[primesec_id], function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'DELETE Failed - DELETE PrimeSecEntry: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},
	this.getPrimeSecEntries = function(req,res,allUsers) {
		var login;
		if(req.session.isAuthenticated) { //Checks if user is authenticated and passes the login value to ejs
			login = true;
		} else {
			login = false;
		}
		var CurrentSchoolYear = getCurrentSchoolYearBegins();
		var selectQuery = "SELECT * FROM subs_ps_assign WHERE subs_ps_assign_school_year_begins = ?";
		dbconfig.getConnection(function(err,connection) {
			connection.query(selectQuery,[CurrentSchoolYear], function(err,result) {
				connection.release();
				if(err) {
					console.log(({status: 1, message: 'GET Failed - GET PrimeSecEntries: ' + err}));
				} else {
					allUsers= allUsers.sort();
					res.render('assignPrimeSec.ejs', {
		    			PrimeSec : result,
		    			login: login,
		    			pto_username : allUsers
		    		});
				}
			});
		});
	},
	this.getPrimeSecForPeriodDay = function(period_number, day) {
		return new Promise(function(fulfill, reject){
			var CurrentSchoolYear = getCurrentSchoolYearBegins();
			var selectQuery = "SELECT subs_ps_assign_user, subs_ps_assign_value FROM subs_ps_assign WHERE subs_ps_assign_school_year_begins = ? AND subs_ps_assign_period = ? AND subs_ps_assign_day = ?";
			dbconfig.getConnection(function(err,connection) {
				connection.query(selectQuery,[CurrentSchoolYear, period_number, day], function(err,results) {
					connection.release();
					if(err) {
						reject(({status: 1, message: 'GET Failed - GET PrimeSecEntries For Specific Day and Period: ' + err}));
					} else {
						fulfill(results);
					}
				});
			});
		});
	},
	this.assignPrimeSecEntry = function(req,res) {
		var value= req.body.ddlValue,
			username = req.body.pto_usernames,
			day = req.body.ddlDay,
			period = req.body.ddlPeriod;
		var CurrentSchoolYear = getCurrentSchoolYearBegins();
			
		var insertQuery = "INSERT INTO subs_ps_assign (subs_ps_assign_value, subs_ps_assign_day, subs_ps_assign_period, subs_ps_assign_user, subs_ps_assign_school_year_begins) ";
		insertQuery += "VALUES (?,?,?,?,?)";
		dbconfig.getConnection(function(err,connection) {
			connection.query(insertQuery,[value, day, period, username, CurrentSchoolYear], function(err,result) {
				connection.release();
				if(err) {
					console.log(({status: 1, message: 'INSERT Failed - Add PrimeSecEntries: ' + err}));
				} else {
					res.redirect("/assignPrimeSec");
				}
			});
		});
	}
}
function getCurrentSchoolYearBegins() {
	var SchoolBegin,
	    CurrentDate,
	    CurrentMonth;
	CurrentDate = new Date();
	CurrentMonth = CurrentDate.getMonth();
	if(CurrentMonth >=7 && CurrentMonth<=12) {
		SchoolBegin = CurrentDate.getFullYear();
	} else {
		SchoolBegin = CurrentDate.getFullYear() - 1;
	}
	return SchoolBegin;
}
module.exports = new PrimeSec();