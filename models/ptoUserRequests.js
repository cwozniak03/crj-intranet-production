// load up the model
const Promise = require('promise');
const dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
// expose this function to our app using module.exports
function PtoUserRequests() {	
	this.getPTOUsernamesByDate = function(date) {
		return new Promise(function(fulfill,reject) {
			var getQuery = "SELECT Distinct username FROM pto_request where pto_date = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(getQuery,[date],function(err,result) {
					connection.release();
					if(err) {
						reject(res.send({status: 1, message: 'Get Failed - PTO By Date: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});			
		});
	}
	
}
module.exports = new PtoUserRequests();