// load up the user model
/*var mysql = require('mysql');*/
var bcrypt = require('bcrypt-nodejs');
var dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
var moment = require('moment');
var ad = require('../config/ad.js').ad;
var Promise = require('promise');

function adRequests() {
	this.getManager = function(sAMAccountName) {
		adRequests = this;
		return new Promise(function(fulfill,reject) {
			var info = {}
			ad.findUser(sAMAccountName, function(err,user) {
				if(err) {
					reject(console.log('Get Manager Failed: '+JSON.stringify(err)));
				} else {
					var managerUsernamePromise = adRequests.getManagerUsername(user.manager);
					managerUsernamePromise.then(function(managerUsername) {
						info = {"givenName":user.cn,"managerUsername":managerUsername};
						fulfill(info);
					});
				}
			});
		});
	},
	this.getManagerUsername = function(cn) {
		return new Promise(function(fulfill,reject) {
			ad.findUser(cn, function(err,user) {
				if(err) {
					reject(console.log('Get Manager Failed: '+JSON.stringify(err)));
				} else {
					fulfill(user.sAMAccountName);
				}
			});
		});
	},
	this.getUserUsernameFromCN = function(cn) {
		return new Promise(function(fulfill,reject) {
			ad.findUser(cn, function(err,user) {
				if(err) {
					reject(console.log("Get Username Failed: "+JSON.stringify(err)));
				} else {
					fulfill(user.sAMAccountName);
				}
			})
		});
	}
	this.getUserFullName = function(cn) {
		return new Promise(function(fulfill,reject) {
			ad.findUser(cn, function(err,user) {
				if(err) {
					reject(console.log('Get User Full Name Failed: '+JSON.stringify(err)));
				} else {
					fulfill(user.displayName);
				}
			});
		});
	},
	this.getRegistrar = function() { //Get a list of anyone in the Registrar Group
        return new Promise(function(fulfill, reject) {
            var allUsers = [];
            ad.getUsersForGroup("Registrar",function(err,users) {
                if(err) {
                    reject(console.log('Group Users Get Error: ' +JSON.stringify(err)));
                } else {
                    for(i=0;i<users.length;i++)    {
                        allUsers.push(users[i].sAMAccountName);
                    }
                    fulfill(allUsers);
                }
            });
        });
    },
	this.getDirectReports = function(sAMAccountName) {
		let users = [];
		let adRequests = this;
		let promiseAggregate = [];
		return new Promise(function(fulfill,reject) {
			ad.findUser(sAMAccountName, function(err,user) {
				if(err) {
					reject(console.log('Get Direct Reports Failed: '+JSON.stringify(err)));
				} else {
					if(user.hasOwnProperty("directReports")) {
						if(typeof user.directReports != 'string') {
							user.directReports.forEach(function(value) {
								users.push(value);
								//If its the principal we want to go one layer deeper so that teachers who report to assistant principal will show up
								if(user.title=='Principal') {
									promiseAggregate.push(adRequests.getCascadesReports(value,adRequests));
								}
							});	
						} else {
							users.push(user.directReports);
						}
					}
					Promise.all(promiseAggregate).then(function(values) {
						for(var x=0;x<=values.length-1;x++) {
							users.push.apply(users,values[x]);
						}
						fulfill(users);
					});
				}
			});
		});
	},
	this.getCascadesReports = function(cn, adRequests) {
		let users = [];
		return new Promise(function(fulfill,reject) {
			ad.findUser(cn, function(err,user) {
				if(err) {
					reject(console.log('Get Cascate Reports Failed: '+JSON.stringify(err)));
				} else {
					if(user.hasOwnProperty("directReports")) {
						if(typeof user.directReports != 'string') {
							user.directReports.forEach(function(value) {
								users.push(value);
							});	
						} else {
							users.push(user.directReports);
						}
					}
					fulfill(users);
				}
			});
		});
	}
}

module.exports = new adRequests();