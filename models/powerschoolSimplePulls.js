let Promise = require('promise');
let config = require("../config/configFile.js").get(process.env.NODE_ENV.trim());
let https = require('https');
let url = require('url');

function PowerSchoolSimple() {
	this.getToken = function() { //Get the powerschool token
		return new Promise(function(fulfill, reject) {
		    let credentials = (new Buffer(config.powerschool.power_data_id + ":" +config.powerschool.power_data_secret)).toString('base64');

		    let options = url.parse(config.powerschool.powerschoolServerUrl);
		    options.path = '/oauth/access_token';
		    options.method = 'POST';
		    options.headers = {
		        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
		        'Authorization': 'Basic ' + credentials
		    };
		    var req = https.request(options, function(res) {
		        var data = '';
		        res.on('data', function(d) {
		            data += d;
		        });
		        res.on('end', function(){
		            if (data !== '') {
		                try {
		                    var obj = JSON.parse(data);
		                    fulfill(obj.access_token);
		                } catch ( exception ) {
		                    if (exception instanceof SyntaxError ) {
		                    } else {
		                        throw exception;
		                    } 
		                }		           
		            }
		        });
		    });
		    req.on('error', function(e) {
		        reject(console.error('Powerschool test server not found, please check the PowerSchool URL and try again\n' + e));
		    });
		    req.write("grant_type=client_credentials");
		    req.end();
		});
	},	
	this.getData = function(path,payload, tableKey, functionCausingError, token, functionRerun) {
		return new Promise(function(fulfill, reject) {
			let info = {};
			var headers = {
				'Content-Type':'application/json',
				'Authorization':'Bearer ' + token,
			};
			var options = {
				'host' : config.powerschool.host,
				'path' : path,
				'method': 'POST',
				'headers': headers,
			};
			var req = https.request(options, function(res) {
				if(res.statusCode != 200) {
					console.log(functionCausingError, res.statusCode);
				};
		        var data = '';
		        res.on('data', function(d) {
		            data += d;
		        });
		        res.on('end', function(){
		            if (data !== '') {
		                try {
		                    var obj = JSON.parse(data);
		                    if(obj.record) {
		                    	info = obj.record.map(function(r) {return r.tables[tableKey];});
		                    }
		                    fulfill(info);
		                } catch ( exception ) {
		                    if (exception instanceof SyntaxError ) {
		                        console.log(data);
		                    } else {
		                        throw exception;
		                    } 
		                }		           
		            }
		        });
		    });

		    req.write(JSON.stringify(payload));
		    req.on('error', function(e) {
		        reject(console.error('Powerschool test server not found, please check the PowerSchool URL and try again\n' + e));
		    });
		    req.end();
		});
		
	}			
}
module.exports = new PowerSchoolSimple();