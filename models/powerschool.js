// load up the user model
let moment = require('moment');
let Promise = require('promise');
let https = require('https');
let url = require('url');
let PrimeSec = require('../models/primesec');
let config = require("../config/configFile.js").get(process.env.NODE_ENV.trim());
const PTOUserRequests = require('../models/PTOUserRequests');
//const DataFills = require('../models/dataFills');

function PowerSchool() {
	this.getToken = function() { //Get the powerschool token
		return new Promise(function(fulfill, reject) {
		    let credentials = (new Buffer(config.powerschool.power_data_id + ":" +config.powerschool.power_data_secret)).toString('base64');

		    let options = url.parse(config.powerschool.powerschoolServerUrl);
		    options.path = '/oauth/access_token';
		    options.method = 'POST';
		    options.headers = {
		        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
		        'Authorization': 'Basic ' + credentials
		    };
		    var req = https.request(options, function(res) {
		        var data = '';
		        res.on('data', function(d) {
		            data += d;
		        });
		        res.on('end', function(){
		            if (data !== '') {
		                try {
		                    var obj = JSON.parse(data);
		                    fulfill(obj.access_token);
		                } catch ( exception ) {
		                    if (exception instanceof SyntaxError ) {
		                    } else {
		                        throw exception;
		                    } 
		                }		           
		            }
		        });
		    });
		    req.on('error', function(e) {
		        reject(console.error('Powerschool test server not found, please check the PowerSchool URL and try again\n' + e));
		    });
		    req.write("grant_type=client_credentials");
		    req.end();
		});
	},
	this.getBellSchedule = function(date) { //Get the bell schedule from powerschool
		var powerschool = this;
		return new Promise(function(fulfill, reject) {	
			//date = date || '2017-01-27';
			var powerschoolPromise = powerschool.getToken();
			powerschoolPromise.then(function(token) {
				var path = '/ws/schema/query/com.company.product.school.school_calendar';
				var headers = {
					'Content-Type':'application/json',
					'Authorization':'Bearer ' + token
				};
				var payload = {
					"schoolid":config.powerschool.school_id,
					"bell_day": date
				};
				var options = {
					'host' : config.powerschool.host,
					'path' : path,
					'method' : 'POST',
					'headers' : headers,
				}
				var req = https.request(options, function(res) {
					if(res.statusCode != 200) {
						console.log('statusCode Bell Schedule: ', res.statusCode);
					}
			        var data = '';
			        res.on('data', function(d) {
			            data += d;
			        });
			        res.on('end', function(){
			            if (data !== '') {
			                try {
			                    var obj = JSON.parse(data);
			                    const PERIODS_TO_KEEP = ['P1','P2','P3','P4','P5','P6','P7','P8'];
			                    if(obj.record !== undefined) {
									obj = obj.record.map(function(r) {return r.tables.calendar_day;}).filter(function(day){return PERIODS_TO_KEEP.indexOf(day.period_abbreviation) != -1;});
								}
			                    fulfill(obj);
			                } catch ( exception ) {
			                    if (exception instanceof SyntaxError ) {
			                        console.log(data);
			                    } else {
			                        throw exception;
			                    } 
			                }		           
			            }
			        });
			    });
			    req.on('error', function(e) {
			        reject(console.error('Powerschool test server not found, please check the PowerSchool URL and try again\n' + e));
			    });			    
			    req.write(JSON.stringify(payload));
			    req.end();
		    });
		});
	},
	this.getClasses = function(req, res) {
		var powerschool = this;
		return new Promise(function(fulfill, reject) {
			// Gets the current logged in user
			var email = req.body.pto_username + config.emailDomain
			var parsedDate = moment(req.body.pto_date).format("YYYY-MM-DD");
			var info = {'date' : parsedDate};
			var powerschoolPromise = powerschool.getToken();
			powerschoolPromise.then(function(token) {
				var path = '/ws/schema/query/com.company.product.school.all_sections_for_teacher';
				var headers = {
					'Content-Type':'application/json',
					'Authorization':'Bearer ' + token,
				};
				var payload = {
					"schoolid":config.powerschool.school_id,
					"date": parsedDate,
					"email": email,
				};
				var options = {
					'host' : config.powerschool.host,
					'path' : path,
					'method': 'POST',
					'headers': headers,
				};
				var req = https.request(options, function(res) {
					if(res.statusCode != 200) {
						console.log('statusCode Get Classes: ', res.statusCode);
					}
			        var data = '';
			        res.on('data', function(d) {
			            data += d;
			        });
			        res.on('end', function(){
			            if (data !== '') {
			                try {
			                    var obj = JSON.parse(data);
			                    if(obj.record) {
			                    	info.classes = obj.record.map(function(r) {return r.tables.sections;});
			                    }
			                    fulfill(info);
			                } catch ( exception ) {
			                    if (exception instanceof SyntaxError ) {
			                        console.log(data);
			                    } else {
			                        throw exception;
			                    } 
			                }		           
			            } else {
			            	fulfill(info);
			            }
			        });
			    });

			    req.write(JSON.stringify(payload));
			    req.on('error', function(e) {
			        reject(console.error('Powerschool test server not found, please check the PowerSchool URL and try again\n' + e));
			    });
			    req.end();
			});
		});
	},
	this.getClassInformation = function(token,section_id) {
		var powerschool = this;
		return new Promise(function(fulfill, reject) {
			var info = {};
				var path = '/ws/schema/query/com.company.product.school.class_information';
				var payload = {
					"section_id":section_id
				};
				let tableString = "sections"
				const functionError = "Get Class Information ";
				let dataPromise = powerschool.getData(path, payload, tableString, functionError, token);
				dataPromise.then(function(result) {
					fulfill(result);
				});
		});
	},
	this.getFreeTeachersByPeriod = function(req,res) {
		var powerschool = this;
		return new Promise(function(fulfill, reject) {
			var date = moment(req.body.date).format("YYYY-MM-DD");
			var period_id = req.body.period_id;
			var period_number = req.body.period_number;
			var day_cycle = req.body.day_cycle;
			var info = {};
			var powerschoolPromise = powerschool.getToken();
			powerschoolPromise.then(function(token) {
				var path = '/ws/schema/query/com.company.product.school.teachers_free_by_period';
				var headers = {
					'Content-Type':'application/json',
					'Authorization':'Bearer ' + token,
				};
				var payload = {
					"bell_day":date,
					"period_id":period_id
				};
				var options = {
					'host' : config.powerschool.host,
					'path' : path,
					'method': 'POST',
					'headers': headers,
				};
				var req = https.request(options, function(res) {
			        if(res.statusCode != 200) {
						console.log('statusCode get free teachers: ', res.statusCode);
					}
			        var data = '';
			        res.on('data', function(d) {
			            data += d;
			        });
			        res.on('end', function(){
			            if (data !== '') {
			                try {
			                    var obj = JSON.parse(data);
			                    if(obj.record) {
			                    	info.users = obj.record.map(function(r) {return r.tables.sections;});
			                    }
			                    let teachersOutPromise = PTOUserRequests.getPTOUsernamesByDate(date);
								teachersOutPromise.then(function(absentTeachers) {
									absentTeachers = JSON.parse(JSON.stringify(absentTeachers));
									absentTeachers = Object.keys(absentTeachers).map(function(key) {return absentTeachers[key].username+config.emailDomain});
				                    const r = new Set(absentTeachers);
									const users = info.users.filter(u => !r.has(u.email));
									info.users = users;
				                    var primeSecByDayPromise = PrimeSec.getPrimeSecForPeriodDay(period_number, day_cycle);
				                    primeSecByDayPromise.then(function(teachers) {
				                    	var avaliable = {'info': info, 'teachers': teachers};
				                    	fulfill(avaliable);
				                    });
				                });
			                } catch ( exception ) {
			                    if (exception instanceof SyntaxError ) {
			                        console.log(data);
			                    } else {
			                        throw exception;
			                    } 
			                }		           
			            }
			        });
			    });

			    req.write(JSON.stringify(payload));
			    req.on('error', function(e) {
			        reject(console.error('Powerschool test server not found, please check the PowerSchool URL and try again\n' + e));
			    });
			    req.end();
			});
		});
	},
	this.getGetCurrentPeriodID = function(time) {
		var powerschool = this;
		return new Promise(function(fulfill, reject) {
			var today = new Date();
			var date = moment(today).format("YYYY-MM-DD");
			var powerschoolPromise = powerschool.getToken();
			var info = {};
			powerschoolPromise.then(function(token) {
				var path = '/ws/schema/query/com.company.product.school.get_current_period_id';

				var payload = {
					"bell_day":date,
					"time":time
				};
				let tableString = "attendance";
				const functionError = "Get Current Period ID ";
				let dataPromise = powerschool.getData(path,payload, tableString, functionError, token);
				dataPromise.then(function(result) {
					fulfill(result);
				});				
			});
		});
	},
	this.getGetOriginalFirstPeriodID = function() {
		var powerschool = this;
		return new Promise(function(fulfill, reject) {
			var today = new Date();
			var date = moment(today).format("YYYY-MM-DD");
			var powerschoolPromise = powerschool.getToken();
			var info = {};
			powerschoolPromise.then(function(token) {
				var path = '/ws/schema/query/com.company.product.school.get_original_first_period';
				var payload = {
					"bell_day":date
				};
				let tableString = "calendar_day";
				const functionError = "Get Original First Period ID ";
				let dataPromise = powerschool.getData(path,payload, tableString, functionError, token);
				dataPromise.then(function(result) {
					fulfill(result);
				});	
			});
		});
	},
	this.checkAbsentSpecificPeriod = function(PeriodID,StudentID,token) {
		var powerschool = this;
		return new Promise(function(fulfill, reject) {
			var today = new Date();
			var date = moment(today).format("YYYY-MM-DD");
			var currentYear = getCurrentSchoolYearBegins();
			var yearID = parseInt(currentYear.toString().slice(-2))+10;
			var powerschoolPromise = powerschool.getToken();
			var info = {};

			var path = '/ws/schema/query/com.company.product.school.get_students_absent_specific_period';
			var payload = {
				"att_day":date,
				"year_id":yearID,
				"period_id": PeriodID,
				"student_id": StudentID
			};
			let tableString = "attendance";
			const functionError = "Check Absent Specific Period ";
			let dataPromise = powerschool.getData(path,payload, tableString, functionError, token);
			dataPromise.then(function(result) {
				fulfill(result);
			});
		});
	},
	this.getAllAbsentStudents = function(period_id) {
		var powerschool = this;
		return new Promise(function(fulfill, reject) {
			var today = new Date();
			var date = moment(today).format("YYYY-MM-DD");
			var currentYear = getCurrentSchoolYearBegins();
			var yearID = parseInt(currentYear.toString().slice(-2))+10;
			var powerschoolPromise = powerschool.getToken();
			var info = {};
			powerschoolPromise.then(function(token) {
				var path = '/ws/schema/query/com.company.product.school.get_students_not_absent_all_day';
				var payload = {
					"att_day":date,
					"year_id": yearID,
					"period_id": period_id
				};
				let tableString = "attendance";
				const functionError = "Check Absent Students ";
				let dataPromise = powerschool.getData(path,payload, tableString, functionError, token);
				dataPromise.then(function(result) {
					fulfill(result);
				});
			});
		});
	},
	this.getStudentInformation = function(StudentID, token) {
		var powerschool = this;
		return new Promise(function(fulfill, reject) {
			var info = {};
			var path = '/ws/schema/query/com.company.product.school.get_student_information';

			var payload = {
				"student_id":StudentID
			};
			let tableString = "students";
			const functionError = "Check Student Information ";
			let dataPromise = powerschool.getData(path,payload, tableString, functionError, token);
			dataPromise.then(function(result) {
				fulfill(result);
			});
		});
	},
	this.getDateExpression = function(date) {
		var powerschool = this;
		return new Promise(function(fulfill, reject) {
			var parsedDate = moment(req.body.pto_date).format("YYYY-MM-DD");
			var powerschoolPromise = powerschool.getToken();
			powerschoolPromise.then(function(token) {
				var path = '/ws/schema/query/com.company.product.school.calendar_expression';
				var payload = {
					"schoolid":config.powerschool.school_id,
					"date": parsedDate,
				};
				let tableString = "calendar_day";
				const functionError = "Date Expression ";
				let dataPromise = powerschool.getData(path,payload, tableString, functionError, token);
				dataPromise.then(function(result) {
					fulfill(result);
				});	
			});
		});
	},
	this.getSectionExpression = function(section_id) {
		var powerschool = this;
		return new Promise(function(fulfill, reject) {
			var powerschoolPromise = powerschool.getToken();
			var info = {};
			powerschoolPromise.then(function(token) {
				var path = '/ws/schema/query/com.company.product.school.section_expression';
				var payload = {
					"ids":section_id,
				};
				let tableString = "sections";
				const functionError = "Section Expression ";
				let dataPromise = powerschool.getData(path,payload, tableString, functionError, token);
				dataPromise.then(function(result) {
					fulfill(result);
				});	
			});
		});
	},
	this.getCurrentAssignments = function(newID, lastID, token) {
		var powerschool = this;
		let currentYearBegin = getCurrentSchoolYearBegins();
		let currentYearEnd = currentYearBegin+1;
		let begin_date = currentYearBegin + '-07-01';
		let end_date = currentYearEnd + '-06-30';
		return new Promise(function(fulfill, reject) {
			var info = {};
			var path = '/ws/schema/query/com.company.product.school.all_current_assignments?pagesize=700';
			var headers = {
				'Content-Type':'application/json',
				'Authorization':'Bearer ' + token,
			};
			var payload = {
				"begin_date": begin_date,
				"end_date": end_date,
				"last_id": lastID
			};
			var options = {
				'host' : config.powerschool.host,
				'path' : path,
				'method': 'POST',
				'headers': headers,
			};
			var req = https.request(options, function(res) {
				if(res.statusCode != 200) {
					console.log('statusCode current assignments: ', res.statusCode);
				};
		        var data = '';
		        res.on('data', function(d) {
		            data += d;
		        });
		        res.on('end', function(){
		            if (data !== '') {
		                try {
		                    var obj = JSON.parse(data);
		                    if(obj.record) {
		                    	info = obj.record.map(function(r) {return r.tables.assignmentscore;});
		                    }
		                    fulfill(info);
		                } catch ( exception ) {
		                    if (exception instanceof SyntaxError ) {
		                        console.log(data);
		                    } else {
		                        throw exception;
		                    } 
		                }		           
		            }
		        });
		    });

		    req.write(JSON.stringify(payload));
		    req.on('error', function(e) {
		    	if(e == "Error: socket hang up") {
		    		let retryData = {'newID': newID, 'lastID': lastID, 'token':token}
		    		reject(retryData);
		    		console.log("we had a socket hangup");
		        	//setTimeout(powerschool.getCurrentAssignments(lastID),1500);
		        } else {
		        	reject(console.error('Powerschool test server not found, please check the PowerSchool URL and try again\n' + e));
		        }	        
		    });
		    req.end();
		});
	},
	this.getClassesCount = function(username, date, token) {
		var powerschool = this;
		return new Promise(function(fulfill, reject) {
			// Gets the current logged in user
			let info = {};
			let count = {};
			var email = username + config.emailDomain;
			var parsedDate = moment(date).format("YYYY-MM-DD");
				var path = '/ws/schema/query/com.company.product.school.all_sections_for_teacher';
				var headers = {
					'Content-Type':'application/json',
					'Authorization':'Bearer ' + token,
				};
				var payload = {
					"schoolid":config.powerschool.school_id,
					"date": parsedDate,
					"email": email,
				};
				var options = {
					'host' : config.powerschool.host,
					'path' : path,
					'method': 'POST',
					'headers': headers,
				};
				var req = https.request(options, function(res) {
					if(res.statusCode != 200) {
						console.log('statusCode classes count: ', res.statusCode);
					}
			        var data = '';
			        res.on('data', function(d) {
			            data += d;
			        });
			        res.on('end', function(){
			            if (data !== '') {
			                try {
			                    var obj = JSON.parse(data);
			                    if(obj.record) {
			                    	info.classes = obj.record.map(function(r) {return r.tables.sections;});
			                    }
								if (info.classes === undefined || info.classes.length == 0) {
									count = {name: username, numClasses: 0};
								} else {
									count = {name: username, numClasses: info.classes.length};
								}
			                    fulfill(count);
			                } catch ( exception ) {
			                    if (exception instanceof SyntaxError ) {
			                        console.log(data);
			                    } else {
			                        throw exception;
			                    } 
			                }		           
			            } else {
			            	fulfill(0);
			            }
			        });
			    });

			    req.write(JSON.stringify(payload));
			    req.on('error', function(e) {
			        reject(console.error('Powerschool test server not found, please check the PowerSchool URL and try again\n' + e));
			    });
			    req.end();
		});
	},
	this.getData = function(path,payload, tableKey, functionCausingError, token, functionRerun) {
		return new Promise(function(fulfill, reject) {
			let info = {};
			var headers = {
				'Content-Type':'application/json',
				'Authorization':'Bearer ' + token,
			};
			var options = {
				'host' : config.powerschool.host,
				'path' : path,
				'method': 'POST',
				'headers': headers,
			};
			var req = https.request(options, function(res) {
				if(res.statusCode != 200) {
					console.log(functionCausingError, res.statusCode);
				};
		        var data = '';
		        res.on('data', function(d) {
		            data += d;
		        });
		        res.on('end', function(){
		            if (data !== '') {
		                try {
		                    var obj = JSON.parse(data);
		                    if(obj.record) {
		                    	info = obj.record.map(function(r) {return r.tables[tableKey];});
		                    }
		                    fulfill(info);
		                } catch ( exception ) {
		                    if (exception instanceof SyntaxError ) {
		                        console.log(data);
		                    } else {
		                        throw exception;
		                    } 
		                }		           
		            }
		        });
		    });

		    req.write(JSON.stringify(payload));
		    req.on('error', function(e) {
		        reject(console.error('Powerschool test server not found, please check the PowerSchool URL and try again\n' + e));
		    });
		    req.end();
		});
		
	}			
}
function getCurrentSchoolYearBegins() {
	var SchoolBegin,
	    CurrentDate,
	    CurrentMonth;
	CurrentDate = new Date();
	CurrentMonth = CurrentDate.getMonth();
	if(CurrentMonth >=7 && CurrentMonth<=12) {
		SchoolBegin = CurrentDate.getFullYear();
	} else {
		SchoolBegin = CurrentDate.getFullYear() - 1;
	}
	return SchoolBegin;
}
module.exports = new PowerSchool();