let dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
const Promise = require('promise');

function DataExtractTruncateTables() {
	this.truncateStudentAssignmentsTable = function() {
		return new Promise(function(fulfill, reject) {
			let getQuery = 'TRUNCATE TABLE student_assignment_grades'; 
			dbconfig.getConnection(function(err, connection){
				connection.query(getQuery,function(err, rows) {
					connection.release();
					if(err){
						reject(console.log("Err Truncate student_assignment_grades table: " + err));
					} else {
						fulfill(true);
					}
				});
			});
		});
	},
	this.truncateStudentDataTable = function() {
		return new Promise(function(fulfill, reject) {
			let getQuery = 'TRUNCATE TABLE student_data'; 
			dbconfig.getConnection(function(err, connection){
				connection.query(getQuery,function(err, rows) {
					connection.release();
					if(err){
						reject(console.log("Err Truncate student_data table: " + err));
					} else {
						fulfill(true);
					}
				});
			});
		});
	}
}
module.exports = new DataExtractTruncateTables();