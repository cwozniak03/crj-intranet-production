let bcrypt = require('bcrypt-nodejs');
let dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
let moment = require('moment');
let ad = require('../config/ad.js').ad;
let email = require('../config/gmail.'+ (process.env.NODE_ENV.trim() || "development") +'.js').email;
let Promise = require('promise');
let AdminPortal = require('../models/adminportal');
let ADRequests = require('../models/adRequests');
let config = require("../config/configFile.js").get(process.env.NODE_ENV.trim());


function emailSender() {
	this.emailTardies = function(student_list) {
		let samedaynextweek = moment().add(7, 'days').format("MM/DD/YYYY");
		let msg;
		let emailList = student_list.join(", ");
		//let name;
		//name = info.lastfirst.split(',');
		msg = "";
		//msg = name[1]+" "+name[0]+",\r\n";

		msg += "You have received a PH due to being late for class. \r\n";
		msg += "If you have any questions contact the Dean of Students immediately.\r\n";
		msg += "If you have CWSP on that day, then you must serve BEFORE that day\r\n";
		msg += "Failure to serve BEFORE " + samedaynextweek + " will result in an automatic Friday PH\r\n";
		msg += "\r\n";
		msg += "You must report to \r\n Room 202 on a Monday\r\n Room 123 on a Tuesday\r\n Room 129 on a Wednesday\r\n Room 204 on a Thursday.\r\n (Friday will be rotating schedule)\r\n";
		msg += "Be there no longer than 4 (FOUR) minutes after the bell at the beginning of your lunch period to serve your PH ON OR BEFORE " + samedaynextweek + "\r\n";
		msg += "LUNCH: \r\n";
		msg += "DO NOT go and get a hot lunch, or to go heat up your lunch.\r\n";
		msg += "Go directly to the PH Room.\r\n";
		msg += "A sack lunch will be provided if you did not bring your own lunch.\r\n";
		msg += "This is an unattended email account please DO NOT respond to this email.";
		let mailOptions = {
			text: msg,
			from: "Dean of Students",
			//to: info.student_schoolemail,
			bcc: emailList + ", " + config.emails.phEmail,
			subject: "PH Assigned"
		}
		email.sendMail(mailOptions, (err) => {
			if (err) {
				return console.log("Tardy PH Email Err: "+ err);
			}
		});
	},
	this.emailTardiesFriday = function(info) {
		const fri = 5;
		const sat = 6;
		let assignedDate = moment().add(1,'weeks').isoWeekday(fri).format("MM/DD/YYY");
		let msg;
		let name;
		name = info.lastfirst.split(',');
		msg = "";
		msg = name[1]+" "+name[0]+",\r\n";

		msg += "You have received a Friday PH due to receiving 2 PH's in a day. \r\nYou must report to the Dean of Students office at the end of day on " + assignedDate + "\r\n";
		msg += "See the Dean of Students if you have any questions.\r\n";
		msg += "This is an unattended email account please DO NOT respond to this email.";
		let mailOptions = {
			text: msg,
			from: "Dean of Students",
			to: info.student_schoolemail,
			bcc: config.emails.phEmail,
			subject: "Friday PH Assigned"
		}		
		email.sendMail(mailOptions, (err) => {
			if (err) {
				return console.log("Email Tardies Friday Err: "+ err);
			}
		});
	},
	this.emailAbsenses = function(info) {
		let absentEmailToPromise = AdminPortal.getAbsentEmailTo();
		absentEmailToPromise.then(function(email) {
			let msg;
			for(i=0;i<info.length;i++) {
				let name;
				name = info[i].lastfirst.split(',');
				msg = "";
				msg += "The following Student has been marked due to being absent this period, but present last period and not marked as absent for the day\r\n";
				msg += name[1]+" "+name[0]+",\r\n";
				let mailOptions = {
					text: msg,
					from: "Absent Students",
					to: email,
					subject: "Student Absent, needs attention"
				}		
				email.sendMail(mailOptions, (err) => {
					if (err) {
						return console.log("Email Absenses Err: "+ err);
					}
				});
			}
		});		
	},
	this.emailPHAssigned = function(res,student_email, reason, type, note) {
		const fri = 5;
		const sat = 6;
		let assignedDate;
		let subject;
		if(type == "PH") {
			assignedDate = moment().add(7,'days').format("MM/DD/YYYY");
		} else if(type == "Friday PH") {
			assignedDate = moment().add(1,'weeks').isoWeekday(fri).format("MM/DD/YYY");
		}
		let samedaynextweek = moment().add(7,'days').format("MM/DD/YYYY");
		let msg;
		msg = "";
		if(type == "PH") {
			subject = "PH Assigned";
			msg += "You have received a PH for "+ reason +". \r\n";
			msg += "With a note of " + note +". \r\n";
			msg += "If you have any questions contact the Dean of Students immediately.\r\n";
			msg += "If you have CWSP on that day, then you must serve BEFORE that day\r\n";
			msg += "Failure to serve BEFORE " + assignedDate + " will result in an automatic Friday PH\r\n";
			msg += "\r\n";
			msg +="You must report to \r\n Room 202 on a Monday\r\n Room 123 on a Tuesday\r\n Room 129 on a Wednesday\r\n Room 204 on a Thursday.\r\n (Friday will be rotating schedule)\r\n";
			msg += "Be there no longer than 4 (FOUR) minutes after the bell at the beginning of your lunch period to serve your PH ON OR BEFORE " + assignedDate + "\r\n";
			msg += "LUNCH: \r\n";
			msg += "DO NOT go and get a hot lunch, or to go heat up your lunch.\r\n";
			msg += "Go directly to the PH Room.\r\n";
			msg += "A sack lunch will be provided if you did not bring your own lunch.\r\n";
			msg += "This is an unattended email account please DO NOT respond to this email.";
		} else if (type == "Friday PH") {
			subject = "Friday PH Assigned";
			msg += "You have received a Friday PH for "+reason+".\r\n";
			msg += "With a note of " + note +". \r\n";
			msg += "Friday PH is an Afternoon PH from 4:15 PM to 5:15 PM.\r\n";
			msg += "Your Friday PH is on "+assignedDate+". Please be in Room 123 on or before 4:15 PM. Please make arrangements to be here on that day and on that time.\r\n";
			msg += "A reflection is mandatory before you leave, you must complete it properly or it will not be accepted and you will have to do it again.\r\n";
			msg += "If you have any questions, please come and see the Dean of Students.\r\n";
			msg += "This is an unattended email account please DO NOT respond to this email.";
		} else {
			subject = "Saturday PH Assigned";
			msg += "You have received a Saturday PH for "+reason+".\r\n";
			msg += "With a note of " + note +". \r\n";
			msg += "Saturday PH is on Saturday mornings at the school from 9 AM to 12 Noon.\r\n";
			msg += "The Dean of Students or Dean of Students will contact you about what date you will serve your Saturday PH.\r\n";
			msg += "Please make arrangements to be here on that day and on that time.\r\n";
			msg += "If you have any questions, please come and see the Dean of Students.\r\n";
			msg += "This is an unattended email account please DO NOT respond to this email.";
		}
		let mailOptions = {
			text: msg,
			from: "Dean of Students",
			to: student_email+config.emailDomain,
			bcc: config.emails.phEmail,
			subject: subject
		}		
		email.sendMail(mailOptions, (err) => {
			if (err) {
				console.log(moment.format());
				return console.log("Email PH Err: "+ err);
			}
		});
	},
	this.emailPHMissed = function(student_username, date, assignedDate) {
		let msg;
		msg = "";
		msg += "Dear student, you are receiving a Friday After School PH for not attending a Lunch PH on "+date+".\r\n";
		msg += "Your After School PH is scheduled for Friday, "+assignedDate+".\r\n";
		msg += "If you have any questions please come and see the Dean of Students\r\n";
		msg += "This is an unattended email account please DO NOT respond to this email.\r\n";
		msg += "If you have any questions please email phs@cristoreyjesuit.org"
		let mailOptions = {
			text: msg,
			from: "Dean of Students",
			to: student_username+config.emailDomain,
			subject: "PH Reminder!"
		}		
		email.sendMail(mailOptions, (err) => {
			if (err) {
				console.log(moment.format());
				return console.log("Email PH Missed Err: "+ err);
			}
		});
	},
	this.emailManager = function(manager,username,start_date, end_date, note,value,givenName) {
		let msg;
		let subjectLine;
		let ccaddress = username+config.emailDomain;
		if(manager == config.usernames.backupFacultyPTOApprover) {
			ccaddress+=", "+config.emails.principalEmail;
			this.emailRegistrarTeacherPTORequest(givenName, start_date, end_date, note, value);
		}
		if(end_date != "") {
			subjectLine = "PTO Request from " + givenName + " from " + moment(start_date).format("MM/DD/YYYY") + " to " + moment(end_date).format("MM/DD/YYYY");
		} else {
			subjectLine = "PTO Request from " + givenName + " on " + moment(start_date).format("MM/DD/YYYY");
		}
		msg = "";
		msg += "You have received a new PTO request from one of your team members.\r\n";
		msg += givenName + " has requested ";
		if(end_date != "") {
			msg += "PTO from " + moment(start_date).format("MM/DD/YYYY") + " to " + moment(end_date).format("MM/DD/YYYY");
		} else {
			if(value == 1 ) { msg += " a full day of PTO on "} else {msg += " a half day of PTO on "}
			msg += moment(start_date).format("MM/DD/YYYY") + "\r\n";			
		}	
		if(note != "") {
			msg += "with the accompanying note of "+note+".\r\n";
		}
		msg += " You can approve this PTO by going to the following link: http://southwell:8081 and clicking on the PTO Approval Button. \r\n";
		let mailOptions = {
			text: msg,
			from: "PTO System",
			to: manager+config.emailDomain,
			cc: ccaddress,
			subject: subjectLine
		}		
		email.sendMail(mailOptions, (err) => {
			if (err) {
				console.log(moment.format());
				return console.log("Email Manager Err: "+ err);
			}
		});
	},
    this.emailRegistrarTeacherPTORequest = function(givenName, start_date, end_date, note, value) {
        let getRegistrarPromise = ADRequests.getRegistrar();
        getRegistrarPromise.then(function(registrars) {
            let registrarList = "";
            for(x=0;x<=registrars.length-1;x++) {
                if (x != registrars.length-1){
                    registrarList += registrars[x] + config.emailDomain + ", ";
                } else {
                    registrarList += registrars[x] + config.emailDomain;
                }
            }
            let msg;
            let subjectLine;
            if(end_date != "") {
                subjectLine = "PTO Request from " + givenName + " from " + moment(start_date).format("MM/DD/YYYY") + " to " + moment(end_date).format("MM/DD/YYYY");
            } else {
                subjectLine = "PTO Request from " + givenName + " on " + moment(start_date).format("MM/DD/YYYY");
            }
            msg = "";
            msg += "There is a new Teacher PTO Request! Check for Substitute Needs!\r\n";
            msg += givenName + " has requested ";
            if(end_date != "") {
                msg += "PTO from " + moment(start_date).format("MM/DD/YYYY") + " to " + moment(end_date).format("MM/DD/YYYY");
            } else {
                if(value == 1 ) { msg += " a full day of PTO on "} else {msg += " a half day of PTO on "}
                msg += moment(start_date).format("MM/DD/YYYY") + "\r\n";            
            }    
            if(note != "") {
                msg += "with the accompanying note of "+note+".\r\n";
            }
			let mailOptions = {
                text: msg,
                from: "PTO System",
                to: registrarList,
                subject: subjectLine
			}		
			email.sendMail(mailOptions, (err) => {
				if (err) {
					console.log(moment.format());
					return console.log("Email Registrar Err: "+ err);
				}
			});            
        });
    },
    this.emailPHReminder = function(student_list) {
		var msg;
		let emailList = student_list.join(", ");
		msg = "";

		msg += "You have recieved a PH that is to be attended today at Lunch. Please remember to show up during your scheduled Lunch Time\r\n";
		msg += "You must report to \r\n Room 202 on a Monday\r\n Room 123 on a Tuesday\r\n Room 129 on a Wednesday\r\n Room 204 on a Thursday.\r\n (Friday will be rotating schedule)\r\n";
		msg += "Lunch will be provided if you did not bring your lunch.\r\n";
		msg += "See Dean Gutierrez if you have any questions.\r\n";
		msg += "This is an unattended email account please DO NOT respond to this email.";

		email.send({
			text: msg,
			from: "Dean of Students",
			bcc: emailList,
			subject: "PH Reminder!"
		}, function(err,message) {
			if(err) {
				console.log(err);
			}		
		});
	},
	this.sendEmailtoHR = function(username,start_date,end_date,pto_notes,pto_value) {
		let msg;
		msg = "";
		msg += username+" has been approved for a PTO request on " + start_date + " ending on "+end_date+".\r\n";
		msg += "This will put them over their alloted PTO.\r\n";
		msg += "This request has the accompanying note: "+pto_notesnote;
		let mailOptions = {
			text: msg,
			from: "PTO Requests",
			to: config.emails.hrEmail,
			subject: "PTO OVERAGE!"
		}		
		email.sendMail(mailOptions, (err) => {
			if (err) {
				console.log(moment.format());
				return console.log("Email PH Missed Err: "+ err);
			}
		});
	}	
}
module.exports = new emailSender();