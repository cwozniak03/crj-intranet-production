// load up the user model
const bcrypt = require('bcrypt-nodejs');
const dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
const moment = require('moment');
const ad = require('../config/ad.js').ad;
const EmailSender = require('../models/emailSender');
const Promise = require('promise');
const Permissions = require('../models/permissions');
// expose this function to our app using module.exports


//TODO add Promises to add, get, update, delete
function Contract() {
	this.createContract = function(contractInfo) {
		//TODO: Format date to YYYY-MM-DD
		let insertQuery = "INSERT INTO contracts (company_name, start_date, end_date) ";
		insertQuery += "values (?,?,?,?,?,?,?,?)";
		dbconfig.getConnection(function(err, connection){
			connection.query(insertQuery,[contractInfo.company_name, contractInfo.start_date, contractInfo.end_date],function(err, rows) {
				connection.release();
				if(err){
					console.log({status: 1, message: 'Post Failed - create Contract: ' + err});
				} else {
					res.redirect("/contract"+rows.insertId);
				}
			});
		});
	},
	this.getContract = function(contract_id) {
		//var today = moment().format("YYYY-MM-DD");
		var selectQuery = "SELECT * FROM contracts WHERE contract_id = ?";
		dbconfig.getConnection(function(err, connection){
			connection.query(selectQuery,[contract_id], function(err, results) {
				connection.release();
				if(err) {
					console.log({stats: 1, message: 'SELECT Failed - Get Contract: ' + err});
				} else {
					res.render('contracts/contract', {
						contract : result,
						moment: moment,
						login: true
					});
				}
			});
		});
	},
	this.listContracts = function(req, res) {
		var selectQuery = "SELECT * FROM contracts";
		dbconfig.getConnection(function(err, connection){
			connection.query(selectQuery, function(err, results) {
				connection.release();
				if(err) {
					console.log({stats: 1, message: 'SELECT Failed - Get Contracts: ' + err});
				} else {
					res.render('contracts/contract', {
						contracts : result,
						moment: moment,
						login: true
					});
				}
			});
		});
	},
	this.getDepartments = function() {
		return new Promise(function(fulfill, reject) {
			var selectQuery = "SELECT * FROM departments";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery, function(err, results){
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Get Failed - Get Departments: ' + err}));
					} else {
						fulfill(results);
					}
				});
			});
		});
	},
	this.updateContract = function(contractInfo) {
		var updateQuery = 'UPDATE contracts SET company_name = ?, start_date = ?, end_date = ?, termination_clause = ?, signatory = ?, renewal_terms = ?, department = ?,';
		updateQuery += ' contract_owner = ? WHERE contract_id = ?';
		dbconfig.getConnection(function(err, connection) {
			connection.query(updateQuery,[contractInfo.company_name, contractInfo.start_date, contractInfo.end_date, contractInfo.termination_clause, contractInfo.signatory, contractInfo.renewal_terms, contractInfo.department, contractInfo.contract_owner, contract.id],function(err,result) {
				connection.release();
				if(err){
					console.log({status:1, message: 'Update Failed - Update Contract:' + err});
				} else {
					res.redirect("/contract"+contract.id);
				}
			});
		});
	},
	this.deleteContract = function(contract_id) {
		var deleteQuery = 'DELETE FROM contracts where contract_id = ?';
		dbconfig.getConnection(function(err, connection) {
			connection.query(updateQuery,[contractInfo.company_name, contractInfo.start_date, contractInfo.end_date, contractInfo.termination_clause, contractInfo.signatory, contractInfo.renewal_terms, contractInfo.department, contractInfo.contract_owner, contract.id],function(err,result) {
				connection.release();
				if(err){
					console.log({status:1, message: 'Delete Failed - Delete Contract:' + err});
				} else {
					res.redirect("/contractList");
				}
			});
		});
	}
}

module.exports = new Contract();