// load up the user model
let bcrypt = require('bcrypt-nodejs');
let dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
let moment = require('moment');
let Promise = require('promise');
// expose this function to our app using module.exports

function AdminPortal() {	
	this.updateMaxCarryOver = function(req, res) {
		var adminportal = this;
		var existsPromise = adminportal.checkValueExists("Max Carry Over");
		existsPromise.then(function(result) {
			console.log(result);
			if(result) {
				adminportal.addAdminValue(req.body.txtMaxCarryOver, "Max Carry Over",res);
			} else {
				adminportal.updateAdminValue(req.body.txtMaxCarryOver, "Max Carry Over",res);
			}
		});
	},
	this.updateFacultyPTO = function(req, res) {
		var adminportal = this;
		var existsPromise = adminportal.checkValueExists("Faculty PTO");
		existsPromise.then(function(result) {
			if(result) {
				adminportal.addAdminValue(req.body.txtFacultyPTO, "Faculty PTO",res);
			} else {
				adminportal.updateAdminValue(req.body.txtFacultyPTO, "Faculty PTO",res);
			}
		});
	},
	this.updateStaffPTO = function(req, res) {
		var adminportal = this;
		var existsPromise = adminportal.checkValueExists("Staff PTO");
		existsPromise.then(function(result) {
			if(result) {
				adminportal.addAdminValue(req.body.txtStaffPTO, "Staff PTO",res);
			} else {
				adminportal.updateAdminValue(req.body.txtStaffPTO, "Staff PTO",res);
			}
		});
	},
	this.updatePTOResetDate = function(req, res) {
		var adminportal = this;
		var existsPromise = adminportal.checkValueExists("PTO Reset Date");
		existsPromise.then(function(result) {
			if(result) {
				adminportal.addAdminValue(req.body.txtPTOResetDate, "PTO Reset Date", res);
			} else {
				adminportal.updateAdminValue(req.body.txtPTOResetDate, "PTO Reset Date", res);
			}
		})
	},
	this.updateAbsentEmailTo = function(req, res) {
		var adminportal = this;
		var existsPromise = adminportal.checkValueExists("Absent Email To");
		existsPromise.then(function(result) {
			if(result) {
				adminportal.addAdminValue(req.body.txtAbsentEmailTo, "Absent Email To", res);
			} else {
				adminportal.updateAdminValue(req.body.txtAbsentEmailTo, "Absent Email To", res);
			}
		})
	},
	this.addPTOApprover = function(req, res) {
		var insertQuery = "INSERT INTO admin_options (admin_options_name, admin_options_value) VALUES (?,?)";
		dbconfig.getConnection(function(err, connection) {
			connection.query(insertQuery,["PTO Approver",req.body.txtPTOApprover],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Add Failed - Insert Admin Option: ' + err});
				} else {
					res.redirect('/adminPortal');	
				}
			});
		});
	},
	this.updatePTOApprover = function(req, res) {
		var updateQuery = "UPDATE admin_options set admin_options_value = ? WHERE admin_options_id = ?";
		dbconfig.getConnection(function(err, connection) {
			connection.query(updateQuery,[req.body.value, req.body.id],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Update Failed - Update PTO Approver Group: ' + err});
				} else {
					res.redirect('/adminPortal');
				}
			})
		});
	},
	this.deletePTOApprover = function(req, res) {
		var deleteQuery = "DELETE FROM admin_options WHERE admin_options_id = ?";
		dbconfig.getConnection(function(err, connection) {
			connection.query(deleteQuery,[req.body.id],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Delete Failed - Delete PTO Approver Group: ' + err});
				} else {
					res.redirect('/adminPortal');
				}
			})
		});
	},	
	this.addAdminValue = function(value, name, res) {
		var insertQuery = "INSERT INTO admin_options (admin_options_name, admin_options_value) VALUES (?,?)";
		dbconfig.getConnection(function(err, connection) {
			connection.query(insertQuery,[name,value],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Add Failed - Insert Admin Option: ' + err});
				} else {
					res.redirect('/adminPortal');	
				}
			});
		});
	},
	this.addPTODays = function(req,res) {
		let date = new Date();
		date = moment(date).format('YYYY-MM-DD');
		let insertQuery = "INSERT INTO pto_adtl_days (username, num_adtl_days, date, user_who_added) VALUES (?,?,?,?)";
		dbconfig.getConnection(function(err, connection) {
			connection.query(insertQuery,[req.body.username, req.body.txtNumDays, date, req.session.username],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Add Failed - Insert Admin Option: ' + err});
				} else {
					res.redirect('/adtlDays');	
				}
			});
		});
	},
	this.updateAdminValue = function(value, name, res) {
		console.log("Updating "+ value + " " + name);
		var updateQuery = "UPDATE admin_options SET admin_options_value = ? WHERE admin_options_name = ?";
		dbconfig.getConnection(function(err, connection) {
			connection.query(updateQuery,[value, name],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Update Failed - Insert Admin Option: ' + err});
				} else {
					res.redirect('/adminPortal');	
				}
			});
		});
	},
	this.checkValueExists = function(value) {
		return new Promise(function(fulfill, reject){
			var selectQuery = "Select admin_options_id FROM admin_options WHERE admin_options_name = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,[value],function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Select Failed - Check Admin Exists: ' + err}));
					} else {
						if(!result.length) {
							fulfill(true);
						} else {
							fulfill(false);
						}	
					}
				});
			});
		});
	},
	this.getPortalData = function(req,res) {
		var selectQuery = "Select * from admin_options";
		var results = {};
		var pto_approvers = {};
		dbconfig.getConnection(function(err, connection) {
			connection.query(selectQuery,function(err, result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Select Failed - Get All Admin Options:' + err});
				} else {
					for(var x=0;x<=result.length-1;x++) {
						if(result[x].admin_options_name == "PTO Approver") {
							pto_approvers[result[x].admin_options_id] = result[x].admin_options_value;
						} else {
							results[result[x].admin_options_name] = result[x].admin_options_value;
						}
					}
					res.render('adminPortal.ejs', {
						login : true,
						results : results,
						pto_approvers : pto_approvers
					});
				}
			});
		});
	},
	this.getResetDate = function(req,res) {
		return new Promise(function(fulfill, reject){
			var selectQuery = "Select admin_options_value FROM admin_options WHERE admin_options_name = 'PTO Reset Date'";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,function(err, result) {
					connection.release();
					if(err) {
						console.log(err);
						reject(console.log({status: 1, message: 'Select Failed - Get PTO Reset Date: ' + err}));
					} else {
						fulfill(result[0].admin_options_value);
					}
				});
			});
		});
	},
	this.getAbsentEmailTo = function() {
		return new Promise(function(fulfill, reject){
			var selectQuery = "Select admin_options_value FROM admin_options WHERE admin_options_name = 'Absent Email To'";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,function(err, result) {
					connection.release();
					if(err) {
						console.log(err);
						reject(console.log({status: 1, message: 'Select Failed - Get Absent Email To: ' + err}));
					} else {
						fulfill(result[0].admin_options_value);
					}
				});
			});
		});
	}
}
module.exports = new AdminPortal();