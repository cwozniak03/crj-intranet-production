// load up the user model
const dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
const moment = require('moment');
const Powerschool = require('../models/powerschool');
const Promise = require('promise');
// expose this function to our app using module.exports

function Substitutes() {
	this.getSubNeeds = function(req,res) {
		var login;
		if(req.session.isAuthenticated) { //Checks if user is authenticated and passes the login value to ejs
			login = true;
		} else {
			login = false;
		}

		var substitute = this;
		var today = new Date();
		var date = moment(req.params.date).format("YYYY-MM-DD") || moment(today).format("YYYY-MM-DD");
		var teachersAbsent = {};
		var classes = {};
		var duties = {};
		var lastUser = "";
		let fullName = "";
		var getQuery = "SELECT pto_request.username, pto_date, psc.pto_classes_class_id, pto_classes_id, psc.pto_substitute_assigned ";
		getQuery += "as 'class_substitute_assigned', u.full_name FROM pto_request ";
		getQuery += "INNER JOIN pto_substitute_classes as psc ON pto_request.pto_request_id = psc.pto_request_id ";
		getQuery += "INNER JOIN users as u ON pto_request.username = u.username ";
		getQuery += "WHERE pto_date = ? AND pto_approved = 1";
		dbconfig.getConnection(function(err,connection){
			connection.query(getQuery,[date],function(err, results){
				connection.release();
				if(err){
					console.log({status: 1, message: 'Get Failed - Sub Needs: ' + err});
				} else {
					results.forEach(function(item){
						if(item.username != lastUser && lastUser != "") {
							teachersAbsent[lastUser] = {
								duties: duties,
								classes: classes,
								name: fullName
							};
							classes = {};
							duties = {};
						}
						classes[item.pto_classes_id] = {'class_id':item.pto_classes_class_id,'assigned':item.class_substitute_assigned};
						duties[item.pto_substitute_duties_id]={'pto_duty_id':item.pto_substitute_duty,'assigned':0};
						lastUser = item.username;
						fullName = item.full_name;
					});
					//Have to push the last values through as it wont go to the top of the loop again.
					teachersAbsent[lastUser] = {
						duties: duties,
						classes: classes,
						name:fullName
					};
					classes = {};
					duties = {};
					var promiseStack = [];
					var periodsStack = {};
					var teachersStack = {};
					var powerschoolPromise = Powerschool.getToken();
					powerschoolPromise.then(function(token) {
						var bellsPromise = Powerschool.getBellSchedule(date);
						bellsPromise.then(function(bellSchedule) {
							var accumulator = [];
							for(var key in teachersAbsent) {
								for(var key2 in teachersAbsent[key].classes) {
									//console.log(teachersAbsent[key].classes[key2].class_id);
									promiseStack.push(Powerschool.getClassInformation(token,teachersAbsent[key].classes[key2].class_id));
								}
							}
							Promise.all(promiseStack).then(function(values) {
								for(var key in teachersAbsent) {
									for(var key2 in teachersAbsent[key].classes) {
										for(var key3 in values) {
											values[key3].forEach(function(info) {
												if(info.section_id == teachersAbsent[key].classes[key2].class_id) {
													teachersAbsent[key].classes[key2].pto_classes_id = key2;
													teachersAbsent[key].classes[key2].class_id = values[key3];
												}
											});
										}
									}
								}
								var needs = {'teachersAbsent': teachersAbsent, 'schedule':bellSchedule};
								for(var key in teachersAbsent) {
									for(var key2 in teachersAbsent[key].classes) {
										for(y=0; y<=bellSchedule.length-1; y++) {
											if(teachersAbsent[key].classes[key2].class_id[0].period_number == bellSchedule[y].period_number) {
												periodsStack["P"+bellSchedule[y].period_number] = teachersAbsent[key].classes[key2];
											} else {
												if (!("P"+bellSchedule[y].period_number in periodsStack)) {
													periodsStack["P"+bellSchedule[y].period_number] = {};
												}					
											}
										}
									}
									teachersStack[key] = periodsStack;
									periodsStack = {};
								}
								res.render('assignsubs.ejs', {
					    			login : login,
					    			moment : moment,
					    			periods : teachersStack,
					    			needs: needs,
					    			date: date
					    		});
							});
						});
					});
				}
			});
		});
	},
	this.markSubAssigned = function(pto_classes_id) { //Gets all of the request types from the database.
		return new Promise(function(fulfill,reject) {
			var updateQuery = "UPDATE pto_substitute_classes SET pto_substitute_assigned = 1 WHERE pto_classes_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(updateQuery,[pto_classes_id],function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'UPDATE Failed - Assign Sub: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},
	this.logSubClassAssigned = function(course_name, sub_name, class_date, pto_class_id, period_number) {
		return new Promise(function(fulfill,reject){
			var insertQuery = "INSERT INTO pto_sub_class_log (pto_sub_class_log_course_name, pto_sub_class_log_sub_name, pto_sub_class_log_class_date, pto_classes_id, pto_sub_class_log_period_number) VALUES (?,?,?,?,?)";
			dbconfig.getConnection(function(err, connection) {
				connection.query(insertQuery,[course_name, sub_name, class_date,pto_class_id, period_number], function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'INSERT Failed - Insert Sub Log: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},
	this.getCalendarID = function(pto_class_id) {
		return new Promise(function(fulfill,reject){
			var updateQuery = "SELECT event_id FROM pto_substitute_classes WHERE pto_classes_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(updateQuery,[pto_class_id], function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'UPDATE Failed - UPDATE Calendar EventID: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});		
	},
	this.addCalendarID = function(event_id,pto_class_id) {
		return new Promise(function(fulfill,reject){
			var updateQuery = "UPDATE pto_substitute_classes SET event_id = ? WHERE pto_classes_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(updateQuery,[event_id,pto_class_id], function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'UPDATE Failed - UPDATE Calendar EventID: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});		
	}
	this.deleteSub = function(id) {
		return new Promise(function(fulfill,reject){
			var deleteQuery = "DELETE FROM  pto_sub_class_log WHERE pto_sub_class_log.pto_classes_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(deleteQuery,[id], function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'Delete Failed - Delete Sub: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},
	this.deleteCalendarID = function(pto_class_id) {
		return new Promise(function(fulfill,reject){
			var updateQuery = "UPDATE pto_substitute_classes SET event_id = 0, pto_substitute_assigned = 0 WHERE pto_classes_id = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(updateQuery,[pto_class_id], function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'UPDATE Failed - UPDATE Calendar EventID: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});			
	},
	this.getClassesSubbedByTeacher = function(t_username) {
		return new Promise(function(fulfill,reject){
			let yearBegin = getCurrentFiscalYear();
			let beginDate = yearBegin+'-07-01';
			let endDate = yearBegin+1+'06-30';
			let teacher = [];
			var selectQuery = "Select COUNT(*) As classes From pto_sub_class_log WHERE pto_sub_class_log_sub_name = ? and pto_sub_class_log_class_date >=? and ";
			selectQuery += "pto_sub_class_log_class_date <=?"
			dbconfig.getConnection(function(err, connection){
				connection.query(selectQuery,[t_username,beginDate,endDate], function(err, result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'GET Failed - Get Teacher Classes: ' + err}));
					} else {
						teacher.push({
							"name":t_username,
							"classes": result[0].classes
						});
						fulfill(teacher);
					}					
				});
			});
		});
	}

	function getCurrentFiscalYear() {
		var FiscalBegin,
		    CurrentDate,
		    CurrentMonth;
		CurrentDate = new Date();
		CurrentMonth = CurrentDate.getMonth();
		if(CurrentMonth >=6 && CurrentMonth<=12) {
			FiscalBegin = CurrentDate.getFullYear();
		} else {
			FiscalBegin = CurrentDate.getFullYear() - 1;
		}
		return FiscalBegin;
	}
}
module.exports = new Substitutes();