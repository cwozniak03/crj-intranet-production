// load up the user model
let bcrypt = require('bcrypt-nodejs');
let dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
let moment = require('moment');
let ad = require('../config/ad.js').ad;
let Promise = require('promise');
let config = require("../config/configFile.js").get(process.env.NODE_ENV.trim());

function Permissions() {
	this.hasRole = function(username, role) {
		return new Promise(function(fulfill, reject) {
			ad.isUserMemberOf(username+config.adDomain, role, function(err, isMember) {
				if(err) {
					reject(console.log('Check Role Access Error: ' +JSON.stringify(err)));
				} else {
					fulfill(isMember);
				}
			});
		});		
	}
	this.canApprovePTO = function(req,res) { //Check if a person has the ability to approve PTO requests
		return new Promise(function(fulfill, reject) {
			ad.isUserMemberOf(req.session.username+config.adDomain, config.adGroupNames.approvePTO, function(err, isMember) {
				if(err) {
					reject(console.log('PTO Approve Access Error: ' +JSON.stringify(err)));
				} else {
					fulfill(isMember);
				}
			});
		});
	},
	this.getAllFacultyStaff = function(req,res) { //Get a list of all Faculty and Staff for a dropdown and remove accounts that are "service" accounts
		return new Promise(function(fulfill, reject) {
			var allUsers = [];
			var removeUsers = config.adGroupNames.removeUsers;
			ad.getUsersForGroup(config.adGroupNames.facultyStaff,function(err,users) {
				if(err) {
					reject(console.log('Group Users Get Error: ' +JSON.stringify(err)));
				} else {
					for(i=0;i<users.length;i++)	{
						if(removeUsers.indexOf(users[i].sAMAccountName) === -1) {
							allUsers.push(users[i].sAMAccountName);
						}
					}
					fulfill(allUsers);
				}
			});
		});
	},
	this.getAllStudents = function() {
		return new Promise(function(fulfill, reject) {
			var info = {};
			var allUsers = {};
			ad.getUsersForGroup(config.adGroupNames.students,function(err,users) {
				if(err) {
					reject(console.log('Group Users Get Error: ' +JSON.stringify(err)));
				} else {
					for(i=0;i<users.length;i++)	{
						if(users[i].dn.indexOf("Disabled Accounts") == -1) {
							allUsers[users[i].sAMAccountName] = {'username':users[i].displayName};
						}
					}
					fulfill(allUsers);
				}
			});
		});
	},
	this.getStudentName = function(sAMAccountName) {
		return new Promise(function(fulfill,reject) {
			var info = {}
			ad.findUser(sAMAccountName, function(err,user) {
				if(err) {
					reject(console.log('Student Name Search: '+JSON.stringify(err)));
				} else {
					fulfill(user);
				}
			});
		})
	},
	this.canDeletePTO = function(req,res) { //Check if a user can delete a PTO request (This is currently only capable to be done by Kathy S.)
		return new Promise(function(fulfill, reject) {
			var canDelete;
			ad.isUserMemberOf(req.session.username+config.adDomain, config.adGroupNames.deletePTO, function(err, isMember) { //check if user is part of the deletePTO group
				if(err) {
					reject(console.log('PTO Delete Access Error: ' +JSON.stringify(err)));
				} else {
					canDelete = isMember;
					fulfill(canDelete);
				}
			});	
		});
	},
	this.isITAdministrator = function(req,res) { //Check if a user can delete a PTO request (This is currently only capable to be done by Kathy S.)
		return new Promise(function(fulfill, reject) {
			var isITAdmin;
			ad.isUserMemberOf(req.session.username+config.adDomain, config.adGroupNames.itAdmins, function(err, isMember) { //check if user is part of the deletePTO group
				if(err) {
					reject(console.log('PTO IT Admin Error: ' +JSON.stringify(err)));
				} else {
					isITAdmin = isMember;
					fulfill(isITAdmin);
				}
			});	
		});
	},
	this.canAssignSubs = function(username) { //Check if a user can delete a PTO request (This is currently only capable to be done by Kathy S.)
		return new Promise(function(fulfill, reject) {
			var canAssignSubs;
			ad.isUserMemberOf(username+config.adDomain, config.adGroupNames.assignSubs, function(err, isMember) { //check if user is part of the deletePTO group
				if(err) {
					reject(console.log('can Assign Subs Access Error: ' +JSON.stringify(err)));
				} else {
					canAssignSubs = isMember;
					fulfill(canAssignSubs);
				}
			});	
		});
	},
	this.isFaculty = function(username) { //check if a user is a faculty member.
		return new Promise(function(fulfill, reject) {
			var isFaculty;
			ad.isUserMemberOf(username+config.adDomain, config.adGroupNames.faculty, function(err, isMember) {
				if(err) {
					reject(console.log('Is Faculty Member Error: ' +JSON.stringify(err)));
				} else {
					isFaculty = isMember;
					fulfill(isFaculty);
				}
			});
		});
	},
	this.isFacultyStaff = function(username) { //check if a user is a faculty or Admin (i.e not a student).
		return new Promise(function(fulfill, reject) {
			var isFaculty;
			ad.isUserMemberOf(username+config.adDomain, config.adGroupNames.facultyStaff, function(err, isMember) {
				if(err) {
					reject(console.log('Is Faculty Member Error: ' +JSON.stringify(err)));
				} else {
					isFaculty = isMember;
					fulfill(isFaculty);
				}
			});
		});
	},
	this.getUsersInGroup = function(groupname) {
		var usersInGroupApprovals = [];
		return new Promise(function(fulfill, reject) {
			ad.getUsersForGroup(groupname,function(err,users) {
				if(err) {
					console.log('Group Users Get Error: ' +JSON.stringify(err));
				} else {
					for(i=0;i<users.length;i++)	{
						usersInGroupApprovals.push(users[i].sAMAccountName);
					}
					fulfill(usersInGroupApprovals);		
				}
			});
		});
	}
}

module.exports = new Permissions();