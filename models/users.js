// load up the user model
let bcrypt = require('bcrypt-nodejs');
let dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
let Promise = require('promise');
// expose this function to our app using module.exports

function Users() {
	this.updateUserInfo = function(username, fullName) {
		return new Promise(function(fulfill,reject) {
			let updateQuery = "UPDATE users SET full_name = ? WHERE username = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(updateQuery,[fullName, username],function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'UPDATE Failed - Update User Info: ' + err}));
					} else {
						fulfill(true);
					}
				});
			});
		});
	},
	this.addUserInfo = function(username, fullName) {
		return new Promise(function(fulfill,reject) {
			let insertQuery = "INSERT INTO users (username, full_name) VALUES (?,?)";
			dbconfig.getConnection(function(err, connection) {
				connection.query(insertQuery,[username, fullName],function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'INSERT Failed - Add User Info: ' + err}));
					} else {
						fulfill(true);
					}
				});
			});
		});		
	},
	this.checkUserExists = function(username, fullName) {
		let users = this;
		return new Promise(function(fulfill,reject) {
			let selectQuery = "SELECT count(id) as count from users WHERE username = ?";
			dbconfig.getConnection(function(err, connection) {
				connection.query(selectQuery,[username], function(err,result) {
					connection.release();
					if(err) {
						reject(console.log({status: 1, message: 'GET Failed - Get Username: ' + err}));
					} else {
						if(result[0].count > 0) {
							let updatePromise = users.updateUserInfo(username, fullName);
							updatePromise.then(function(update) {
								fulfill(update);
							});
						} else {
							let insertPromise = users.addUserInfo(username, fullName);
							insertPromise.then(function(insert) {
								fulfill(insert);
							});
						}
					}
				});
			});
		});
	}
}
module.exports = new Users();