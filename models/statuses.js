var dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
function Status() {
	this.addStatus = function(request,response){
		let Status_Name = request.body.txtStatusName;
		let INSERTQUERY = "insert into Statuses( Status_Name ) Values ( ?)";
		dbconfig.getConnection(function(err,connection) { 
			connection.query(INSERTQUERY, [ Status_Name], function(err, result) { 
				connection.release();
				if(err) { 
					console.log({status: 1, message: 'Get Failed - get Status: ' + err});
				} else { 
					response.redirect('/getStatus');
				}
			});
		});
	},
	this.getStatus = function(request,response) {
		return new Promise(function(fulfill, reject){
			let selectQuery = "select * FROM Statuses";
			dbconfig.getConnection(function(err,connection){
				connection.query(selectQuery,function(err,result) {
					connection.release();
					if(err) { 
						reject(console.log({status: 1, message: 'Get Failed - get Status: ' + err}));
					} else {
						fulfill(result);
					}
				});
			});
		});
	},
	this.renderStatusView = function(req,res) {
		let statusPromise = this.getStatus();
		statusPromise.then(function(statuses) {
			res.render('statuses.ejs', {
				login : true,
				statuses : statuses
			});
		});
	},	
	this.updateStatus = function(request,response) {
		let Status_Name = request.body.txtStatus_Name;
		let Status_ID =  request.body.txtStatus_ID;
		let updateQuery ="UPDATE Statuses SET Status_Name = ? WHERE Status_ID = ?";
		dbconfig.GetConnection(function(err,connection) {
			connection.query(updateQuery, [Status_Name, Status_ID], function(err, result){
				connection.release();
				if (err) {
					console.log({status: 1, message: 'Update Failed - UPDATE Status: ' + err});
				} else {
					res.redirect('/getStatus');
				}
			});

		});
	},
	this.deleteStatus = function(request,response) {
		let Status_ID = reque.body.txtStatus_ID;
		let deleteQuery = "DELETE FROM Statuses WHERE Status_ID = ?";
		dbconfig.GetConnection(function(err,connection) {
			connection.query(deleteQuery,[Status_ID],function(err,result) {
				connection.release();
				if(err) {
					console.log({status: 1, message: 'Delete Failed - Delete Status: ' + err});
				} else {
					res.redirect('/getStatus');
				}
			});
		});
	}
}
module.exports = new Status();
