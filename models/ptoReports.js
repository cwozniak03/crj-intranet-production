let bcrypt = require('bcrypt-nodejs');
let dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
let moment = require('moment');
let ad = require('../config/ad.js').ad;
let Promise = require('promise');
let Permissions = require('../models/permissions');
let adRequests = require('../models/adRequests');
let Pto = require('../models/pto');
let config = require("../config/configFile.js").get(process.env.NODE_ENV.trim());

function PTOReports() {
	this.approvedTeacherPTO = function(req,res) {
		let FiscalBegin = getCurrentFiscalYear();
		let BeginDate = FiscalBegin + "-" + "07-01";
		let EndDate = FiscalBegin+ 1 + "-" + "06-30";
		let groupApprovals = [];
		let usersInGroupApprovals = [];
		let promiseAggregate = [];
		let usersPromise;
		let teachersPromise = Permissions.getUsersInGroup(config.adGroupNames.faculty);
		if (req.params.date != "") {
            BeginDate = moment(req.params.date).format("YYYY-MM-DD");
            EndDate = moment(req.params.date).format("YYYY-MM-DD")
        }
		teachersPromise.then(function(teachers) {
			let getQuery = "SELECT pto_request_id, pto_date, pto_notes, pto_value, username, pto_request_type_name, pto_request_submitted_date FROM pto_request ";
			getQuery += "INNER JOIN pto_request_types ON pto_request_types.pto_request_type_id = pto_request.pto_request_type_id ";
			getQuery += "where username IN('"+teachers.join("','")+"') AND pto_approved = 1 AND pto_date BETWEEN ? AND ? ";
            getQuery += "ORDER BY pto_date"; //Grab all of the users PTO requests and send them to the template
			dbconfig.getConnection(function(err, connection){
			    let query = connection.query(getQuery,[BeginDate,EndDate],function(err, result) {
			    	connection.release();
			    	if(err){
			    		console.log({status: 1, message: 'Get Failed - PTO approved Teacher PTO: ' + err});
			    	} else {
						res.render('pto/approvedTeacherPTO.ejs', {
			    			login : true,
			    			moment : moment,
			    			pto: result
			    		});
			    	}
			    });
			});
		});	
	},
	this.totalClassesYear = function(req,res) {
		let FiscalBegin = getCurrentFiscalYear();
		let BeginDate = FiscalBegin + "-" + "07-01";
		let EndDate = FiscalBegin+ 1 + "-" + "06-30";
		let getQuery = "SELECT full_name, count(pto_classes_id) as count FROM pto_substitute_classes as psc ";
			getQuery += "INNER JOIN pto_request as pr on psc.pto_request_id = pr.pto_request_id ";
			getQuery += "INNER JOIN users as u on pr.username = u.username ";
			getQuery += "WHERE pr.pto_date >= ? ";
			getQuery += "AND pr.pto_date <= ? group by u.full_name";
		dbconfig.getConnection(function(err, connection){
		    var query = connection.query(getQuery,[BeginDate,EndDate],function(err, result) {
		    	connection.release();
		    	if(err){
		    		console.log({status: 1, message: 'Get Failed - PTO approved Teacher PTO: ' + err});
		    	} else {
					res.render('pto/subsRequestsReport.ejs', {
		    			login : true,
		    			report: result
		    		});
		    	}
		    });
		});
	},
	this.getPTObyReportTo = function(req, res) {
		let FiscalBegin = getCurrentFiscalYear();
		let BeginDate = FiscalBegin + "-" + "07-01";
		let EndDate = FiscalBegin+ 1 + "-" + "06-30";
		let ptos = this;
		let login;
		let userPromiseAggregate = [];
		if(req.session.isAuthenticated) { //Checks if user is authenticated and passes the login value to ejs
			login = true;
		} else {
			login = false;
		}
		let getDirectReports = adRequests.getDirectReports(req.session.username);
		getDirectReports.then(function(directReports){
			directReports.forEach(function(results) {
				userPromiseAggregate.push(adRequests.getUserUsernameFromCN(results));
			});
			Promise.all(userPromiseAggregate).then(function(usernames) {
				let pto_totalsPromise = ptos.getUsedPTO(usernames);
				pto_totalsPromise.then(function(pto_totals){
					let getQuery = "SELECT pto_request_id, pto_date, pto_notes, pto_request_types.pto_request_type_name, pto_value, username FROM pto_request ";
						getQuery += "INNER JOIN pto_request_types ON pto_request_types.pto_request_type_id = pto_request.pto_request_type_id ";
						getQuery += "WHERE username IN('"+usernames.join("','")+"') ";
						getQuery += "AND pto_request.pto_date >= ? ";
						getQuery += "AND pto_request.pto_date <= ? ";
						getQuery += "ORDER BY username, pto_date"; //Grab all of the users PTO requests and send them to the template
					dbconfig.getConnection(function(err, connection){
						connection.query(getQuery,[BeginDate,EndDate],function(err, result) {
					    	connection.release();
					    	if(err){
					    		console.log({status: 1, message: 'Get Failed - PTO Get Direct Reports Report Needed: ' + err});
					    	} else {
								res.render('pto/ptoDirectReportsReport.ejs', {
					    			login : login,
					    			moment : moment,
					    			flash: req.flash('pto'),
					    			pto: result,
					    			pto_totals: pto_totals
					    		});
					    	}
					    });
					});
				});
			});
		});
	},
	this.getPTOAllUsers = function(req, res) {
		let FiscalBegin = getCurrentFiscalYear();
		let BeginDate = FiscalBegin + "-" + "07-01";
		let EndDate = FiscalBegin+ 1 + "-" + "06-30";
		let ptos = this;
		let login;
		let userPromiseAggregate = [];
		if(req.session.isAuthenticated) { //Checks if user is authenticated and passes the login value to ejs
			login = true;
		} else {
			login = false;
		}
		let getAllUsers = Permissions.getAllFacultyStaff(req,res);
		getAllUsers.then(function(allUsers){
			let pto_totalsPromise = ptos.getUsedPTO(allUsers);
			pto_totalsPromise.then(function(pto_totals){
				let getQuery = "SELECT pto_request_id, pto_date, pto_notes, pto_request_types.pto_request_type_name, pto_value, username, pto_approved FROM pto_request ";
					getQuery += "INNER JOIN pto_request_types ON pto_request_types.pto_request_type_id = pto_request.pto_request_type_id ";
					getQuery += "WHERE pto_request.pto_date >= ? ";
					getQuery += "AND pto_request.pto_date <= ? ";
					getQuery += "ORDER BY username, pto_date"; //Grab all of the users PTO requests and send them to the template
				dbconfig.getConnection(function(err, connection){
					connection.query(getQuery,[BeginDate,EndDate],function(err, result) {
				    	connection.release();
				    	if(err){
				    		console.log({status: 1, message: 'Get Failed - PTO Get Direct Reports Report Needed: ' + err});
				    	} else {
							res.render('pto/allUsersPTO.ejs', {
				    			login : login,
				    			moment : moment,
				    			flash: req.flash('pto'),
				    			pto: result,
				    			pto_totals: pto_totals
				    		});
				    	}
				    });
				});
			});
		});
	},
	this.getUsedPTO = function(usernames) {
		return new Promise(function(fulfill,reject) {
			let FiscalBegin = getCurrentFiscalYear();
			let BeginDate = FiscalBegin + "-" + "07-01";
			let EndDate = FiscalBegin+ 1 + "-" + "06-30";
			let getQuery = "select results_table.username, sum(pto_initial_seed_amount) as total_days, pto_value as days_used ";
				getQuery += "FROM ";
				getQuery += "(SELECT a.username, pto_initial_seed_amount, sum(pto_value) as pto_value from pto_initial_seed A ";
				getQuery += "left join pto_request b ";
				getQuery += "on a.username = b.username ";
				getQuery += "where pto_date > ? ";
				getQuery += "and pto_date < ? ";
				getQuery += "and a.username IN('"+usernames.join("','")+"') ";
				getQuery += "and b.pto_request_type_id IN (1,2) ";
				getQuery += "and b.pto_approved IN (1,2) ";
				getQuery += "group by a.username ";

				getQuery += "UNION ALL ";
				getQuery += "SELECT c.username, sum(num_adtl_days), 0 from pto_adtl_days c ";
				getQuery += "WHERE c.username IN('"+usernames.join("','")+"') ";
				getQuery += "group by username) results_table ";
				getQuery += "group by username"; //Grab all of the users PTO requests and send them to the template
			dbconfig.getConnection(function(err, connection){
			    connection.query(getQuery,[BeginDate,EndDate],function(err, result) {
			    	connection.release();
			    	if(err){
			    		reject(res.send({status: 1, message: 'Get Failed - PTO calcuate total PTO: ' + err}));
			    	} else {
			    		fulfill(result);
			    	}
			    });
			});
		});
	},
	this.getTeacherPayoutReport = function(req, res) {
		let FiscalBegin = getCurrentFiscalYear();
		let BeginDate = FiscalBegin + "-" + "07-01";
		let EndDate = FiscalBegin+ 1 + "-" + "06-30";
		if(req.session.isAuthenticated) { //Checks if user is authenticated and passes the login value to ejs
			login = true;
		} else {
			login = false;
		}
		let getQuery = "SELECT pto_request.username, SUM(pto_value) as days_used, pto_initial_seed_amount - SUM(pto_value) as pto_remainder FROM pto_request ";
			getQuery += "JOIN pto_initial_seed on pto_request.username = pto_initial_seed.username ";
			getQuery += "WHERE pto_date BETWEEN ? AND ? ";
			getQuery += "and pto_request_type_id IN (1,2) ";
			getQuery += "and pto_request.pto_approved = 1 ";
			getQuery += "and pto_initial_seed_amount = 10 ";
			getQuery += "group by username ";
		dbconfig.getConnection(function(err, connection){
		    connection.query(getQuery,[BeginDate,EndDate],function(err, result) {
		    	connection.release();
		    	if(err){
		    		console.log({status: 1, message: 'Get Failed - PTO Teacher Payout: ' + err});
		    	} else {
    				res.render('pto/teacherPayout.ejs', {
		    			login : login,
		    			moment : moment,
		    			flash: req.flash('pto'),
		    			pto: result
		    		});
		    	}
		    });
		});
	}
}
function getCurrentFiscalYear() {
	var FiscalBegin,
	    CurrentDate,
	    CurrentMonth;
	CurrentDate = new Date();
	CurrentMonth = CurrentDate.getMonth();
	if(CurrentMonth >=6 && CurrentMonth<=12) {
		FiscalBegin = CurrentDate.getFullYear();
	} else {
		FiscalBegin = CurrentDate.getFullYear() - 1;
	}
	return FiscalBegin;
}
module.exports = new PTOReports();