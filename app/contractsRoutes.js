var Promise = require('promise');
var moment = require('moment');

var Permissions = require('../models/permissions');
var Contracts = require('../models/contracts');

module.exports = function(app){
	// =====================================
	// HOME PAGE (with login links and image) ========
	// =====================================

	app.get('/contract/:id', isLoggedIn, isFacultyStaff, function(req,res) {
		var usersPromise = Permissions.getAllFacultyStaff(req,res); //set up the promise for all active faculty and staff
		usersPromise.then(function(allUsers) {
			res.render('ph/contractInfo.ejs', {
				faculty_username: allUsers
			});
		});
	});
	app.get('/createContract', isLoggedIn, isFacultyStaff, function(req,res) {
		res.render('contracts/createContract', {
			login:true
		});
	})
	app.get('/contractList', isLoggedIn, isFacultyStaff, function(req,res) {
		res.render('ph/callHomeReport.ejs');
	});
	app.post('/createContract', isLoggedIn, isFacultyStaff, function(req,res) {
		var contractInfo = {company_name:req.body.companyName, start_date:req.body.startDate, end_date:req.body.end_date}
		Contracts.createContract(contractInfo);
	});

	// route middleware to make sure
	function isLoggedIn(req, res, next) {
		// if user is authenticated in the session, carry on
		if (req.session.isAuthenticated)
			return next();

		// if they aren't redirect them to the home page
		res.redirect('/');
	}

	function isFacultyStaff(req,res,next) {
		//if user is faculty staff, carry on
		var isFacultyStaff = Permissions.isFacultyStaff(req.session.username);
		isFacultyStaff.then(function(result) {
			if(result) { return next();} else {res.redirect('/');}
		});
	}
}