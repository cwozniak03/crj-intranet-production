const moment = require('moment');
const Utils = require('../models/utils');
module.exports = function(app){
	// =====================================
	// HOME PAGE (with login links and image) ========
	// =====================================
	app.post('/genCode', isLoggedIn, function(req,res) {
		let code = Utils.GenerateCode();
		res.send(code);
	});
	app.post('/validateCode', isLoggedIn, function(req,res) {
		let validatePromise = Utils.ValidateCode(req.body.code);
		validatePromise.then(function(validated) {
			res.send(validated);
		});
	});
	// route middleware to make sure
	function isLoggedIn(req, res, next) {
		// if user is authenticated in the session, carry on
		if (req.session.isAuthenticated)
			return next();

		// if they aren't redirect them to the home page
		res.redirect('/');
	}
}