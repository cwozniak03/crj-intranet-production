var moment = require('moment');
let collegeFinancials = require('../models/collegeFinancials');
const multer = require('multer');
const storage = multer.memoryStorage();
const upload = multer({storage: storage});
var Permissions = require('../models/permissions');
module.exports = function(app){
	/*==============Get Routes=============*/
	app.get('/student_profile/:student_number/:page?', isLoggedIn, function(req,res) {
		let student_number = req.params.student_number;
		let page = req.params.page;
		collegeFinancials.renderStudentProfiles(student_number, page, req, res);
	});
	app.get('/colleges', isLoggedIn, function(req,res) {
		collegeFinancials.renderCollegesView(req,res);
	});
	app.get('/grants', isLoggedIn, function(req,res) {
		collegeFinancials.renderGrantsView(req,res);
	});
	app.get('/funding_types', isLoggedIn, function(req,res) {
		collegeFinancials.renderFundingTypeView(req,res);
	});
	app.post('/getStudentsByNumber', isLoggedIn, function(req,res){
		var studentsPermission = collegeFinancials.getAllStudents();
		studentsPermission.then(function(students){
			res.send(students);
		});
	});
	app.get('/fillZeroEFCGrants', isLoggedIn, function(req,res){
		res.render('collegeFinancials/fillZeroEFC.ejs', {
			login : true
		});
	});
	/*==============Add Routes===============*/	
	app.post('/addCollege', isLoggedIn, function(req,res) {
		collegeFinancials.addCollege(req,res);
	});
	app.post('/addGrant', isLoggedIn, function(req,res) {
		collegeFinancials.addGrant(req,res);
	});
	app.post('/addFundingType', isLoggedIn, function(req,res) {
		collegeFinancials.addFundingType(req,res);
	});
	app.post('/addCollegeToStudent', isLoggedIn, function(req,res) {
		collegeFinancials.addCollegeToStudent(req,res);
	});
	app.post('/addfillZeroEFCGrants', isLoggedIn, function(req,res) {
		collegeFinancials.insertPellGrantZeroEFC(req.body.txtPellGrantAmount);
		collegeFinancials.insertFederalUnsubsidized(req.body.txtFederalUnsubsidizedAmount);
		collegeFinancials.insertFederalSubsidized(req.body.txtFederalSubsidizedAmount);
		res.redirect("/");
	});
	app.post('/addfillStaffordLoans', isLoggedIn, function(req,res) {
		collegeFinancials.insertFederalUnsubsidized(req.body.txtFederalUnsubsidizedAmount);
		collegeFinancials.insertFederalSubsidized(req.body.txtFederalSubsidizedAmount);
		res.redirect("/");
	});
	app.post('/addStudentNote', isLoggedIn, function(req,res) {
		collegeFinancials.addNote(req,res);
	});	
	app.post('/addStudentGrant', isLoggedIn, upload.single("upFile"), function(req,res) {
		if(typeof req.file !== 'undefined') { //File uploaded! Do normal stuff
			if(req.body.hidGeneralFund == "true") {
				collegeFinancials.addGeneralGrantToStudent(req.body.hidStuNum, req.body.hidGrant_ID, req.body.txtGrantAmount, req.body.txtGrantLength, req.file.originalname, req.file.mimetype, req.file.buffer, req, res);
			} else {
				collegeFinancials.addGrantToStudentCollege(req.body.hidStuNum, req.body.hidStuColID, req.body.hidGrant_ID, req.body.txtGrantAmount, req.body.txtGrantLength, req.file.originalname, req.file.mimetype, req.file.buffer, req, res);
			}
		} else { //No file uploaded so send empty strings for file and dont upload in the model.
			if(req.body.hidGeneralFund == "true") {
				collegeFinancials.addGeneralGrantToStudent(req.body.hidStuNum, req.body.hidGrant_ID, req.body.txtGrantAmount, req.body.txtGrantLength, "", "", "", req, res);
			} else {
				collegeFinancials.addGrantToStudentCollege(req.body.hidStuNum, req.body.hidStuColID, req.body.hidGrant_ID, req.body.txtGrantAmount, req.body.txtGrantLength, "", "", "", req, res);
			}
		}
		
	});	
	app.post('/getColleges', isLoggedIn, function(req,res) { //This is for populating a list of colleges for output.
		let collegesPromise = collegeFinancials.getColleges();
		collegesPromise.then(function(colleges) {
			res.send(colleges);
		});
	});
	app.post('/getGrants', isLoggedIn, function(req,res) { //This is for populating a list of grants for output.
		let grantsPromise = collegeFinancials.getGrants();
		grantsPromise.then(function(grants) {
			res.send(grants);
		});
	});
	app.get('/getDocumentByID/:id', isLoggedIn, function(req,res) { //Download the attached file
		let student_college_fund_id = req.params.id;
		collegeFinancials.getDocument(student_college_fund_id, req, res);
	});
	app.get('/getGeneralDocumentByID/:id', isLoggedIn, function(req,res) { //Download the attached file
		let student_general_fund_id = req.params.id;
		collegeFinancials.getDocumentGeneralFund(student_general_fund_id, req, res);
	});
	/*==============Update Routes===============*/	
	app.post('/updateCollege', isLoggedIn, function(req,res) {
		collegeFinancials.updateCollege(req,res);
	});
	app.post('/updateGrant', isLoggedIn, function(req,res) {
		collegeFinancials.updateGrant(req,res);
	});
	app.post('/updateFundingType', isLoggedIn, function(req,res) {
		collegeFinancials.updateFundingType(req,res);
	});
	app.post('/updateStudentCollegeFund', isLoggedIn, function(req,res) {
		collegeFinancials.updateStudentCollegeFund(req,res);
	});
	app.post('/updateStudentCollege', isLoggedIn, function(req,res) {
		collegeFinancials.updateStudentCollege(req,res);
	});	
	app.post('/updateStudentGeneralFund', isLoggedIn, function(req,res) {
		collegeFinancials.updateStudentGeneralFund(req,res);
	});			
	app.post('/updateStudentEFC', isLoggedIn, function(req,res) {
		collegeFinancials.updateInsertStudentEFC(req,res);
	});
	app.post('/updateStudentNote', isLoggedIn, function(req,res) {
		collegeFinancials.updateNote(req,res);
	});
	/*==============Delete Routes===============*/	
	app.post('/deleteCollege', isLoggedIn, function(req,res) {
		collegeFinancials.deleteCollege(req,res);
	});
	app.post('/deleteGrant', isLoggedIn, function(req,res) {
		collegeFinancials.deleteGrant(req,res);
	});
	app.post('/deleteFundingType', isLoggedIn, function(req,res) {
		collegeFinancials.deleteFundingType(req,res);
	});
	app.post('/deleteStudentCollege', isLoggedIn, function(req,res) {
		let studentCollegePromise = collegeFinancials.deleteCollegeFromStudent(req,res);
		studentCollegePromise.then(function(result) {
			res.send(result);
		});
	});
	app.post('/deleteStudentCollegeFund', isLoggedIn, function(req,res) {
		let studentCollegeFund = collegeFinancials.deleteGrantFromStudent(req,res);
		studentCollegeFund.then(function(result) {
			res.send(result);
		})
	});
	app.post('/deleteStudentGeneralFund', isLoggedIn, function(req,res) {
		let studentGeneralFund = collegeFinancials.deleteGeneralGrantFromStudent(req,res);
		studentGeneralFund.then(function(result) {
			res.send(result);
		})
	});
	app.post('/deleteStudentNote', isLoggedIn, function(req,res) {
		let studentNote = collegeFinancials.deleteNote(req,res);
		studentNote.then(function(result) {
			res.send(result);
		})
	});	
	// route middleware to make sure
	function isLoggedIn(req, res, next) {
		// if user is authenticated in the session, carry on
		if (req.session.isAuthenticated)
			return next();

		// if they aren't redirect them to the home page
		res.redirect('/');
	}
}