var Promise = require('promise');
var moment = require('moment');
var AdminPortal = require('../models/adminportal');
var Permissions = require('../models/permissions');
module.exports = function(app){
	// =====================================
	// HOME PAGE (with login links and image) ========
	// =====================================
	app.get('/adminPortal',isLoggedIn, function(req,res){
		var itAdminPromise = Permissions.isITAdministrator(req,res); //set up the promise for all active faculty and staff
		itAdminPromise.then(function(isAdmin) {
			if(isAdmin) {
				AdminPortal.getPortalData(req,res);
			} else {
				res.render('index.ejs');
			}		
		});
	});
	app.get('/adtlDays',isLoggedIn, function(req,res){
		let login = true;
		let usersPromise = Permissions.getAllFacultyStaff(req,res); //set up the promise for all active faculty and staff
		usersPromise.then(function(allUsers) {
			allUsers.sort();
			res.render('admin/adtlDays.ejs', {
    			login : login,
    			username : allUsers
    		});
		});
	});
	app.post('/assignMaxCarryOver', isLoggedIn, function(req,res){
		AdminPortal.updateMaxCarryOver(req,res);
	});

	app.post('/assignFacultyPTO', isLoggedIn, function(req,res){
		AdminPortal.updateFacultyPTO(req,res);
	});
	app.post('/assignStaffPTO', isLoggedIn, function(req,res){
		AdminPortal.updateStaffPTO(req,res);
	});
	app.post('/assignPTOResetDate', isLoggedIn, function(req,res){
		AdminPortal.updatePTOResetDate(req,res);
	});
	app.post('/addPTOApprovers', isLoggedIn, function(req,res){
		AdminPortal.addPTOApprover(req,res);
	});
	app.post('/updatePTOApprovers', isLoggedIn, function(req,res){
		AdminPortal.updatePTOApprover(req,res);
	});
	app.post('/deletePTOApprovers', isLoggedIn, function(req,res){
		AdminPortal.deletePTOApprover(req,res);
	});
	app.post('/assignAbsentEmails', isLoggedIn, function(req,res){
		AdminPortal.updateAbsentEmailTo(req,res);
	});
	app.post('/addPTODays', isLoggedIn, function(req,res){
		AdminPortal.addPTODays(req,res);
	});
	// route middleware to make sure
	function isLoggedIn(req, res, next) {
		// if user is authenticated in the session, carry on
		if (req.session.isAuthenticated)
			return next();

		// if they aren't redirect them to the home page
		res.redirect('/');
	}
}