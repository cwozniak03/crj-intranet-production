// app/routes.js

//Set up the LDAP Login information and credentials
var ad = require('../config/ad.js').ad;
var Promise = require('promise');
var Permissions = require('../models/permissions');
let ADRequests = require('../models/adRequests');
let Users = require('../models/users');
let PTO = require('../models/pto');
let config = require("../config/configFile.js").get(process.env.NODE_ENV.trim());
module.exports = function(app) {

	// =====================================
	// HOME PAGE (with login links) ========
	// =====================================
	app.get('/', isLoggedIn, function(req, res) {
		let promiseAggregate = [];
		promiseAggregate.push(Permissions.canApprovePTO(req,res)); //Users can Approve PTO
		promiseAggregate.push(Permissions.hasRole(req.session.username, config.adGroupNames.ptoReports)); //Users can See PTO Reports
		promiseAggregate.push(Permissions.canAssignSubs(req.session.username)); //User can Assign Subs
		promiseAggregate.push(Permissions.hasRole(req.session.username, config.adGroupNames.deletePTO)); //User can Delete PTO
		promiseAggregate.push(Permissions.hasRole(req.session.username, config.adGroupNames.systemAdmins)); //User is a System Admin
		promiseAggregate.push(Permissions.hasRole(req.session.username, config.adGroupNames.collegeFinancials));
		promiseAggregate.push(Permissions.hasRole(req.session.username, config.adGroupNames.itAdmins));
		Promise.all(promiseAggregate).then(function(values) {
			res.render('index.ejs', {
				login: req.session.isAuthenticated,
				canApprovePTO: values[0],
				canViewPTOReports: values[1],
				canAssignSubs: values[2],
				canDeletePTO: values[3],
				systemAdmin: values[4],
				allowCollege: values[5],
				itAdmin: values[6]
			}); // load the index.ejs file
		});
	});

	// =====================================
	// LOGIN ===============================
	// =====================================
	// show the login form
	app.get('/login', function(req, res) {
		res.render('login.ejs', {  
			login: false,
			message: false 
		});
	});

	// process the login form
	app.post('/login', 
		/*passport.authenticate('windows-login', {
            successRedirect : '/profile', // redirect to the secure profile section
            failureRedirect : '/login', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
		}),*/
        function(req, res) {
        	var username = req.body.username;
        	var password = req.body.password;
        	ad.authenticate(username.toLowerCase()+config.adDomain, password, function(err,auth) { //Authenticate the users login credentials against active directory
        		if (err) {
        			console.log('ERROR: '+JSON.stringify(err));
        			res.render('login.ejs', {  
						login: false,
						message: true
					});
        		} else {
	        		if(auth) {
	        			if (req.body.remember) {
			              req.session.cookie.maxAge = 1000 * 60 * 3;
			            } else {
			              req.session.cookie.expires = false;
			            }
			            req.session.isAuthenticated = true; //Establish that the user is now authenticated in the session
			            req.session.username = username;
						let namePromise = ADRequests.getUserFullName(username);
						namePromise.then(function(fullName) {
							let setUserInfo = Users.checkUserExists(username,fullName);
							let seedPTO = PTO.SeedInitialPTOLogin(username);
							try {
								Promise.all([setUserInfo, seedPTO]).then(values => {
									try {
										res.redirect('/');
									} catch (e) {
										console.log("Exception " + e);
									}
									
								});
							} catch (e) {
								console.log("Exception Promise " + e)
							}
	        			});
	        		} else {
	        			res.render('login.ejs', {  
							login: false,
							message: true
						});
	        		}
        		}
        	});  
    	}
    );

	// =====================================
	// PROFILE SECTION =========================
	// =====================================
	// we will want this protected so you have to be logged in to visit
	// we will use route middleware to verify this (the isLoggedIn function)
	app.get('/profile', isLoggedIn, function(req, res) {
		//Subscription.getSubscriptions(req.user,res);
		res.render('profile.ejs', {
			user : req.user // get the user out of session and pass to template
		});
	});

	// =====================================
	// LOGOUT ==============================
	// =====================================
	app.get('/logout', function(req, res) {
		req.session.destroy(function(err) {
			res.redirect('/');
		});	
	});
};

// route middleware to make sure
function isLoggedIn(req, res, next) {
	// if user is authenticated in the session, carry on
	if (req.session.isAuthenticated)
		return next();

	// if they aren't redirect them to the home page
	res.redirect('/login');
}