const Promise = require('promise');
const moment = require('moment');
const Powerschool = require('../models/powerschool');
const Substitutes = require('../models/substitutes');
const Calendar = require('../models/calendar');
const config = require("../config/configFile.js").get(process.env.NODE_ENV.trim());
module.exports = function(app){
	// =====================================
	// HOME PAGE (with login links and image) ========
	// =====================================
	app.get('/subassignment', isLoggedIn, function(req, res) {
		Substitutes.getSubNeeds(req,res);
	});
	app.get('/subassignment/:date', isLoggedIn, function(req,res) {
		Substitutes.getSubNeeds(req,res);
	});
	app.post('/getFreeTeachers', isLoggedIn, function(req,res) {
		var powerschoolPromise = Powerschool.getToken();
		powerschoolPromise.then(function(token) {
			let teachersPromise = Powerschool.getFreeTeachersByPeriod(req,res,token);
			teachersPromise.then(function(data){
				let teacherStack = [];
				let userClasses = [];
				let results = [];
				for(var key in data.info.users) {
					let username = data.info.users[key].email.slice(0,-20);
					teacherStack.push(Substitutes.getClassesSubbedByTeacher(username)); //Get the number of classes all free teachers have subbed
					userClasses.push(Powerschool.getClassesCount(username,req.body.date,token));	//Get the number of classes avaliable teachers teach that day
				}
				Promise.all(teacherStack).then(function(values) {
					Promise.all(userClasses).then(function(classes) {
						for(var key in values) {
							let teacherClasses = classes.find(x => x.name === values[key][0].name); //Find teh name of the teacher in the current loop
							switch(data.teachers.length) {
								//Assign the values based on if they are Primary, Secondary and Tertiary
								// 0 means they are not any of the 3
								// 1 means they are Primary
								// 2 means Secondary
								// 3 means Tertiary
								case 0:
									results.push({
										"name":values[key][0].name,
										"classes":values[key][0].classes,
										"numClasses":teacherClasses.numClasses
									});
									break;
								case 1:
									if(data.teachers[0].subs_ps_assign_user == values[key][0].name) {
										results.push({
											"name":values[key][0].name,
											"classes":values[key][0].classes,
											"numClasses":teacherClasses.numClasses,
											"PrimeSec":data.teachers[0].subs_ps_assign_value
										});
									} else {
										results.push({
											"name":values[key][0].name,
											"classes":values[key][0].classes,
											"numClasses":teacherClasses.numClasses
										});
									}
									break;
								case 2:
									if(data.teachers[0].subs_ps_assign_user == values[key][0].name) {
										results.push({
											"name":values[key][0].name,
											"classes":values[key][0].classes,
											"numClasses":teacherClasses.numClasses,
											"PrimeSec":data.teachers[0].subs_ps_assign_value
										});
									} else if(data.teachers[1].subs_ps_assign_user == values[key][0].name) {
										results.push({
											"name":values[key][0].name,
											"classes":values[key][0].classes,
											"numClasses":teacherClasses.numClasses,
											"PrimeSec":data.teachers[1].subs_ps_assign_value
										});
									} else {
										results.push({
											"name":values[key][0].name,
											"classes":values[key][0].classes,
											"numClasses":teacherClasses.numClasses
										});
									}
									break;
								case 3:
									if(data.teachers[0].subs_ps_assign_user == values[key][0].name) {
										results.push({
											"name":values[key][0].name,
											"classes":values[key][0].classes,
											"numClasses":teacherClasses.numClasses,
											"PrimeSec":data.teachers[0].subs_ps_assign_value
										});
									} else if(data.teachers[1].subs_ps_assign_user == values[key][0].name) {
										results.push({
											"name":values[key][0].name,
											"classes":values[key][0].classes,
											"numClasses":teacherClasses.numClasses,
											"PrimeSec":data.teachers[1].subs_ps_assign_value
										});
									} else if(data.teachers[2].subs_ps_assign_user == values[key][0].name) {
										results.push({
											"name":values[key][0].name,
											"classes":values[key][0].classes,
											"numClasses":teacherClasses.numClasses,
											"PrimeSec":data.teachers[1].subs_ps_assign_value
										});
									} else {
										results.push({
											"name":values[key][0].name,
											"classes":values[key][0].classes,
											"numClasses":teacherClasses.numClasses
										});
									}
									break;																
							}
						}
						res.send(results);
					});
				});
			});
		});		
	});
	app.post('/deleteSub', isLoggedIn, function(req,res) {
		let subsPromise = Substitutes.deleteSub(req.body.id);
		subsPromise.then(function(results){
			let calendarID = Substitutes.getCalendarID(req.body.id);
			calendarID.then(function(eventID) {
				let promiseStack = [];
				promiseStack.push(Substitutes.deleteCalendarID(req.body.id));
				promiseStack.push(Calendar.deleteCalendarEvent(eventID));
				Promise.all(promiseStack).then(function(values) {
					res.send(results);
				});
			});
		});
	});
	// route middleware to make sure
	function isLoggedIn(req, res, next) {
		// if user is authenticated in the session, carry on
		if (req.session.isAuthenticated)
			return next();

		// if they aren't redirect them to the home page
		res.redirect('/');
	}
}