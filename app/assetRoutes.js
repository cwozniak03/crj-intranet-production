let Asset = require('../models/assets');
let Permissions = require('../models/permissions');
module.exports = function(app){
	app.post('/addAsset',isLoggedIn,isFacultyStaff,function(req,res){
		Asset.addAsset(req,res);
	});

	app.get('/getAsset',isLoggedIn,isFacultyStaff,function(req,res){
		Asset.renderAssetView(req,res);
	});

	app.post('/updateAsset',isLoggedIn,isFacultyStaff,function(req,res){
		Asset.updateAsset(req,res);
	});

	app.post('/deleteAsset',isLoggedIn,isFacultyStaff,function(req,res){
		Asset.deleteAsset(req,res);	
	});
	// route middleware to make sure
	function isLoggedIn(req, res, next) {
		// if user is authenticated in the session, carry on
		if (req.session.isAuthenticated)
			return next();

		// if they aren't redirect them to the home page
		res.redirect('/');
	}
	function isFacultyStaff(req,res,next) {
		//if user is faculty staff, carry on
		var isFacultyStaff = Permissions.isFacultyStaff(req.session.username);
		isFacultyStaff.then(function(result) {
			if(result) { return next();} else {res.redirect('/');}
		});
	}
}