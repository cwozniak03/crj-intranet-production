let Status = require('../models/statuses');
let Permissions = require('../models/permissions');
let ADRequests = require('../models/adRequests');
let Asset = require('../models/assets');
let Checkout = require('../models/checkout');
module.exports = function(app){
	app.get('/checkOutAsset',isLoggedIn,isFacultyStaff,function(req,res){
		Checkout.renderCheckOutView(req,res);
	});
	app.get('/checkInAsset',isLoggedIn,isFacultyStaff,function(req,res){
		Checkout.renderCheckInAssets(req,res);
	});
	app.post('/addCheckIn',isLoggedIn,isFacultyStaff,function(req,res){
		Checkout.checkInAsset(req,res);
	});
	app.post('/addCheckOut',isLoggedIn,isFacultyStaff,function(req,res){
		Checkout.addCheckOut(req,res);
	});
	app.post('/updateCheckout',isLoggedIn,isFacultyStaff,function(req,res){
		Checkout.updateCheckInAssets(req,res);
	})	
	// route middleware to make sure
	function isLoggedIn(req, res, next) {
		// if user is authenticated in the session, carry on
		if (req.session.isAuthenticated)
			return next();

		// if they aren't redirect them to the home page
		res.redirect('/');
	}
	function isFacultyStaff(req,res,next) {
		//if user is faculty staff, carry on
		var isFacultyStaff = Permissions.isFacultyStaff(req.session.username);
		isFacultyStaff.then(function(result) {
			if(result) { return next();} else {res.redirect('/');}
		});
	}
}
	