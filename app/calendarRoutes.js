var Promise = require('promise');
var moment = require('moment');
var Calendar = require('../models/calendar');
module.exports = function(app){
	// =====================================
	// HOME PAGE (with login links and image) ========
	// =====================================
	app.post('/assignSubToClass', isLoggedIn, function(req,res) {
		Calendar.sendCalendarInvite(req,res);
	});
	// route middleware to make sure
	function isLoggedIn(req, res, next) {
		// if user is authenticated in the session, carry on
		if (req.session.isAuthenticated)
			return next();

		// if they aren't redirect them to the home page
		res.redirect('/');
	}
}