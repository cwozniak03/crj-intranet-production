var Promise = require('promise');
var moment = require('moment');
var PrimeSec = require('../models/primesec');
var Permissions = require('../models/permissions');
module.exports = function(app){
	// =====================================
	// HOME PAGE (with login links and image) ========
	// =====================================
	app.get('/assignPrimeSec',isLoggedIn, function(req,res){
		var usersPromise = Permissions.getAllFacultyStaff(req,res); //set up the promise for all active faculty and staff
			usersPromise.then(function(allUsers) {
				PrimeSec.getPrimeSecEntries(req,res,allUsers);
			});
	});
	app.post('/assignPrimeSec',isLoggedIn,function(req,res){
		PrimeSec.assignPrimeSecEntry(req,res);
	});

	app.post('/updatePrimeSec',isLoggedIn,function(req,res){
		var updatePrimeSecPromise = PrimeSec.updatePrimeSecEntry(req,res);
		updatePrimeSecPromise.then(function(result){
			res.send(result);
		});
	});

	app.post('/deletePrimeSec', isLoggedIn,function(req,res){
		var deletePrimeSecPromise = PrimeSec.deletePrimeSecEntry(req,res);
		deletePrimeSecPromise.then(function(result) {
			res.send(result);
		});
	});
	// route middleware to make sure
	function isLoggedIn(req, res, next) {
		// if user is authenticated in the session, carry on
		if (req.session.isAuthenticated)
			return next();

		// if they aren't redirect them to the home page
		res.redirect('/');
	}
}