var moment = require('moment');
var PTODates = require('../models/ptodates');
var Permissions = require('../models/permissions');
module.exports = function(app){
	// =====================================
	// HOME PAGE (with login links and image) ========
	// =====================================
	app.get('/blackoutDates', isLoggedIn, function(req,res) {
		PTODates.getBlackoutDates(req,res);
	});

	app.post('/blackoutDates', isLoggedIn, function(req,res) {
		PTODates.addBlackoutDate(req,res);
	});
	app.post('/updateBlackoutDate', isLoggedIn, function(req,res) {
		PTODates.updateBlackoutDate(req,res);
	});
	app.post('/deleteBlackoutDate', isLoggedIn, function(req,res) {
		PTODates.deleteBlackoutDate(req,res);
	});
	app.post('/checkPTODate', isLoggedIn, function(req,res) {
		var isFacultyPromise = Permissions.isFaculty(req.body.pto_username);
		isFacultyPromise.then(function(isFaculty) {
			if(isFaculty) {
				var checkDatePromise = PTODates.checkIfPTOIsBlackout(req.body.pto_date,req.body.pto_date_end);
				checkDatePromise.then(function(isAllowed) {
					res.send(isAllowed);
				});
			} else {
				res.send(false);
			}
		});
	});
	app.get('/searchPTO', isLoggedIn, function(req,res) {
		var canDeletePromise = Permissions.canDeletePTO(req,res);
		canDeletePromise.then(function(canDelete) {
			res.render('searchDeleteDates.ejs', {
				login:true,
				canDelete: canDelete,
				moment: moment
			});
		});
	});
	app.post('/searchPTO', isLoggedIn, function(req,res) {
		var searchDatesPromise = PTODates.searchPTODate(req,res);
		searchDatesPromise.then(function(result) {
			res.send(result);
		});
	});
	// route middleware to make sure
	function isLoggedIn(req, res, next) {
		// if user is authenticated in the session, carry on
		if (req.session.isAuthenticated)
			return next();

		// if they aren't redirect them to the home page
		res.redirect('/');
	}
}