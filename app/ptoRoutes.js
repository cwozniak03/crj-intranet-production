var Pto = require('../models/pto');
var Promise = require('promise');
var moment = require('moment');
var Permissions = require('../models/permissions');
var Powerschool = require('../models/powerschool');
const PTOReports = require('../models/ptoReports');
module.exports = function(app){
	// =====================================
	// HOME PAGE (with login links and image) ========
	// =====================================
	app.get('/pto', isLoggedIn, function(req, res) {
		var request = req;
		var response = res;
		var canDeletePromise = Permissions.canDeletePTO(request,response);
		canDeletePromise.then(function(canDelete) {
			var pendingPromise = Pto.getPendingPTO(request,response,canDelete); //set up the promise for pending PTO
			pendingPromise.then(function(pendingPTO){
				var deniedPromise = Pto.getDeniedPTO(request,response,canDelete); //Set up the promise for Denied PTO
				deniedPromise.then(function(deniedPTO){
					var usersPromise = Permissions.getAllFacultyStaff(request,response); //set up the promise for all active faculty and staff
					usersPromise.then(function(allUsers) {
						var requestTypesPromise = Pto.getPTORequestTypes(request,response); //get the request types
						requestTypesPromise.then(function(request_types) {
							var seedAmountPromise = Pto.ptoSeedAmount(request,response);
							seedAmountPromise.then(function(seedAmount){
								Pto.calculateTotalPTO(request,response,seedAmount,pendingPTO,deniedPTO,allUsers,canDelete,request_types); //Send all data to the PTO calculater and display the page 
							});	
						});
					});
				});
			});
		});
	});
	app.get('/assignpto', isLoggedIn, function(req,res) {
		var usersPromise = Permissions.getAllFacultyStaff(req,res); //set up the promise for all active faculty and staff
		usersPromise.then(function(allUsers) {
			var requestTypesPromise = Pto.getPTORequestTypes(req,res); //get the request types
			requestTypesPromise.then(function(request_types) {
				res.render('assignpto.ejs', {
	    			login : true,
	    			moment : moment,
	    			pto_username : allUsers.sort(),
	    			request_types : request_types,
	    			isFaculty : true
	    		});	
			});
		});
	});
	app.get('/overrideCode', isLoggedIn, function(req,res) {
		res.render('pto/overrideCode.ejs', {
			login : true
		});
	})
	app.get('/ptoSearch', isLoggedIn, function(req, res){
		res.redirect('/pto');
	});
	app.get('/ptoReports', isLoggedIn, function(req, res){
		let promiseAggregate = [];
		promiseAggregate.push(Permissions.hasRole(req.session.username, "PTOReports")); //Users can See PTO Reports
		promiseAggregate.push(Permissions.hasRole(req.session.username, "CRJIntranetAdministrator")); //User is a System Admin
		Promise.all(promiseAggregate).then(function(values) {
			res.render('pto/ptoReports.ejs', {
				login: true,
				canViewPTOReports: values[0],
				systemAdmin: values[1]
			});
		});

	});
	app.get('/approvedTeacherPTO', isLoggedIn, function(req, res){
		PTOReports.approvedTeacherPTO(req,res);
	});
	app.get('/approvedTeacherPTO/:date', isLoggedIn, function(req,res) {
        PTOReports.approvedTeacherPTO(req,res);
    });
	app.get('/directReportsPTO', isLoggedIn, function(req, res){
		PTOReports.getPTObyReportTo(req,res);
	});
	app.get('/allPTO', isLoggedIn, function(req, res){
		PTOReports.getPTOAllUsers(req,res);
	});
	app.get('/teacherPayout', isLoggedIn, function(req, res){
		PTOReports.getTeacherPayoutReport(req, res);
	});
	app.get('/classesForYear', isLoggedIn, function(req, res){
		PTOReports.totalClassesYear(req,res);
	});
	app.post('/ptoSearch', isLoggedIn, function(req, res) { //searches for peoples PTO's if users has DeletePTO in active directory
		var request = req;
		var response = res;
		var canDeletePromise = Permissions.canDeletePTO(request,response);
		canDeletePromise.then(function(canDelete) {
			var pendingPromise = Pto.getPendingPTO(request,response,canDelete); //set up the promise for pending PTO
			pendingPromise.then(function(pendingPTO){
				var deniedPromise = Pto.getDeniedPTO(request,response,canDelete); //Set up the promise for Denied PTO
				deniedPromise.then(function(deniedPTO){
					var usersPromise = Permissions.getAllFacultyStaff(request,response); //set up the promise for all active faculty and staff
					usersPromise.then(function(allUsers) {
						var seedAmountPromise = Pto.ptoSeedAmount(request,response);
						seedAmountPromise.then(function(seedAmount){
							Pto.calculateTotalPTO(request,response,seedAmount,pendingPTO,deniedPTO,allUsers,canDelete); //Send all data to the PTO calculater and display the page 
						});	
					});
				});
			});
		});
	});
	app.post('/getClasses',isLoggedIn,function(req,res) {
		var classesPromise = Powerschool.getClasses(req,res);
		classesPromise.then(function(results){
			res.send(results);
		});
	});
	app.post('/deletePTO',isLoggedIn, function(req, res) { //Deletes a PTO Request [only accessible by someone with DeletePTO in Active Directory]
		Pto.deletePTO(req,res);
	});

	app.get('/ptoapproval',isLoggedIn, function(req, res) { //Lists all PTO's that are awaiting approval or denial
		Pto.getPTOApprovalsNeeded(req,res);
	});
	app.post('/approvePTO',isLoggedIn, function(req, res) { //Approve a PTO Request
		Pto.approvePTO(req,res);
	});
	app.post('/denyPTO',isLoggedIn, function(req, res) { //Deny a PTO request
		Pto.denyPTO(req,res);
	});
	app.post('/pto', isLoggedIn, function(req, res) { //Adds a PTO request
		Pto.addPTO(req, res);
	});
	app.post('/assignpto', isLoggedIn, function(req, res) { //Adds a PTO request
		Pto.assignPTO(req, res);
	});
	// route middleware to make sure
	function isLoggedIn(req, res, next) {
		// if user is authenticated in the session, carry on
		if (req.session.isAuthenticated)
			return next();

		// if they aren't redirect them to the home page
		res.redirect('/');
	}
}