let Status = require('../models/statuses');
let Permissions = require('../models/permissions');
module.exports = function(app){
	app.post('/addStatus',isLoggedIn,isFacultyStaff,function(req,res){
		Status.addStatus(req,res);
	});

	app.get('/getStatus',isLoggedIn,isFacultyStaff,function(req,res){
		Status.renderStatusView(req,res);
	});

	app.post('/updateStatus',isLoggedIn,isFacultyStaff,function(req,res){
		Status.updateStatus(req,res);
	});

	app.post('/deleteStatues',isLoggedIn,isFacultyStaff,function(req,res){
		Status.deleteStatus(req,res);	
	});
	// route middleware to make sure
	function isLoggedIn(req, res, next) {
		// if user is authenticated in the session, carry on
		if (req.session.isAuthenticated)
			return next();

		// if they aren't redirect them to the home page
		res.redirect('/');
	}
	function isFacultyStaff(req,res,next) {
		//if user is faculty staff, carry on
		var isFacultyStaff = Permissions.isFacultyStaff(req.session.username);
		isFacultyStaff.then(function(result) {
			if(result) { return next();} else {res.redirect('/');}
		});
	}
}
	