const Promise = require('promise');
const moment = require('moment');
const Permissions = require('../models/permissions');
const excelCreation = require('../models/excelCreation');
const PH = require('../models/ph');
module.exports = function(app){
	// =====================================
	// HOME PAGE (with login links and image) ========
	// =====================================
	app.get('/assignPH', isLoggedIn, function(req, res) {
		res.render('ph/ph.ejs', {
			login: true,
			success: false
		});
	});
	app.get('/phReports', isLoggedIn, function(req,res) {
		var isFacultyStaff = Permissions.isFacultyStaff(req.session.username);
		isFacultyStaff.then(function(result) {
			res.render('ph/phReports.ejs', {
				isFacultyStaff: result,
				login: true
			})
		});
	});
	app.get('/gradeTardies', isLoggedIn, isFacultyStaff, function(req,res) {
		res.render('ph/gradeTardies.ejs', {
			login: true
		});
	});
	app.get('/callHome', isLoggedIn, isFacultyStaff, function(req,res) {
		res.render('ph/callHomeReport.ejs');
	});
	app.get('/outstandingPhs', isLoggedIn, isFacultyStaff, function(req,res) {
		PH.renderOutstandingPHs(req,res);
	});
	app.get('/outstandingFridayPhs', isLoggedIn, isFacultyStaff, function(req,res) {
		PH.getOustandingFridays(req,res);
	});
	app.get('/outstandingSaturdayPhs', isLoggedIn, isFacultyStaff, function(req,res) {
		PH.getOutstandingSaturdays(req,res);
	});
	app.get('/phBySID', isLoggedIn, isFacultyStaff, function(req,res) {
		PH.getPHListBySID(req,res);
	});		
	app.get('/attendedPhs', isLoggedIn, isFacultyStaff, function(req,res) {
		PH.getAttendedPHs(req,res);
	});
	app.get('/recentFirsts',isLoggedIn, isFacultyStaff, function(req,res) {
		PH.getRecentFirsts(req,res);
	});
	app.get('/exportOustandingXLS', isLoggedIn, isFacultyStaff, function(req,res) {
		const headers = ['First Name', 'Last Name', 'Assigned By', 'Date', 'Date To Attend', 'Reason', 'Description', 'Attended', 'Grade'];
		let outstandingPHPromise = PH.getOustandingPHs();
		outstandingPHPromise.then(function(results) {
			excelCreation.CreateExcelFileOutstandingPH(headers, results, res);
		});
	});	

	app.get('/downloadOutstandingPH', function(req,res) {
		let filepath = './public/OutstandingPH.xlsx';
		let fileName ="OutstandingPH.xlsx";
					
		res.download(filepath, fileName);
	});
	app.post('/gradeTardies', isLoggedIn, isFacultyStaff, function(req, res) {
		PH.getTardiesByGrade(req, res);
	});
	app.post('/callHome', isLoggedIn, isFacultyStaff, function(req,res) {
		PH.getMoreThan10(req,res);
	})
	app.post('/assignPH', isLoggedIn, function(req,res) {
		if(req.body.ph_type == 'PH') {
			var checkAlreadyHasPHForTodayPromise = PH.checkAlreadyHasPHForToday(req.body.students,null);
			checkAlreadyHasPHForTodayPromise.then(function(result){	
				if(result[0]) {
					//Assign Regular PH with "hidden" attended marker (We want to keep track, but students wont have to attend past 1 a day)
					PH.addPH(req,res,false,true);
					//Check if a student already has a Friday PH
					var checkAlreadyHasFridayPromise = PH.checkAlreadyHasFridayThisWeek(req.body.students);
					checkAlreadyHasFridayPromise.then(function(hasFriday){
						if(!hasFriday) {
							//Assign a Friday PH
							PH.addPH(req,res,true,false);		
						}
					});
				} else {
					PH.addPH(req,res,false,false);
				}
			});
		} else {
			PH.addPH(req,res,false,false);
		}
	});
	app.post('/phMarkAttended', isLoggedIn, isFacultyStaff, function(req,res) {
		PH.markAttended(req,res);
	});
	app.post('/phMarkMissed', isLoggedIn, isFacultyStaff, function(req,res) {
		PH.markMissed(req,res);
	});
	app.post('/phMarkRemoved', isLoggedIn, isFacultyStaff, function(req,res) {
		PH.markRemoved(req,res);
	});	
	app.post('/getStudents', isLoggedIn, function(req,res){
		var studentsPermission = Permissions.getAllStudents();
		studentsPermission.then(function(students){
			res.send(students);
		});
	});
	app.post('/getOutstandingPHStudent', isLoggedIn, isFacultyStaff, function(req,res) {
		var oustandingPHPermission = PH.getOutstandingPHsByStudent(req.body.student,req.body.ph_type);
		oustandingPHPermission.then(function(students){
			res.send(students);
		});
	});
	app.post('/getAttendedPHStudent', isLoggedIn, isFacultyStaff, function(req,res) {
		var attendedPHPermission = PH.getAttendedPHsByStudent(req,res);
		attendedPHPermission.then(function(students){
			res.send(students);
		});
	});	
	app.post('/emailTardyReminder', isLoggedIn, isFacultyStaff, function(req,res) {
		PH.emailPHForToday();
		res.send("Complete");
	});
	// route middleware to make sure
	function isLoggedIn(req, res, next) {
		// if user is authenticated in the session, carry on
		if (req.session.isAuthenticated)
			return next();

		// if they aren't redirect them to the home page
		res.redirect('/');
	}

	function isFacultyStaff(req,res,next) {
		//if user is faculty staff, carry on
		var isFacultyStaff = Permissions.isFacultyStaff(req.session.username);
		isFacultyStaff.then(function(result) {
			if(result) { return next();} else {res.redirect('/');}
		});
	}
}